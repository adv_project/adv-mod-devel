#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('update', '#> Running script', 'update');

my $json = '{ "action": "tick" }';
my $responses = dbQueryAtom("select trigger_update('".$json."'::jsonb);");
resolveOfflineResponse($responses);

1;
