#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

my $owner = $GAMEPROPERTIES->{'owner'};
logEventNotice('configure-game.pl', ':: Running perl script', 'init');
callResumeOffline();
my $json = '{"action":["register_world_object"],"player":"'.$owner.'"}';
processOfflineAdminEntry($json);
$json = '{"action":["register_as_owner"],"player":"'.$owner.'"}';
processOfflineAdminEntry($json);
processOfflineModMessage("message", "you_can_enter_the_game_now");

1;
