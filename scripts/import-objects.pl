#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('import-objects.pl', ':: Running perl script', 'init');
# Import objects
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};
my $owner = $GAMEPROPERTIES->{'owner'};
my $retval;
$retval = system($WDIR.'/mods/jsonlib/bin/import_objects "dbname='.$dbname.' host='.$dbhost.' user='.$dbuser.'" '.$schema.' '.$WDIR.' '.$WDIR.'/awg.json');
if ($retval > 0) {
#  my $json = '{"response_class":"objects_importer_failed","response_type":"error","player":"'.$GAMEPROPERTIES->{'owner'}.'","recipient_username":"'.$GAMEPROPERTIES->{'owner'}.'","text":[],"retval":true}';
#  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
#  resolveGameResponse($response);
  processOfflineModMessage('message', 'objects_importer_failed');
} else {
#  my $json = '{"response_class":"objects_importer_ok","response_type":"dialog","player":"'.$GAMEPROPERTIES->{'owner'}.'","recipient_username":"'.$GAMEPROPERTIES->{'owner'}.'","text":[],"retval":true}';
#  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
#  resolveGameResponse($response);
  processOfflineModMessage('message', 'objects_importer_ok');
}

1;
