#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('populate-database.pl', ':: Running perl script', 'init');
dbCreateSchema();
importSqlCode($WDIR."/mods/adv-devel/sqlcore");
confirmDbIsSane();
sleep 1;
forkLogEvent("info", "populate-database.pl", "*** Database created and populated ***");
forkLogEvent("info", "populate-database.pl", "For more info check the tables:");
forkLogEvent("info", "populate-database.pl", "  trigger_stack_logs");
forkLogEvent("info", "populate-database.pl", "  trigger_logs");
forkLogEvent("info", "populate-database.pl", "  responses_stack_logs");
forkLogEvent("info", "populate-database.pl", "  responses_logs");
forkLogEvent("info", "populate-database.pl", "  parser_logs");
forkLogEvent("info", "populate-database.pl", "  debug_logs");
forkLogEvent("info", "populate-database.pl", "Some useful queries:");
forkLogEvent("info", "populate-database.pl", "  select log_class, log_content from logs");
forkLogEvent("info", "populate-database.pl", "  select log_class, log_trace, log_content from trigger_logs");
forkLogEvent("info", "populate-database.pl", "  select log_content, log_dump from responses_stack_logs");
forkLogEvent("info", "populate-database.pl", "Also, in psql try: \"\d+ <table_name>\"");

1;
