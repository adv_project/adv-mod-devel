#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('promote-player-...pl', ':: Running perl script', 'init');
# Populating world map
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};

my $player = $GAMEPROPERTIES->{'owner'};
my $json = '{"action":["set_game_phase"],"player":"'.$player.'","game_phase":"character"}';
processOfflineAdminEntry($json);
processOfflineModMessage('dialog', 'banner_create_character');

1;
