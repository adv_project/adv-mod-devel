#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('draw-map.pl', ':: Running perl script', 'junk');
my $json   = readFile($WDIR."/awgmeta.json");
my $meta   = JSON::decode_json($json);
   $json   = readFile($WDIR."/awg.json");
my $object = JSON::decode_json($json);
my @world  = @{$object};

# The dictionary

my $dict = {
  'alpine_forest' => '@',
  'alpine_mountains' => '^',
  'alpine_plateau' => '#',
  'arid_desert' => '.',
  'arid_plateau' => '#',
  'cloud_forest' => 'C',
  'cold_forest' => '@',
  'cold_high_mountains' => '^',
  'cold_steppe' => '.',
  'deciduous_forest' => '@',
  'deep_sea' => '.',
  'desert' => '.',
  'frozen_desert' => '.',
  'frozen_high_mountains' => '^',
  'frozen_plateau' => '#',
  'herbaceous_savannah' => '~',
  'mangrove' => '&',
  'monzonic_jungle' => '&',
  'polar_desert' => '.',
  'polar_high_mountains' => '^',
  'polar_mountains' => '^',
  'prairies' => '~',
  'rainforest' => 'R',
  'sea' => '.',
  'steppe' => '.',
  'taiga' => '@',
  'taiga_mountains' => '^',
  'tundra' => '.',
  'tundra_mountains' => '^',
  'warm_forest' => '@',
  'warm_prairies' => '~',
  'warm_steppe' => '.',
  'wooded_savannah' => '@',
  'river' => '+',
  'frozen_river' => '+',
  'lake' => 'L',
  'coral_reef' => 'c',
  'volcano' => 'V',
  'volcanic_island' => 'V'
};

my $colors = {
  'alpine_forest' => '#(cyan)',
  'alpine_mountains' => '#(cyan)',
  'alpine_plateau' => '#(cyan)',
  'arid_desert' => '#(red)',
  'arid_plateau' => '#(red)',
  'cloud_forest' => '#(green)',
  'cold_forest' => '#(green)',
  'cold_high_mountains' => '#(white)',
  'cold_steppe' => '#(red)',
  'deciduous_forest' => '#(green)',
  'deep_sea' => '#(blue)',
  'desert' => '#(red)',
  'frozen_desert' => '#(white)',
  'frozen_high_mountains' => '#(white)',
  'frozen_plateau' => '#(white)',
  'herbaceous_savannah' => '#(green)',
  'mangrove' => '#(green)',
  'monzonic_jungle' => '#(green)',
  'polar_desert' => '#(white)',
  'polar_high_mountains' => '#(white)',
  'polar_mountains' => '#(white)',
  'prairies' => '#(yellow)',
  'rainforest' => '#(green)',
  'sea' => '#(blue)',
  'steppe' => '#(yellow)',
  'taiga' => '#(magenta)',
  'taiga_mountains' => '#(magenta)',
  'tundra' => '#(magenta)',
  'tundra_mountains' => '#(magenta)',
  'warm_forest' => '#(green)',
  'warm_prairies' => '#(yellow)',
  'warm_steppe' => '#(yellow)',
  'wooded_savannah' => '#(green)',
  'river' => '#(blue)',
  'frozen_river' => '#(white)',
  'lake' => '#(blue)',
  'coral_reef' => '#(cyan)',
  'volcano' => '#(red)',
  'volcanic_island' => '#(red)'
};

# The Matrix

my @rows = ();
for my $i (0 .. (@{$meta->{'tiles_matrix'}}-1)) {
  for my $j (0 .. (@{$meta->{'tiles_matrix'}[$i]}-1)) {
    $rows[$j][$i] = $meta->{'tiles_matrix'}[$i][$j];
  }
}

# The Biomes

my %biomes = {};
for my $i (0 .. (@world-1)) {
  my $json = JSON->new->pretty(1)->encode($world[$i]);
  my $obj = JSON::decode_json($json);
  my $id = $obj->{'id'};
  my $class = $obj->{'classes'}[0];
  $biomes{$id} = $class;
}

my $matrix = {
  'the_matrix' => dclone(\@rows),
  'the_biomes' => dclone(\%biomes)
};

# Draw it

my @lines = ();
for my $i (0 .. (@{$matrix->{'the_matrix'}}-1)) {
  my $line = '  ';
  my $cur_color;
  for my $j (0 .. (@{$matrix->{'the_matrix'}[$i]}-1)) {
    my $k = $matrix->{'the_matrix'}[$i][$j];
    my $b = $matrix->{'the_biomes'}->{$k};
    my $c = $dict->{$b};
    my $color = $colors->{$b};
    if ($c) {
      if ($cur_color ne $color) {
        $cur_color = $color;
        $c = $color.$c;
      }
      $line = $line.$c;
    } else {
      $cur_color = '#(white)';
      $line = $line.$cur_color.'x';
    }
  }
  push(@lines, $line);
}

# Prepare response

my $response = {
  'response_type' => 'message',
  'response_class' => 'map',
  'messages' => dclone(\@lines),
  'recipient_username' => $INPUT->{'player'}
};

my @responses = ($response);
resolveGameResponse(JSON->new->encode(dclone(\@responses)));

1;
