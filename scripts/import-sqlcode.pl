#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/adv-devel-lib.pl";

logEventNotice('import-sqlcode.pl', ':: Running perl script', 'init');
importSqlCode($WDIR."/mods/adv-devel/sql");

1;
