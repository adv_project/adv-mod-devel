SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
MOD := adv-devel
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')

MDDIRS :=			.

EXECDIRS :=		scripts \
							hooks

PERLDIRS :=		perl \
							perl/api \
							perl/autoload \
							perl/awg \
							perl/db \
							perl/import \
							perl/io \
							perl/logs \
							perl/players \
							perl/run

SQLDIRS :=		sqlcore \
							sql/00-Tables \
							sql/01-Toolbox \
							sql/02-Engine \
							sql/03-TriggersAPI \
							sql/04-Attributes \
							sql/04-Deprecated \
							sql/05-CLI \
							sql/06-Testing

JSONDIRS :=		. \
							json \
							json/admin \
							json/artifacts \
							json/biomes \
							json/coastlines \
							json/terrains \
							json/character_classes \
							json/classes \
							json/biome_ecoregions \
							json/elements \
							json/fallback \
							json/game_ecoregions \
							json/genders \
							json/knowledge \
							json/locomotion \
							json/meta \
							json/proto_ecoregions \
							json/races \
							json/resource_classes \
							json/resources \
							json/senses \
							json/species \
							json/storyline_objects \
							json/techniques \
							json/worlds

WORDSDIRS :=	words/en \
							words/es
MDDIRS_MINIMAL :=			.

EXECDIRS_MINIMAL :=		scripts \
							        hooks

PERLDIRS_MINIMAL :=		perl \
							perl/api \
							perl/autoload \
							perl/db \
							perl/import \
							perl/awg \
							perl/io \
							perl/logs \
							perl/players \
							perl/run

SQLDIRS_MINIMAL :=		sqlcore \
							sql/00-Tables \
							sql/01-Toolbox \
							sql/02-Engine \
							sql/03-TriggersAPI \
							sql/04-Attributes \
							sql/04-Deprecated \
							sql/05-CLI \
							sql/06-Testing

JSONDIRS_MINIMAL :=		. \
							json \
							json/admin \
							json/biomes \
							json/character_classes \
							json/classes \
							json/elements \
							json/fallback \
							json/genders \
							json/knowledge \
							json/locomotion \
							json/meta \
							json/races \
							json/senses \
							json/species \
							json/worlds

WORDSDIRS_MINIMAL :=	words/en

default:

.PHONY: clean

.FORCE:

install:
	for mddir in $(MDDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mddir; \
		for mdfile in $$mddir/*.md ; do \
			install -Dv -m 644 $$mdfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mdfile ; \
		done; \
	done
	for execdir in $(EXECDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execdir; \
		for execfile in $$execdir/* ; do \
			install -Dv -m 755 $$execfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execfile ; \
		done; \
	done
	for wordsdir in $(WORDSDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$wordsdir; \
		for wordsfile in $$wordsdir/*.words ; do \
			install -Dv -m 644 $$wordsfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$wordsfile ; \
		done; \
	done
	for perldir in $(PERLDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perldir; \
		for perlfile in $$perldir/*.pl ; do \
			install -Dv -m 644 $$perlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perlfile ; \
		done; \
	done
	for jsondir in $(JSONDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsondir; \
		for jsonfile in $$jsondir/*.json ; do \
			install -Dv -m 644 $$jsonfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsonfile ; \
		done; \
	done
	for sqldir in $(SQLDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sqldir; \
		for sqlfile in $$sqldir/*.sql ; do \
			install -Dv -m 644 $$sqlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sqlfile ; \
		done; \
	done
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

minimal-install:
	for mddir in $(MDDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mddir; \
		for mdfile in $$mddir/*.md ; do \
			install -Dv -m 644 $$mdfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mdfile ; \
		done; \
	done
	for execdir in $(EXECDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execdir; \
		for execfile in $$execdir/* ; do \
			install -Dv -m 755 $$execfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execfile ; \
		done; \
	done
	for wordsdir in $(WORDSDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$wordsdir; \
		for wordsfile in $$wordsdir/*.words ; do \
			install -Dv -m 644 $$wordsfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$wordsfile ; \
		done; \
	done
	for perldir in $(PERLDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perldir; \
		for perlfile in $$perldir/*.pl ; do \
			install -Dv -m 644 $$perlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perlfile ; \
		done; \
	done
	for jsondir in $(JSONDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsondir; \
		for jsonfile in $$jsondir/*.json ; do \
			install -Dv -m 644 $$jsonfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsonfile ; \
		done; \
	done
	for sqldir in $(SQLDIRS_MINIMAL) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sqldir; \
		for sqlfile in $$sqldir/*.sql ; do \
			install -Dv -m 644 $$sqlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sqlfile ; \
		done; \
	done

uninstall:
	@echo "Uninstalling..."
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:

