-- Slots define open spaces in objects
CREATE TABLE slots (
	id         VARCHAR REFERENCES everything ON DELETE CASCADE,
	ord        INTEGER,
	name       VARCHAR,
	element    VARCHAR,
	mass       NUMERIC,
	closure    NUMERIC  DEFAULT 0.0,
	slot_type  VARCHAR  DEFAULT 'placement'::VARCHAR,
	value      NUMERIC  DEFAULT -1.0
);

-- Plugs define how objects occupy spaces
CREATE TABLE plugs (
	id         VARCHAR  REFERENCES everything ON DELETE CASCADE,
	ord        INTEGER,
	name       VARCHAR,
	element    VARCHAR,
	mass       NUMERIC,
	slot_type  VARCHAR  DEFAULT 'placement'::VARCHAR,
	value      NUMERIC  DEFAULT 1.0
);

-- Slices define the mass of objects
CREATE TABLE slices (
	id       VARCHAR REFERENCES everything ON DELETE CASCADE,
	name     VARCHAR,
   element  VARCHAR,
	mass     NUMERIC
);

