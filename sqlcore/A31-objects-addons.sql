-- Engraved text on objects, line by line
CREATE TABLE engraved_text (
	id          VARCHAR    REFERENCES everything ON DELETE CASCADE,
  line_number INTEGER,   -- Deprecated
  text_line   TEXT
);

CREATE TABLE memories (
  self varchar,
  meme varchar,
  times integer,
  recognized boolean default false,
  context varchar default 'describe'
);

CREATE TABLE color_codes (
  color_code  NUMERIC,
  color_name  TEXT
);

CREATE TABLE diaphaneity_codes (
  diaphaneity_code  NUMERIC,
  diaphaneity_name  TEXT
);

CREATE TABLE shape_codes (
  shape_code  NUMERIC,
  shape_name  TEXT
);

