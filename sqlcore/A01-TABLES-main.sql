/* Database logs */

CREATE TABLE logs (
	ts           TIMESTAMP,
	log_level    VARCHAR,
	log_class    VARCHAR,
	log_content  VARCHAR,
  last_commit  BOOLEAN DEFAULT TRUE
);

/* Main index, contains all unique ids used in the game */

CREATE TABLE everything (
    id        VARCHAR PRIMARY KEY,    -- Unique, may be stone, hunger, asdf1234, ...
    type      VARCHAR,                -- May be attribute, instance, class...
    in_table  VARCHAR                 -- Table in which the object is located
);

-- All objects created since world's birth go into this main table
CREATE TABLE objects (
	id       VARCHAR    PRIMARY KEY  REFERENCES everything ON DELETE CASCADE,
	classes  VARCHAR[],
	active   BOOLEAN    DEFAULT FALSE
);

-- Links are used to connect different objects in several ways
CREATE TABLE links (
   id      VARCHAR   REFERENCES everything ON DELETE CASCADE,
	type    VARCHAR,    -- Link type (parent, neighbour, etc.)
	name    VARCHAR,    -- Link name (e.g. north)
	target  VARCHAR     -- Target object id
);

/* Attributes index, used to reference attributes by class and viceversa */

CREATE TABLE attributes_index (
    id         VARCHAR  REFERENCES everything ON DELETE CASCADE,
    attribute  VARCHAR  REFERENCES everything ON DELETE CASCADE
);

/* Boundaries, used as attribute value ranges */

CREATE TABLE boundaries (
    id     VARCHAR  REFERENCES everything ON DELETE CASCADE,
    label  VARCHAR,
	 value  NUMERIC
);

