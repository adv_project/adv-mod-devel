CREATE OR REPLACE FUNCTION log_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content VARCHAR
)
RETURNS VOID AS $$
  BEGIN
    INSERT INTO logs
    VALUES (
      now(),
      log_level,
      log_class,
      log_content
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_logs()
RETURNS INTEGER AS $$
	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;
	
  BEGIN
		SELECT 5000
    INTO threshold;

		SELECT count (ts)
    FROM logs
    INTO n;

		SELECT n - threshold
    INTO overflow;

		IF overflow > 0 THEN
			DELETE FROM logs
      WHERE ts IN (
        SELECT ts
        FROM logs
        LIMIT overflow
      );

			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

