#!/usr/bin/env perl

sub buildAwgInput {
  my $i = shift;
  my $o = {
    'geometry' => {
      'type' => $i->{'geometry_type'},
      'size' => $i->{'geometry_size'},
      'width' => $i->{'geometry_width'},
      'height' => $i->{'geometry_height'}
    },
    'altitude' => {
      'minimum' => $i->{'altitude_minimum'},
      'maximum' => $i->{'altitude_maximum'}
    },
    'salinity' => {
      'minimum' => $i->{'salinity_minimum'},
      'maximum' => $i->{'salinity_maximum'}
    },
    'atm_humidity' => {
      'default' => $i->{'atm_humidity_default'}
    },
    'soil_humidity' => {
      'minimum' => $i->{'soil_humidity_minimum'},
      'maximum' => $i->{'soil_humidity_maximum'}
    },
    'humidity_absorption' => {
      'default' => $i->{'humidity_absorption_default'}
    },
    'temperature' => {
      'minimum' => $i->{'temperature_minimum'},
      'maximum' => $i->{'temperature_maximum'},
      'maximum_gradient' => $i->{'temperature_maximum_gradient'},
      'percentage' => $i->{'temperature_percentage'},
      'randomness' => $i->{'temperature_randomness'}
    },
    'pressure' => {
      'default' => $i->{'pressure_default'},
      'minimum' => $i->{'pressure_minimum'},
      'maximum' => $i->{'pressure_maximum'},
      'maximum_gradient' => $i->{'pressure_maximum_gradient'},
      'percentage' => $i->{'pressure_percentage'},
      'randomness' => $i->{'pressure_randomness'}
    },
    'land_percentage' => $i->{'land_percentage'},
    'accidents' => {
      'strength' => $i->{'accidents_strength'}
    },
    'erosion' => {
      'strength' => $i->{'erosion_strength'},
      'percentage' => $i->{'erosion_percentage'},
      'randomness' => $i->{'erosion_randomness'}
    },
    'collapse' => {
      'strength' => $i->{'collapse_strength'},
      'percentage' => $i->{'collapse_percentage'},
      'randomness' => $i->{'collapse_randomness'}
    },
    'waterfall' => {
      'percentage' => $i->{'waterfall_percentage'},
      'randomness' => $i->{'waterfall_randomness'}
    },
    'attributes' => {
      'seconds_per_tick' => $i->{'seconds_per_tick'},
      'ticks_per_day' => $i->{'ticks_per_day'},
    },
    'world_tilt' => $i->{'world_tilt'},
    'chrons' => $i->{'chrons'},
    'world_path' => './awg.json',
    'log_path' => './awg.log',
    'meta_path' => './awgmeta.json',
    'world_class' => 'world'
  };
  return $o;
}

1;

