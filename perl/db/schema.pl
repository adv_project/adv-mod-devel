#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/db/dbi.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

# Create database schema for this game
# dbCreateSchema()
sub dbCreateSchema {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = 'create schema '.$schema.';';
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 0 });
	my $sth = $dbh->prepare( $commands );
	my $rv = $sth->execute();
	print $DBI::errstr unless ($rv >= 0);
	$sth->finish();
	$dbh->disconnect();
}

# Destroy database schema
# dbDestroySchema()
sub dbDestroySchema {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = 'drop schema '.$schema.' cascade;';
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass{ RaiseError => 0 });
	my $sth = $dbh->prepare( $commands );
	my $rv = $sth->execute();
	print $DBI::errstr unless ($rv >= 0);
	$sth->finish();
	$dbh->disconnect();
}

1;
