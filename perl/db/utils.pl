#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/db/query.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

#
# Helper db functions
#

sub newId {
	return dbQueryAtom('select new_id(16);');
}

sub newPath {
	my $suffix = shift;
	my $newpath;
	while (1) {
		$newpath = $WDIR.'/'.newId().$suffix;
		last unless (-f $newpath);
	}
	return $newpath;
}

sub getNewId {
	return dbQueryAtom('select make();');
}

### END Database Interface

1;
