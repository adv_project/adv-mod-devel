#!/usr/bin/env perl
	
require "/usr/share/adv/mods/adv-devel/perl/io/file.pl";
require "/usr/share/adv/mods/adv-devel/perl/io/json.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/io.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/query.pl";
require "/usr/share/adv/mods/adv-devel/perl/players/all.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/utils.pl";
require "/usr/share/adv/mods/adv-devel/perl/run/env.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/messages.pl";

sub resolveGameResponse {
  my $json = shift;
  my $responses = JSON::decode_json($json);

  foreach (@{ $responses }) {
    my $response = $_;
    if ($response->{'response_type'} eq "awginput") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', 'response type is awginput, forking', 'DBI');
      my $player = $GAMEPROPERTIES->{'owner'};
      my $awginput = buildAwgInput($response);
      writeFile($WDIR.'/awginput_object.json', JSON->new->pretty(1)->encode($awginput));
      system($WDIR.'/mods/'.$MODNAME.'/scripts/create-world.pl '.$WDIR.' &');
    } elsif ($response->{'response_type'} eq "help") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendInfo($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    } elsif ($response->{'response_type'} eq "error") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendError($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    } elsif (! $response->{'response_type'}) {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
#     callSendWarning($response->{'recipient_username'}, "(empty) ".JSON->new->encode($response));
    } else {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendPlain($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    }
  }
}

sub processAdminEntry {
  my $action = shift;
  my $json = '{"action":["'.$action.'"], "player":"'.$GAMEPROPERTIES->{'owner'}.'"}';
  my $response = dbQueryAtom("select process_admin_entry('".$json."'::jsonb);");
  resolveGameResponse($response);
}

sub processModMessage {
  my $response_type = shift;
  my $response_class = shift;
  my $player = shift;
  $player = $GAMEPROPERTIES->{'owner'} unless ($player);
  my $recipient = shift;
  $recipient = $player unless ($recipient);
  my $json = '{"response_class":"'.$response_class.'","response_type":"'.$response_type.'","player":"'.$player.'","recipient_username":"'.$recipient.'","text":[],"retval":true}';
  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
  resolveGameResponse($response);
}

sub resolveOfflineResponse {
  my $json = shift;
  my $responses = JSON::decode_json($json);

  foreach (@{ $responses }) {
    my $response = $_;
    if ($response->{'response_type'} eq "help") {
      logEventDebug('perl/db/responses.pl resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'info', dclone(\@{$parsed->{'messages'}}));
    } elsif ($response->{'response_type'} eq "error") {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'error', dclone(\@{$parsed->{'messages'}}));
    } elsif (! $response->{'response_type'}) {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
#     callSendWarning($response->{'recipient_username'}, "(empty) ".JSON->new->encode($response));
    } else {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'plain', dclone(\@{$parsed->{'messages'}}));
    }
  }
}

sub processOfflineAdminEntry {
  my $json = shift;
  my $response = dbQueryAtom("select process_admin_entry('".$json."'::jsonb);");
  resolveOfflineResponse($response);
}

sub processOfflineModMessage {
  my $response_type = shift;
  my $response_class = shift;
  my $player = shift;
  $player = $GAMEPROPERTIES->{'owner'} unless ($player);
  my $recipient = shift;
  $recipient = $player unless ($recipient);
  my $json = '{"response_class":"'.$response_class.'","response_type":"'.$response_type.'","player":"'.$player.'","recipient_username":"'.$recipient.'","text":[],"retval":true}';
  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
  resolveOfflineResponse($response);
}

1;
