#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/db/dbi.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

# Query the database and return the result
# dbQuery(query)
# Outputs a 2-dimensional array, in the form:
# array[row][column]
sub dbQuery {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 0 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my @result = ();
	while(my @row = $sth->fetchrow_array()) {
    my @columns = ();
  	for my $r (0 .. (@row - 1)) {
	  	push(@columns, $row[$r]);
  	}
		push(@result, dclone(\@columns));
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone(\@result);
}

# Query the database and return only one array,
# useful to queries that return only one colums.
# dbQuerySingle(query)
sub dbQueryRow {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 0 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my @result = ();
	my @row = $sth->fetchrow_array();
	for my $r (0 .. (@row - 1)) {
		push(@result, $row[$r]);
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone(\@result);
}

# Query the database and return only one array,
# useful to queries that return only one colums.
# dbQuerySingle(query)
sub dbQuerySingle {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 1 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my $result = JSON::decode_json(qw<[]>);
	while(my @row = $sth->fetchrow_array()) {
		push @{ $result }, @row[0];
	}
	$sth->finish();
	$dbh->disconnect();
	return dclone($result);
}

# Query the database and return only one element.
# dbQueryAtom(query)
sub dbQueryAtom {
	local $/;
	my $schema = $GAMEPROPERTIES->{'name'};
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = shift;
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 1 })
   or die $DBI::errstr;
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute() or die $DBI::errstr;
	print $DBI::errstr unless ($rv >= 0);
	my @row = $sth->fetchrow_array();
	$sth->finish();
	$dbh->disconnect();
	return $row[0];
}

1;
