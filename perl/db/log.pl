#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/db/dbi.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub confirmDbIsSane {
  writeFile($WDIR.'/.dbsane.sql', '');
}

sub checkDbIsSane {
  return 0 unless ( -f $WDIR.'/.dbsane.sql' );
  return 1;
}

sub dbLogEvent {
  return 1 unless (checkDbIsSane);
  $log_level = shift;
  $log_class = '[PERL] '.shift;
  $log_content = shift;
  dbExecute("select log_event('".$log_level."', '".$log_class."', '".$log_content."');");
}

sub dbBroadcastLogEvent {
  return 1 unless (checkDbIsSane);
  $log_level = shift;
  $log_class = '[PERL] '.shift;
  $log_content = shift;
  dbExecute("select broadcast_log_event('".$log_level."', '".$log_class."', '".$log_content."');");
}

sub dbLogEventInfo {
  $log_class = shift;
  $log_content = shift;
  dbLogEvent('info', $log_class, $content);
}

sub dbLogEventNotice {
  $log_class = shift;
  $log_content = shift;
  dbLogEvent('notice', $log_class, $content);
}

sub dbLogEventDebug {
  $log_class = shift;
  $log_content = shift;
  dbLogEvent('debug', $log_class, $content);
}

1;
