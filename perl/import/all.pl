#!/usr/bin/env perl
	
require "/usr/share/adv/mods/adv-devel/perl/io/file.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/dbi.pl";

#
# Import sql code
#

sub importSqlCode {
  my $dir = shift;
	
	if ( -d $dir ) {
    print "Entering directory ".$dir."\n";
	  local $/;
	  opendir my $dh, $dir;
	  my @files = readdir $dh;
	  closedir $dh;
	  my @sorted_files = sort(@files);
	  for my $i (0 .. (@sorted_files - 1)) {
	    if ($sorted_files[$i] =~ /.*\.sql/) {
	      dbExecuteFile($dir.'/'.$sorted_files[$i]);
	    } elsif ( -d $dir.'/'.$sorted_files[$i]
                and $sorted_files[$i] ne '..'
                and $sorted_files[$i] ne '.' ) {
        my $subdir = $dir."/".$sorted_files[$i];
        print "  Entering subdirectory ".$subdir."\n";
	      opendir my $dh, $subdir;
	      my @subdirfiles = readdir $dh;
	      closedir $dh;
	      my @sorted_subdirfiles = sort(@subdirfiles);
    	  for my $j (0 .. (@sorted_subdirfiles - 1)) {
	        if ($sorted_subdirfiles[$j] =~ /.*\.sql/) {
    	      dbExecuteFile($subdir.'/'.$sorted_subdirfiles[$j]);
          }
        }
      }
	  } # for my $i (0 .. (@files - 1)) ...
	}# if ( -d $dir ) ...
}

# Autoload sql code provided by the mods

sub importSqlAutoload {
	#my @AUTOLOAD = ();
	foreach (@{ $GAMEPROPERTIES->{'mods'} }) {
	  my $mod = $_;
	  my $dir = $WDIR."/mods/".$mod."/sql.d";
	  if ( -d $dir ) {
	    local $/;
	    opendir my $dh, $dir;
	    my @files = readdir $dh;
	    closedir $dh;
	    for my $i (0 .. (@files - 1)) {
	      dbExecuteFile($dir.'/'.$files[$i])
	        if ($files[$i] =~ /.*\.sql/);
	    } # for my $i (0 .. (@files - 1)) ...
	  } # if ( -d $dir ) ...
	} # foreach (@{ $GAMEPROPERTIES->{'mods'} }) ...
}

#
# Import commands
#

sub importCommands {
	my $import_commands = $WDIR."/import_commands.sql";
	writeFile($import_commands, 'set search_path to '.$GAMEPROPERTIES->{'name'}.'; ');
	foreach (@{ $GAMEPROPERTIES->{'mods'} }) {
	  my $mod = $_;
	  my $dir = $WDIR."/mods/".$mod."/commands";
	  if ( -d $dir ) {
	    local $/;
	    opendir my $dh, $dir;
	    my @files = readdir $dh;
	    closedir $dh;
	    for my $i (0 .. (@files - 1)) {
	      if ($files[$i] =~ /.*\.json/) {
	        local $/;
	        open my $fh, '<', $dir.'/'.$files[$i];
	        my $json = <$fh>;
	        close $fh;
	        my $array = JSON::decode_json($json);
	        foreach (@{$array}) { 
	          my $action = $_->{'action'};
	          my $call = $_->{'call'};
	          my $game_phase = $_->{'game_phase'};
	          my $arguments = $_->{'arguments'};
	          my @hints = @{ $_->{'hints'} };
	          my $allows_multi = $_->{'allows_multi'};
	          my $allows_tail = $_->{'allows_tail'};
	          my $launch_as = $_->{'launch_as'};
	          $action = $_->{'action'};
	          $call = $_->{'call'};
	          $game_phase = 'game' unless ($game_phase);
	          $arguments = 0 unless ($arguments);
	          @hints = () unless (@hints);
	          $allows_multi = 'FALSE' unless ($allows_multi);
	          $allows_tail = 'FALSE' unless ($allows_tail);
	          $launch_as = 'api_call' unless ($launch_as);
	    my $command = "insert into cli_actions (
	      action,
	      call,
	      game_phase,
	      arguments,
	      hints,
	      allows_multi,
	      allows_tail,
	      launch_as
	    ) values (
	      '".$action."'::VARCHAR,
	      '".$call."'::VARCHAR,
	      '".$game_phase."'::VARCHAR,
	      ".$arguments.",
	      array['".join("','", @hints)."']::VARCHAR[],
	      ".$allows_multi.",
	      ".$allows_tail.",
	      '".$launch_as."'::VARCHAR
	    ); ";
	    appendFile($import_commands, $command);
	          #->HERE
	        }
	      } # if ($files[$i] =~ /.*\.json/) ...
	    } # for my $i (0 .. (@files - 1)) ...
	  } # if ( -d $dir ) ...
	} # foreach (@{ $GAMEPROPERTIES->{'mods'} }) ...
	dbExecuteFile($import_commands);
}
	
#
# Import words to the dictionary
#
sub importWords {	
	my $import_words = $WDIR."/import_words.sql";
	writeFile($import_words, 'set search_path to '.$GAMEPROPERTIES->{'name'}."; delete from dict_tokens; delete from dict_punctuation; delete from dict_classes;\n");
	foreach (@{ $GAMEPROPERTIES->{'mods'} }) {
	  my $mod = $_;
	  my $dir = $WDIR."/mods/".$mod."/words";
	  if ( -d $dir ) {
	    local $/;
	    opendir my $ldh, $dir;
	    my @langdirs = readdir $ldh;
	    closedir $ldh;
	    for my $l (0 .. (@langdirs - 1)) {
	     my $lang = $langdirs[$l];
	     opendir my $dh, $dir.'/'.$lang;
	     my @files = readdir $dh;
	     closedir $dh;
       my $ord = 0;
	     for my $i (0 .. (@files - 1)) {
	      if ($files[$i] =~ /.*\.words/) {

          my $state_mode = 'idle';
          my @state_punctuation = ();
          my @state_classes = ();
          my @state_tokens = ();
          my @custom_text = ();

	        local $/ = "\n";
	        open(WFH, $dir.'/'.$lang.'/'.$files[$i]);
	
	        foreach my $line (<WFH>) {
          $ord = $ord + 1;
	        if ( $line =~ /^\/\// and $state_mode ne 'dialog') {
	          # Comment
            1;
	        } elsif ( $line =~ /^$/ and $state_mode ne 'dialog') {
	          # Comment
            1;
	        } elsif ( $line =~ /^\[end custom\]/ and $state_mode eq 'custom') {
	          my $command = "insert into dict_custom values ( ".$ord.", '".$state_id."', '".$lang."', array['".join("','", @custom_text)."']::text[] );";
	          appendFile($import_words, $command);
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end dialog\]/ and $state_mode eq 'dialog') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end punctuation\]/ and $state_mode eq 'punctuation') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end world_params\]/ and $state_mode eq 'world_params') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end diaphaneity_codes\]/ and $state_mode eq 'diaphaneity_codes') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end color_codes\]/ and $state_mode eq 'color_codes') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[end shape_codes\]/ and $state_mode eq 'shape_codes') {
            $state_mode = 'idle';
	        } elsif ( $line =~ /^\[custom:.*\]/ and $state_mode ne 'custom') {
            $state_mode = 'custom';
	          chomp($line);
            $line =~ s/^\[//;
            $line =~ s/\]$//;
	          my @fields = split(/:/, $line);
            $state_id = $fields[1];
            @custom_text = ();
	        } elsif ( $line =~ /^\[dialog:.*\]/ and $state_mode ne 'dialog') {
            $state_mode = 'dialog';
	          chomp($line);
            $line =~ s/^\[//;
            $line =~ s/\]$//;
	          my @fields = split(/:/, $line);
            $state_id = $fields[1];
	        } elsif ( $line =~ /^\[punctuation\]/ and $state_mode ne 'punctuation') {
            $state_mode = 'punctuation';
	        } elsif ( $line =~ /^\[diaphaneity_codes\]/ and $state_mode ne 'diaphaneity_codes') {
            $state_mode = 'diaphaneity_codes';
	        } elsif ( $line =~ /^\[color_codes\]/ and $state_mode ne 'color_codes') {
            $state_mode = 'color_codes';
	        } elsif ( $line =~ /^\[shape_codes\]/ and $state_mode ne 'shape_codes') {
            $state_mode = 'shape_codes';
	        } elsif ( $line =~ /^\[world_params\]/ and $state_mode ne 'world_params') {
            $state_mode = 'world_params';
	        } elsif ( $line =~ /^\[.*\]/ and $state_mode ne 'dialog') {
            $state_mode = 'class';
	          chomp($line);
            $line =~ s/^\[//;
            $line =~ s/\]$//;
            $state_id = $line;
	        } elsif ( $line =~ /^<.*>/ and $state_mode ne 'dialog') {
            $state_mode = 'token';
	          chomp($line);
            $line =~ s/^<//;
            $line =~ s/>$//;
            $state_id = $line;
	        } elsif ($state_mode eq 'custom') {
	          chomp($line);
            push(@custom_text, $line);
	        } elsif ($state_mode eq 'dialog' or $state_mode eq 'class') {
	          chomp($line);
	          my $command = "insert into dict_classes values ( ".$ord.", '".$state_id."', '".$lang."', '".$line."' );";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'token') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $context = $fields[0];
            $line =~ s/^$context: //;
	          my $command = "insert into dict_tokens values ( ".$ord.", '".$state_id."', '".$lang."', '".$context."', '".$line."' );";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'punctuation') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $token = $fields[0];
            $line =~ s/^$token: //;
	          my $command = "insert into dict_punctuation values ( '".$token."', '".$lang."', '".$line."' );";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'color_codes') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $token = $fields[0];
            $line =~ s/^$token: //;
	          my $command = "insert into color_codes values ( ".$token."::numeric, '".$line."'::text);";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'shape_codes') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $token = $fields[0];
            $line =~ s/^$token: //;
	          my $command = "insert into shape_codes values ( ".$token."::numeric, '".$line."'::text);";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'diaphaneity_codes') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $token = $fields[0];
            $line =~ s/^$token: //;
	          my $command = "insert into diaphaneity_codes values ( ".$token."::numeric, '".$line."'::text);";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'world_params') {
	          chomp($line);
	          my @fields = split(/: /, $line);
            my $token = $fields[0];
            $line =~ s/^$token: //;
	          my $command = "insert into dict_world_params values ( '".$token."', '".$lang."', '".$line."' );";
	          appendFile($import_words, $command);
	        } elsif ($state_mode eq 'idle') {
            # Do nothing
	        } else {
	          # Default
	          1;
	        } # if ( $line =~ /^:/) ...
	        } # foreach my $line (<WFH>) ...
	        close WFH;
	      } # if ($files[$i] =~ /.*\.words/) ...
	    } #for my $i (0 .. (@files - 1)) ...
	   } #for my $l ...
	  } # if ( -d $dir ) ...
	} # foreach (@{ $GAMEPROPERTIES->{'mods'} }) ...
	dbExecuteFile($import_words);
}
	
# Import tables
sub importTables {
  my $database = 'adv';
  my $userid   = 'adv';
  my $schema   = $GAMEPROPERTIES->{'name'};
  my $retval = system($WDIR.'/mods/jsonlib/bin/import_tables "dbname='.$database.' host='.$DBHOST.' user='.$userid.'" '.$schema.' '.$WDIR);
  return $retval;
}

# Import classes
sub importClasses {
  my $database = 'adv';
  my $userid   = 'adv';
  my $schema   = $GAMEPROPERTIES->{'name'};
  my $retval = system($WDIR.'/mods/jsonlib/bin/import_classes "dbname='.$database.' host='.$DBHOST.' user='.$userid.'" '.$schema.' '.$WDIR.'/mods/adv-devel/json');
  return $retval;
}

1;
