#!/usr/bin/env perl
	
require "/usr/share/adv/mods/adv-devel/perl/io/file.pl";
require "/usr/share/adv/mods/adv-devel/perl/io/json.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/io.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/query.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/utils.pl";
require "/usr/share/adv/mods/adv-devel/perl/run/env.pl";
use POSIX qw(strftime);

$LOGFILE = $WDIR.'/'.$MODNAME.'.log';
$LOGDUMP = $WDIR.'/'.$MODNAME.'.dump';

sub now {
  return strftime('%Y-%m-%d %H:%M:%S', localtime);
}

sub logEvent {
  my $log_level = shift;
  my $log_class = shift;
  my $log_content = shift;
  my $tag = shift;
  my $logfile;
  my $log_ts = now();
  my $string = '['.$log_ts.'] ('.$log_level.') '.$log_class.': '.$log_content."\n";
  if ($tag) {
    my @tags = split(/,/, $tag);
    foreach (@tags) {
      my $t = $_;
      if ($t) {
        $logfile = $WDIR.'/'.$MODNAME.'-'.$t.'.log';
      } else {
        $logfile = $WDIR.'/'.$MODNAME.'.log';
      }
      appendFile($logfile, $string);
    }
  } else {
    $logfile = $WDIR.'/'.$MODNAME.'.log';
    appendFile($logfile, $string); 
  }
}

sub logEventNotice {
  my $log_class = shift;
  my $log_content = shift;
  my $tag = shift;
  logEvent('notice', $log_class, $log_content, $tag);
}

sub logEventDebug {
  my $log_class = shift;
  my $log_content = shift;
  my $tag = shift;
  logEvent('debug', $log_class, $log_content, $tag);
}

sub forkLogEvent {
  my $log_level = shift;
  my $log_class = shift;
  my $log_content = shift;
  my $tag = shift;
  logEvent($log_level, $log_class, $log_content, $tag);
  dbLogEvent($log_level, $log_class, $log_content);
}

sub broadcastLogEvent {
  my $log_level = shift;
  my $log_class = shift;
  my $log_content = shift;
  my $tag = shift;
  logEvent($log_level, $log_class, $log_content, $tag);
  dbBroadcastLogEvent($log_level, $log_class, $log_content);
}

1;
