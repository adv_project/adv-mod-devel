#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/query.pl";

# TODO: this entire file is a mess

#
# Player management
#

sub checkOwner {
	return ($GAMEPROPERTIES->{'owner'} eq $INPUT->{'player'});
}

sub isPlayerRegistered {
	my $player = shift;
	return 0 unless (dbQueryAtom("select id from links where type = 'attribute' and name = 'username' and target = '$player';"));
	return 1;
}

sub registerPlayer {
	my $player = shift;
	my $as_owner = shift;
  my $json;
	if ($as_owner) {
    $json = '{"action":["register_as_owner"], "player":"'.$GAMEPROPERTIES->{'owner'}.'"}';
	} else {
    $json = '{"action":["register_as_player"], "player":"'.$player.'"}';
	}
  my $response = dbQueryAtom("select process_admin_entry('".$json."'::jsonb);");
  resolveGameResponse($response);
}

sub setPlayerConnected {
	my $player = shift;
  my $id = dbQueryAtom("select id from links where type = 'attribute' and name = 'username' and target = '".$player."';");
	dbExecute("select initialize_boolean_attribute('".$id."', 'connected'); update players set connected = true, last_connected = ".int(time())." where id = '".$id."';");
}

sub setPlayerDisconnected {
	my $player = shift;
  my $id = dbQueryAtom("select id from links where type = 'attribute' and name = 'username' and target = '".$player."';");
	dbExecute("select initialize_boolean_attribute('".$id."', 'connected'); update players set connected = false, last_connected = ".int(time())." where id = '".$id."';");
}

sub getPlayerPhase {
	my $player = shift;
  my $id = dbQueryAtom("select id from links where type = 'attribute' and name = 'username' and target = '".$player."';");
  my $phase = dbQueryAtom("select target from links where id = '".$id."' and type = 'attribute' and name = 'game_phase';");
	return $phase;
}

1;
