#!/usr/bin/env perl
	
require "/usr/share/adv/mods/adv-devel/perl/io/file.pl";
require "/usr/share/adv/mods/adv-devel/perl/io/json.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/io.pl";
require "/usr/share/adv/mods/adv-devel/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/schema.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/query.pl";
require "/usr/share/adv/mods/adv-devel/perl/import/all.pl";
require "/usr/share/adv/mods/adv-devel/perl/players/all.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/utils.pl";
require "/usr/share/adv/mods/adv-devel/perl/run/env.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/messages.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/responses.pl";
require "/usr/share/adv/mods/adv-devel/perl/db/log.pl";
require "/usr/share/adv/mods/adv-devel/perl/awg/all.pl";
require "/usr/share/adv/mods/adv-devel/perl/logs/all.pl";

1;
