#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/io/file.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub writeConfig {
  my $json = JSON->new->pretty(1)->encode($CONFIG);
  writeFile($CONFIGFILE, $json);
}

# Define global paths
$WDIR = $ARGV[0];
$MODNAME = "adv-devel" unless ($MODNAME);
$MODDIR = $WDIR."/mods/".$MODNAME;
$OUTPUTFILE = $WDIR . '/output.json';
$INPUTFILE = $WDIR . '/input.json';              $INPUT;
$LOGLEVEL = 5;  # Set it for now, until we read the config
$CONFIGFILE = $WDIR."/config.json";              $CONFIG;
$PROPERTIESFILE = $MODDIR."/properties.json";    $PROPERTIES;
$GAMEPROPERTIESFILE = $WDIR."/properties.json";  $GAMEPROPERTIES;  $DBHOST;
$INPUT;

# The init sequence needs these
$WORLDEDIT = $WDIR."/worldedit.json";

#TODO: processing input and output should be done/initialized from a function
# Output to server
writeFile($OUTPUTFILE, qw<[]>);

if ((-e $INPUTFILE) and (-s $INPUTFILE > 5)) {
  my $json = readFile($INPUTFILE);
  $INPUT = JSON::decode_json($json);
}

#
# Set the environment
#

# Global data structure to store logging state
$LOGSTATE = {};


#TODO: should go to a separate file
# Configuration file
$CONFIGFILE = $MODDIR."/config.json" unless (-f $CONFIGFILE);
{
  my $json = readFile($CONFIGFILE);
  $CONFIG = JSON::decode_json($json);
  unless ($CONFIG->{'birth'}) {
    $CONFIG->{'birth'} = time();
    my @ignore = ("update");
    $CONFIG->{'debug-ignore'} = dclone(\@ignore);
    $CONFIGFILE = $WDIR . "/config.json";
    writeConfig();
  }
}

#TODO: these too
# Mod properties
my $modprop = readFile($PROPERTIESFILE);
$PROPERTIES = JSON::decode_json($modprop);

# Game properties
my $gameprop = readFile($GAMEPROPERTIESFILE);
$GAMEPROPERTIES = JSON::decode_json($gameprop);
$DBHOST = $GAMEPROPERTIES->{'dbhost'};

# Read log level from config
$LOGLEVEL = $CONFIG->{'log-level'};

# Color constants
$RESET = "\0110";
$BOLD = "\0112";
$ITALIC = "\0113";
$UNDERLINE = "\0114";
$NOTBOLD = "\0115";
$NOTITALIC = "\0116";
$NOTUNDERLINE = "\0117";
$RED = "\011r";
$GREEN = "\011g";
$YELLOW = "\011y";
$BLUE = "\011b";
$MAGENTA = "\011m";
$CYAN = "\011c";
$WHITE = "\011w";
$POSITIVE = "\011p";
$NEGATIVE = "\011n";

# Gonfiguration options (in principle, configurable per player, but we'll see)
$COL0 = $RESET;
$COL1 = $BOLD.$WHITE;
$COL2 = $BOLD.$GREEN;
$COL3 = $BOLD.$YELLOW;
$COL4 = $BOLD.$MAGENTA;

1;
