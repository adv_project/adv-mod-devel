#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-devel/perl/io/json.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub outputPush {
	my $object = shift;
	my $json = readFile($OUTPUTFILE);
	my $out = JSON::decode_json($json);
	push @{ $out }, $object;
	$json = JSON->new->pretty(1)->encode( dclone($out) );
  logEventDebug('io.pl:15 outputPush()', "Pushing API call:\n".$json, 'API');
	writeFile($OUTPUTFILE, $json);
}

sub manage_error_badly {
	local $/;
	open my $fh, '>', $WDIR.'/error.json';
	print $fh qw<[]>;
	close $fh;
  logEventDebug('io.pl:25 manage_error_badly()', '[]', 'API');
}

1;
