# Development version of Adv mod

## Proposed changes to naming conventions in json files

Some of the following json field names should be more descriptive, even self-explanatory:

#### triggers

>>>
_(The trigger) [...] emulates a simple 'script' execution (the script being the trigger)
within a given environment (the buffer). The call consists of the trigger id
and the json buffer.
The trigger reads all necessary data from the buffer, proceeds to execute the
instructions defined in each condition in sequence, and stores the consecutive
results in the buffer. At the end of the cycle, the updated buffer is returned,
possibly to be passed as environment to another trigger.
Each trigger define a task (mandatory), and optionally a type and an array of tags
that regulate the trigger's behavior._
>>>

**Proposed alternative names:** _procedure_

#### task

The job assigned to the trigger; may be a command, a hook, a procedure, etc. Some examples:
`walk`,
`hook_start_game`,
`help`,
`spawn_angry_bear`.

**Proposed alternative names:** _(add your suggestions here)_

#### conditions

>>>
_Conditions are to be thought as lines of a script._
>>>

**Proposed alternative names:** _expression, expr_

#### condition -> query

>>>
_[...] serves as a preliminary command to provide the test with the necessary context without
resorting to another condition._
>>>

**Proposed alternative names:** _(add your suggestions here)_

#### condition -> test

>>>
_[...] may be thought as the 'command' to be executed in this 'line of code' (the
condition). It takes into account the query result, if any, and processes the
buffer as needed, updating the buffer with the results._
>>>

**Proposed alternative names:** _(add your suggestions here)_

#### condition -> arg, value, amount

These fields may be thought as arguments passed to the condition;
`arg` is a string, while `value` and `amount` are numeric values.

**Proposed alternative names:** _(add your suggestions here)_

#### condition -> write

>>>
_[...] this is the name of the buffer field to be written by the test._
>>>

**Proposed alternative names:** _(add your suggestions here)_

### Hints

Since one of the objectives is to generate these classes from a plain text file,
seems a good idea that the trigger definitions look like a program:
one condition per line, plus a header with the trigger-specific fields.

Here goes a draft of the hypothetical text file, with the naming conventions
already in place (lines starting with `//` are comments; inline comments should also work):

```
trigger label_for_this_trigger #some_tag #another_tag

task some_job
type trigger_type

begin

// from now on, each line represents a condition
// labels are generated automatically as line1, line2, ...
cond_test(cond_query) arg:cond_arg value:cond_value > write_field
cond_test() amount:cond_amount > write_field // this condition has no query
// we could define labels like this:
[label] cond_test(cond_query) arg:cond_arg > write_field
// and type like this:
*cond_type cond_test(cond_query) arg:cond_arg value:cond_value > write_field
// tags:
cond_test() #tag1 #tag2 > write_field

end trigger
```

Another example, implementing randomly some of the procedures already implemented
in the mod:

```
trigger initialize_world #run_on_init #admin

task hook_init
type admin_hook

begin

jwrite_query(qpick_world_id) > world-id
jwrite_arg() arg:@player > owner-username
*if jcheck_world_is_sane(@world-id) #abort_on_failure
// some more code
*endif
// ...

end trigger
```

---

## ToDo

- Knowledge, memories, experience
- Autohelp, hints
- Character creation menu
- Split into definitive mods
- Clean and develop storyline
  - Score system for tiles (initial, special...)
- Populate and diversify world map
- Order and rationalize multiple word outputs
- Support all special symbols in perl parser (avoid db conflicts), e.g. "'"
- Use ``type`` as message type in ``jtrigger_response`` tests, e.g. for errors
- Add width and height as world parameters for grid geometry
- Fix object recognition

## Proposed actions

- ``go [direction]`` - *displace in the given direction if possible*
- ``leave, go outside`` - *exit current parent if possible*
- ``walk, hike, climb, descend, fall [direction]`` - *displace (in land)*
- ``swim [direction]`` - *displace (in water)*
- ``dive, fly`` - *possible under certain conditions*
- ``sail, ride [direction]`` - *displace (using vehicle)*
- ``hide, flee, run, sneak`` - *use when in danger, decrease risk of being hurt or noticed*
- ``pick, drop, wield, grip, wear [object]`` - *take and drop available objects*
- ``put [object] in [object]`` - *make use of containers*
- ``move, open, close [object]`` - *special action to be performed on some objects*
- ``turn [on|off] [object]`` - *special action to be performed on some objects*
- ``inflate, deflate, ignite, ... [object]`` - *special actions to be performed on some objects*
- ``look, explore, diagnose, analyse, inventory`` - *display or discover features*
- ``eat, drink [object]`` - *take some nourishment*
- ``rest, wait, sleep`` - *let time pass*
- ``try [object]`` - *learn some technique by experimenting with objects*
- ``build [object]`` - *build something using some tools and materials*
- ``say [words]`` - *expel articulate sounds hoping they will be heard*
- ``read [object]`` - *to be performed over objects containing text*
- ``write [object] [text]`` - *put some text on objects*
- ``again`` - *repeat last action if possible*
