CREATE OR REPLACE FUNCTION update_inherited(object_id varchar) RETURNS VOID AS $$
  DECLARE
    i varchar;
    t varchar;
  BEGIN
    delete from inherited i where i.id = object_id;
    for i in (select id from get_all_inherited(object_id))
    loop
      insert into inherited (id, inherits) values (object_id, i);
    end loop;
    delete from tagged t where t.id = object_id;
    for i in (select inherits from inherited where id = object_id)
    loop
      for t in (select tag from get_tags(i))
      loop
        if t is not null and t <> '' then
          insert into tagged (id, tag) values (object_id, t);
        end if;
      end loop;
    end loop;
    insert into inherited (id, inherits) values (object_id, 'everything');
    return;
  END;
$$ LANGUAGE plpgsql;

