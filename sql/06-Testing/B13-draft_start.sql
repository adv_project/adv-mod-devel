CREATE OR REPLACE FUNCTION draft_start(
  buffer JSONB,
  query         JSONB,
  condition_id  VARCHAR
) RETURNS JSONB AS $$
  DECLARE
    player_id           VARCHAR;
    selected_species    VARCHAR;
    selected_race       VARCHAR;
    selected_gender     VARCHAR;
    selected_class      VARCHAR;
    current_classes     VARCHAR[];
    updated_classes     VARCHAR[];
    result      JSONB;
  BEGIN
    result := '{}'::jsonb;
    player_id := buffer->>'player_id';

    /* Set game phase -- you'll be a condition soon... (with music by Urge Overkill) */
    update links set target = 'game' where id = buffer->>player_id and type = 'attribute' and name = 'game_phase';

    /* Write objects->classes[] for this player */
    select target from links where id = player_id and type = 'init' and name = 'species' into selected_species;
    select target from links where id = player_id and type = 'init' and name = 'race' into selected_race;
    select target from links where id = player_id and type = 'init' and name = 'gender' into selected_gender;
    select target from links where id = player_id and type = 'init' and name = 'class' into selected_class;
    select classes from objects where id = player_id into current_classes;
    updated_classes := current_classes || array[
      selected_species,
      selected_race,
      selected_gender,
      selected_class
    ]::varchar[];
    update objects set classes = array(
      select distinct unnest(updated_classes)
    ) where id = player_id;
    perform update_inherited(player_id);

    /* Greet this player with his/her awful fate */
    perform push_response(buffer);

    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

