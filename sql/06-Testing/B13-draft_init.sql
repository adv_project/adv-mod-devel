CREATE OR REPLACE FUNCTION draft_init(
  buffer JSONB,
  query         JSONB,
  condition_id  VARCHAR
) RETURNS JSONB AS $$
  DECLARE
    player_id       VARCHAR;
    result          JSONB;
    initial_tile    VARCHAR;
    this_id varchar;
  BEGIN
    result := '{}'::jsonb;

    /* TODO: integrate these with awg/iomport sequence */

		select target from links
		where type = 'init' and name = 'initial_tile'
		into initial_tile;

    /* worst possible brute-force way to put objects in the world */
    if not exists (select id from objects where 'mailbox' = any(classes))
    then
      select do_create_object(array['mailbox']::varchar[], false)
      into this_id;
      insert into links (id, target, type, name) values (this_id, initial_tile, 'parent', 'in_place');
      select do_create_object(array['mailbox']::varchar[], false)
      into this_id;
      insert into links (id, target, type, name) values (this_id, initial_tile, 'parent', 'in_place');
      select do_create_object(array['leaflet']::varchar[], false)
      into this_id;
      insert into links (id, target, type, name) values (this_id, initial_tile, 'parent', 'in_place');
      select do_create_object(array['air_conditioner']::varchar[], false)
      into this_id;
      insert into links (id, target, type, name) values (this_id, initial_tile, 'parent', 'in_place');
      select do_create_object(array['engineer_diplomma']::varchar[], false)
      into this_id;
      insert into links (id, target, type, name) values (this_id, initial_tile, 'parent', 'in_place');
    end if;
    
    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

