CREATE OR REPLACE FUNCTION populate_world_map() RETURNS VOID AS $$
  DECLARE
    r record;
    initial_tile_count integer;
    candidates_count integer;
    initial_tile    VARCHAR;
    initial_tile_ecoregion    VARCHAR;

    w varchar;
    o varchar;
    e varchar;
    e_name varchar;
    i varchar;
    t varchar;
    v numeric;
    c record;
    n record;
    no_crossing boolean;
    ncbuf record;
  BEGIN
    -- Initialize the neighborhood
    drop table if exists neighborhood;
    create table neighborhood as
      select * from make_neighborhood;

/*
 *  CREATE AUXILIARY TABLES
 */
    CREATE TEMP TABLE already_connected (
      origin_id     VARCHAR,
      neighbour_id  VARCHAR
    );

    create temp table fake_neighborhood (
      origin varchar,
      origin_classes varchar[],
      neighbour varchar,
      neighbour_classes varchar[],
      direction varchar,
      biomes_match boolean,
      origin_is_flooded boolean,
      neighbour_is_flooded boolean,
      altitude_gradient numeric,
      temperature_gradient numeric,
      origin_proto_ecoregion varchar,
      neighbour_proto_ecoregion varchar
    ) on commit drop;

/*
 *  ADD COASTLINES AND PATHWAYS
 */
    /* Rework all connections between tiles */
    for r in (select * from neighborhood)
    loop
      if not exists (
        select a.origin_id from already_connected a
        where a.origin_id = r.neighbour
        and a.neighbour_id = r.origin
      )
      then
        perform make_connection_between_tiles(r);

        insert into already_connected
          ( origin_id, neighbour_id )
        values
          ( r.origin, r.neighbour );
      end if;
    end loop;

    /* Connect adjacent coastlines */
    for r in (select tile_id, coast_id, direction from coastline_buffer)
    loop
/**** COAST HEADS NORTH ****/
      if r.direction = 'north'
      then
        /* Connect to adjacent coasts in the same tile */
        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'east'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'south_east', 1);
        end if;

        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'west'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'south_west', 1);
        end if;

/**** COAST HEADS NORTH ****/
        /* Connect to adjacent coasts in neighbouring tiles */
        for n in
          select
            nbuf.neighbour as neighbour_tile,
            nbuf.direction as neighbour_direction,
            cbuf.coast_id  as coast_id,
            cbuf.direction as coast_direction
          from neighborhood nbuf, coastline_buffer cbuf
          where nbuf.neighbour = cbuf.tile_id
          and   nbuf.origin    = r.tile_id
        loop
          if n.neighbour_direction = 'east' and n.coast_direction = 'north'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'east', 1);
          elsif n.neighbour_direction = 'west' and n.coast_direction = 'north'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'west', 1);
          elsif n.neighbour_direction = 'east' and n.coast_direction = 'west'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south_east', 1);
          elsif n.neighbour_direction = 'west' and n.coast_direction = 'east'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south_west', 1);
/**** COAST HEADS NORTH ****/
          else
            select r.tile_id as origin, n.neighbour_direction as direction into ncbuf;
            select check_no_crossing(ncbuf) into no_crossing;
            if no_crossing is true
            and n.neighbour_direction = 'north_east'
            and n.coast_direction = 'west'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'north_east', 1);
            elsif no_crossing is true
            and n.neighbour_direction = 'north_west'
            and n.coast_direction = 'east'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'north_west', 1);
            end if;
          end if;
        end loop;
/**** COAST HEADS SOUTH ****/
      elsif r.direction = 'south'
      then
        /* Connect to adjacent coasts in the same tile */
        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'east'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'north_east', 1);
        end if;

        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'west'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'north_west', 1);
        end if;

/**** COAST HEADS SOUTH ****/
        /* Connect to adjacent coasts in neighbouring tiles */
        for n in
          select
            nbuf.neighbour as neighbour_tile,
            nbuf.direction as neighbour_direction,
            cbuf.coast_id  as coast_id,
            cbuf.direction as coast_direction
          from neighborhood nbuf, coastline_buffer cbuf
          where nbuf.neighbour = cbuf.tile_id
          and   nbuf.origin    = r.tile_id
        loop
          if n.neighbour_direction = 'east' and n.coast_direction = 'south'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'east', 1);
          elsif n.neighbour_direction = 'west' and n.coast_direction = 'south'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'west', 1);
          elsif n.neighbour_direction = 'east' and n.coast_direction = 'west'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north_east', 1);
          elsif n.neighbour_direction = 'west' and n.coast_direction = 'east'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north_west', 1);
/**** COAST HEADS SOUTH ****/
          else
            select r.tile_id as origin, n.neighbour_direction as direction into ncbuf;
            select check_no_crossing(ncbuf) into no_crossing;
            if no_crossing is true
            and n.neighbour_direction = 'south_east'
            and n.coast_direction = 'west'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'south_east', 1);
            elsif no_crossing is true
            and n.neighbour_direction = 'south_west'
            and n.coast_direction = 'east'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'south_west', 1);
            end if;
          end if;
        end loop;
/**** COAST HEADS EAST ****/
      elsif r.direction = 'east'
      then
        /* Connect to adjacent coasts in the same tile */
        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'north'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'north_west', 1);
        end if;

        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'south'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'south_west', 1);
        end if;

/**** COAST HEADS EAST ****/
        /* Connect to adjacent coasts in neighbouring tiles */
        for n in
          select
            nbuf.neighbour as neighbour_tile,
            nbuf.direction as neighbour_direction,
            cbuf.coast_id  as coast_id,
            cbuf.direction as coast_direction
          from neighborhood nbuf, coastline_buffer cbuf
          where nbuf.neighbour = cbuf.tile_id
          and   nbuf.origin    = r.tile_id
        loop
          if n.neighbour_direction = 'north' and n.coast_direction = 'east'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north', 1);
          elsif n.neighbour_direction = 'south' and n.coast_direction = 'east'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south', 1);
          elsif n.neighbour_direction = 'north' and n.coast_direction = 'south'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north_west', 1);
          elsif n.neighbour_direction = 'south' and n.coast_direction = 'north'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south_west', 1);
/**** COAST HEADS EAST ****/
          else
            select r.tile_id as origin, n.neighbour_direction as direction into ncbuf;
            select check_no_crossing(ncbuf) into no_crossing;
            if no_crossing is true
            and n.neighbour_direction = 'north_east'
            and n.coast_direction = 'south'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'north_east', 1);
            elsif no_crossing is true
            and n.neighbour_direction = 'south_east'
            and n.coast_direction = 'north'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'south_east', 1);
            end if;
          end if;
        end loop;
/**** COAST HEADS WEST ****/
      elsif r.direction = 'west'
      then
        /* Connect to adjacent coasts in the same tile */
        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'north'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'north_east', 1);
        end if;

        select cbuf.coast_id from coastline_buffer cbuf
        where cbuf.tile_id = r.tile_id and cbuf.direction = 'south'
        into i;
        if i is not null
        then
          insert into pathways ( origin, destination, action, direction, difficulty )
          values ( r.coast_id, i, 'walk', 'south_east', 1);
        end if;

/**** COAST HEADS WEST ****/
        /* Connect to adjacent coasts in neighbouring tiles */
        for n in
          select
            nbuf.neighbour as neighbour_tile,
            nbuf.direction as neighbour_direction,
            cbuf.coast_id  as coast_id,
            cbuf.direction as coast_direction
          from neighborhood nbuf, coastline_buffer cbuf
          where nbuf.neighbour = cbuf.tile_id
          and   nbuf.origin    = r.tile_id
        loop
          if n.neighbour_direction = 'north' and n.coast_direction = 'west'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north', 1);
          elsif n.neighbour_direction = 'south' and n.coast_direction = 'west'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south', 1);
          elsif n.neighbour_direction = 'north' and n.coast_direction = 'south'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'north_east', 1);
          elsif n.neighbour_direction = 'south' and n.coast_direction = 'north'
          then
            insert into pathways ( origin, destination, action, direction, difficulty )
            values ( r.coast_id, n.coast_id, 'walk', 'south_east', 1);
/**** COAST HEADS WEST ****/
          else
            select r.tile_id as origin, n.neighbour_direction as direction into ncbuf;
            select check_no_crossing(ncbuf) into no_crossing;
            if no_crossing is true
            and n.neighbour_direction = 'north_west'
            and n.coast_direction = 'south'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'north_west', 1);
            elsif no_crossing is true
            and n.neighbour_direction = 'south_west'
            and n.coast_direction = 'north'
            then
              insert into pathways ( origin, destination, action, direction, difficulty )
              values ( r.coast_id, n.coast_id, 'walk', 'south_west', 1);
            end if;
          end if;
        end loop;
      end if;
    end loop;
    
/*
 *  PICK INITIAL TILE
 *  TODO: pick also other relevant places
 */
    /* Pick the initial tile - this should be part of the storyline */
    select count(t.id) from tiles t, objects o
    where o.id = t.id and t.initial_tile_index <> 0
    into initial_tile_count;

    select (initial_tile_count / 20)::integer
    into candidates_count;

    if candidates_count < 1
    then candidates_count := 1;
    end if;

    CREATE TEMP TABLE initial_tile_candidates ON COMMIT DROP AS
    select t.id, t.initial_tile_index from tiles t, objects o
    where o.id = t.id
    order by initial_tile_index desc
    limit candidates_count;

    select id from initial_tile_candidates
    order by random()
    limit 1
    into initial_tile;

    /* Create initial_tile ecoregion object */
    select create_object(array[]::varchar[] || 'initial_tile'::varchar) into initial_tile_ecoregion;
    /* Assign the initial tile to initial_tile ecoregion */
    insert into links (id, type, name, target) values (initial_tile, 'ecoregion', 'border', initial_tile_ecoregion);
    /* Give the initial tile the init string attribute 'initial_tile' */
    /* TODO: deprecate this, use ecoregion instead */
    insert into links (id, type, name, target) values (initial_tile, 'init', 'initial_tile', initial_tile);

/*
 *  DISCARD AUXILIARY TABLES
 */
    /* Drop the junk */
    DROP TABLE fake_neighborhood;
    DROP TABLE already_connected;
    DROP TABLE initial_tile_candidates;
  END;
$$ LANGUAGE plpgsql;

