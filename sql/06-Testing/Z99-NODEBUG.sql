CREATE OR REPLACE FUNCTION log_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content VARCHAR
)
RETURNS VOID AS $$
  BEGIN
--    INSERT INTO logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_content
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_parser_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    INSERT INTO parser_logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_content,
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_word_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    perform log_parser_event(
--      'debug',
--      log_class,
--      concat('=> ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_datum_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    perform log_parser_event(
--      'debug',
--      log_class,
--      concat('  -> ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_extractor_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    perform log_parser_event(
--      'debug',
--      log_class,
--      concat(' :: ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_identify_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    perform log_parser_event(
--      'info',
--      log_class,
--      concat(' => '::text, log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_trigger_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_trace   VARCHAR,
  log_content TEXT,
  log_dump    JSONB DEFAULT '{}'::JSONB
)
RETURNS VOID AS $$
  DECLARE j text;
  BEGIN
--    if log_dump = '{}'::jsonb
--    then j := ''::text;
--    else j := jsonb_pretty(log_dump);
--    end if;
--
--    INSERT INTO trigger_logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_trace,
--      log_content,
--      j
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_query_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    JSONB DEFAULT '{}'::JSONB
)
RETURNS VOID AS $$
  BEGIN
--    perform log_trigger_event(
--      'debug',
--      log_class,
--      '     [QUERY]',
--      concat('   ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_test_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    JSONB DEFAULT '{}'::JSONB
)
RETURNS VOID AS $$
  BEGIN
--    perform log_trigger_event(
--      'debug',
--      log_class,
--      '     [TEST]',
--      concat('   ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_internal_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    JSONB DEFAULT '{}'::JSONB
)
RETURNS VOID AS $$
  BEGIN
--    perform log_trigger_event(
--      'debug',
--      log_class,
--      '      [INTERNAL]',
--      concat('    ', log_content),
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_trigger_stack_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content TEXT
)
RETURNS VOID AS $$
  BEGIN
--    INSERT INTO trigger_stack_logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_content
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_responses_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    INSERT INTO responses_logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_content,
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_responses_stack_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    JSONB DEFAULT '{}'::JSONB
)
RETURNS VOID AS $$
  DECLARE j text;
  BEGIN
--    if log_dump = '{}'::jsonb
--    then j := ''::text;
--    else j := jsonb_pretty(log_dump);
--    end if;
--
--    INSERT INTO responses_stack_logs
--    VALUES (
--      now(),
--      log_level,
--      log_class,
--      log_content,
--      j
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_debug_event(
  log_class   VARCHAR,
  log_content TEXT,
  log_dump    TEXT DEFAULT ''::TEXT
)
RETURNS VOID AS $$
  BEGIN
--    INSERT INTO debug_logs
--    VALUES (
--      log_class,
--      log_content,
--      log_dump
--    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION broadcast_log_event(
  log_level   VARCHAR,
  log_class   VARCHAR,
  log_content TEXT
)
RETURNS VOID AS $$
  BEGIN
--    perform log_event(log_level, log_class, log_content);
--    perform log_parser_event(log_level, log_class, log_content);
--    perform log_trigger_event(log_level, log_class, '* BROADCAST *', log_content);
--    perform log_trigger_stack_event(log_level, log_class, log_content);
--    perform log_responses_event(log_level, log_class, log_content);
--    perform log_responses_stack_event(log_level, log_class, log_content);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_trigger_stack_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                10000 INTO threshold;
		SELECT count (ts) FROM trigger_stack_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM trigger_stack_logs WHERE ts IN
        ( SELECT ts FROM trigger_stack_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_trigger_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                50000 INTO threshold;
		SELECT count (ts) FROM trigger_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM trigger_logs WHERE ts IN
        ( SELECT ts FROM trigger_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_responses_stack_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                10000 INTO threshold;
		SELECT count (ts) FROM responses_stack_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM responses_stack_logs WHERE ts IN
        ( SELECT ts FROM responses_stack_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_responses_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                10000 INTO threshold;
		SELECT count (ts) FROM responses_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM responses_logs WHERE ts IN
        ( SELECT ts FROM responses_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_parser_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                10000 INTO threshold;
		SELECT count (ts) FROM parser_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM parser_logs WHERE ts IN
        ( SELECT ts FROM parser_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_debug_logs()
RETURNS INTEGER AS $$	
  DECLARE
		n         INTEGER;
		threshold INTEGER;
		overflow  INTEGER;	
  BEGIN
		SELECT                 1000 INTO threshold;
		SELECT count (log_class) FROM debug_logs INTO n;
		SELECT        n - threshold INTO overflow;

		IF overflow > 0
    THEN DELETE FROM debug_logs WHERE ts IN
        ( SELECT log_class FROM debug_logs LIMIT overflow );
			RETURN overflow;
		ELSE
			RETURN 0;
		END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION purge_all_logs()
RETURNS VOID AS $$	
  DECLARE
		n         INTEGER;
  BEGIN
		SELECT purge_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table logs'));
--    END IF;

		SELECT purge_trigger_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table trigger_logs'));
--    END IF;

		SELECT purge_trigger_stack_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table trigger_stack_logs'));
--    END IF;

		SELECT purge_responses_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table responses_logs'));
--    END IF;

		SELECT purge_responses_stack_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table responses_stack_logs'));
--    END IF;

		SELECT purge_parser_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table parser_logs'));
--    END IF;

		SELECT purge_debug_logs() INTO n;
--    IF n > 0 THEN
--      PERFORM log_event('info', 'purge_all_logs',
--        concat('Purged ', n::text, 'logs from table debug_logs'));
--    END IF;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION reset_log_commits()
RETURNS VOID AS $$	
  BEGIN
--	UPDATE logs                 SET last_commit = false;
		UPDATE trigger_logs         SET last_commit = false;
		UPDATE trigger_stack_logs   SET last_commit = false;
		UPDATE responses_logs       SET last_commit = false;
		UPDATE responses_stack_logs SET last_commit = false;
		UPDATE parser_logs          SET last_commit = false;
		UPDATE debug_logs           SET last_commit = false;
  END;
$$ LANGUAGE plpgsql;


