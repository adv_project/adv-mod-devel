/* Fix depth for deep ocean tiles adjacent to coastlines */
CREATE OR REPLACE FUNCTION fix_ocean_depth(
  r record
) RETURNS VOID AS $$
  BEGIN
    perform log_event('warning', '[INTERNAL]', 'fix_ocean_depth(): not implemented');
  END;
$$ LANGUAGE plpgsql;

