CREATE OR REPLACE FUNCTION opposite_direction(
  d VARCHAR
) RETURNS VARCHAR AS $$
  BEGIN
    if    d = 'north'     then return 'south';
    elsif d = 'south'     then return 'north';
    elsif d = 'east'      then return 'west';
    elsif d = 'west'      then return 'east';
    elsif d = 'north_east'  then return 'south_west';
    elsif d = 'north_west'  then return 'south_east';
    elsif d = 'south_east'  then return 'north_west';
    elsif d = 'south_west'  then return 'north_east';
    else return 'unknown';
    end if;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION evaluate_neighbours(
  r record
) RETURNS INTEGER AS $$
  DECLARE
    orig varchar;
    dest varchar;
  BEGIN
    select r.origin_proto_ecoregion    into orig;
    select r.neighbour_proto_ecoregion into dest;
    if    ( orig = 'oceanic_watermass'            and dest = 'oceanic_watermass' )
       then return 1;
    elsif ( orig = 'inland_watermass'             and dest = 'inland_watermass' )
       then return 2;
    elsif ( orig = 'inland_landlocked_watermass'  and dest = 'inland_landlocked_watermass' )
       then return 3;
    elsif ( orig = 'oceanic_landmass'             and dest = 'oceanic_landmass' )
       then return 4;
    elsif ( orig = 'steep_terrain'                and dest = 'steep_terrain' )
       then return 5;
    elsif ( orig = 'wild_terrain'                 and dest = 'wild_terrain' )
       then return 6;
    elsif ( orig = 'oceanic_watermass'            and dest = 'inland_watermass' )
       or ( dest = 'oceanic_watermass'            and orig = 'inland_watermass' )
       then return 12;
    elsif ( orig = 'oceanic_watermass'            and dest = 'inland_landlocked_watermass' )
       or ( dest = 'oceanic_watermass'            and orig = 'inland_landlocked_watermass' )
       then return 13;
    elsif ( orig = 'oceanic_watermass'            and dest = 'oceanic_landmass' )
       or ( dest = 'oceanic_watermass'            and orig = 'oceanic_landmass' )
       then return 14;
    elsif ( orig = 'oceanic_watermass'            and dest = 'steep_terrain' )
       or ( dest = 'oceanic_watermass'            and orig = 'steep_terrain' )
       then return 15;
    elsif ( orig = 'oceanic_watermass'            and dest = 'wild_terrain' )
       or ( dest = 'oceanic_watermass'            and orig = 'wild_terrain' )
       then return 16;
    elsif ( orig = 'inland_watermass'             and dest = 'inland_landlocked_watermass' )
       or ( dest = 'inland_watermass'             and orig = 'inland_landlocked_watermass' )
       then return 23;
    elsif ( orig = 'inland_watermass'             and dest = 'oceanic_landmass' )
       or ( dest = 'inland_watermass'             and orig = 'oceanic_landmass' )
       then return 24;
    elsif ( orig = 'inland_watermass'             and dest = 'steep_terrain' )
       or ( dest = 'inland_watermass'             and orig = 'steep_terrain' )
       then return 25;
    elsif ( orig = 'inland_watermass'             and dest = 'wild_terrain' )
       or ( dest = 'inland_watermass'             and orig = 'wild_terrain' )
       then return 26;
    elsif ( orig = 'inland_landlocked_watermass'  and dest = 'oceanic_landmass' )
       or ( dest = 'inland_landlocked_watermass'  and orig = 'oceanic_landmass' )
       then return 34;
    elsif ( orig = 'inland_landlocked_watermass'  and dest = 'steep_terrain' )
       or ( dest = 'inland_landlocked_watermass'  and orig = 'steep_terrain' )
       then return 35;
    elsif ( orig = 'inland_landlocked_watermass'  and dest = 'wild_terrain' )
       or ( dest = 'inland_landlocked_watermass'  and orig = 'wild_terrain' )
       then return 36;
    elsif ( orig = 'oceanic_landmass'             and dest = 'steep_terrain' )
       or ( dest = 'oceanic_landmass'             and orig = 'steep_terrain' )
       then return 45;
    elsif ( orig = 'oceanic_landmass'             and dest = 'wild_terrain' )
       or ( dest = 'oceanic_landmass'             and orig = 'wild_terrain' )
       then return 46;
    elsif ( orig = 'steep_terrain'                and dest = 'wild_terrain' )
       or ( dest = 'steep_terrain'                and orig = 'wild_terrain' )
       then return 56;
    else    return 99;
    end if;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_same_ecoregion(
  r record
) RETURNS BOOLEAN AS $$
  BEGIN
    return r.origin_proto_ecoregion = r.neighbour_proto_ecoregion;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_same_biome(
  r record
) RETURNS BOOLEAN AS $$
  BEGIN
    return r.origin_classes = r.neighbour_classes;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_straight_direction(
  d VARCHAR
) RETURNS BOOLEAN AS $$
  BEGIN
    return d not like '%\_%';
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION check_no_crossing(
  r record
) RETURNS BOOLEAN AS $$
  DECLARE
    straight_direction boolean;
    cw_dir varchar;
    ccw_dir varchar;
    cw_is_flooded boolean;
    ccw_is_flooded boolean;
  BEGIN
    select check_straight_direction(r.direction) into straight_direction;

    if straight_direction is true then return true; end if;

    if    r.direction = 'north_east' then cw_dir := 'east';  ccw_dir := 'north';
    elsif r.direction = 'north_west' then cw_dir := 'north'; ccw_dir := 'west';
    elsif r.direction = 'south_west' then cw_dir := 'west';  ccw_dir := 'south';
    elsif r.direction = 'south_east' then cw_dir := 'south'; ccw_dir := 'east';
    else return false;
    end if;

    select neighbour_is_flooded from neighborhood
    where origin = r.origin and direction = cw_dir
    into cw_is_flooded;

    select neighbour_is_flooded from neighborhood
    where origin = r.origin and direction = ccw_dir
    into ccw_is_flooded;

    if cw_is_flooded is not true or ccw_is_flooded is not true
    then return true;
    else return false;
    end if;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION evaluate_alt_gradient(
  g NUMERIC
) RETURNS INTEGER AS $$
  BEGIN
    if      g < -1350                then  return  -2;    -- wild downward
    elsif   g > -1351 and g < -350   then  return  -1;    -- steep downward
    elsif   g >  -351 and g <  350   then  return   0;    -- almost plain
    elsif   g >   349 and g < 1350   then  return   1;    -- steep upwards
    elsif   g >  1349                then  return   2;    -- wild upwards
    else    return  99;    -- no boundary, perhaps grid map, or something happened
    end if;
  END;
$$ LANGUAGE plpgsql;

