CREATE OR REPLACE FUNCTION inherit_tile_attributes(
  created_id varchar,
  tile_id varchar,
  trigger_id varchar
) RETURNS VOID AS $$
  DECLARE
    inherited_tile_attributes record;
    cond record;
  BEGIN
    select * from tiles where id = tile_id into inherited_tile_attributes;
    insert into tiles (
      id,
      altitude,
      atm_humidity,
      effective_temperature,
      humidity_absorption,
      initial_tile_index,
      is_waterfall,
      latitude,
      longitude,
      pressure,
      salinity,
      soil_humidity,
      soil_quality,
      temperature
    ) values (
      created_id,
      inherited_tile_attributes.altitude,
      inherited_tile_attributes.atm_humidity,
      inherited_tile_attributes.effective_temperature,
      inherited_tile_attributes.humidity_absorption,
      inherited_tile_attributes.initial_tile_index,
      false,
      inherited_tile_attributes.latitude,
      inherited_tile_attributes.longitude,
      inherited_tile_attributes.pressure,
      inherited_tile_attributes.salinity,
      inherited_tile_attributes.soil_humidity,
      inherited_tile_attributes.soil_quality,
      inherited_tile_attributes.temperature
    );

    for cond in
      select c.* from conditions c where c.target = trigger_id
    loop
      if cond.arg = 'altitude'
      then
        if cond.write = 'replace'
        then
          update tiles set altitude = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set altitude = altitude + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'atm_humidity'
      then
        if cond.write = 'replace'
        then
          update tiles set atm_humidity = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set atm_humidity = atm_humidity + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'effective_temperature'
      then
        if cond.write = 'replace'
        then
          update tiles set effective_temperature = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set effective_temperature = effective_temperature + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'humidity_absorption'
      then
        if cond.write = 'replace'
        then
          update tiles set humidity_absorption = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set humidity_absorption = humidity_absorption + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'initial_tile_index'
      then
        if cond.write = 'replace'
        then
          update tiles set initial_tile_index = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set initial_tile_index = initial_tile_index + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'pressure'
      then
        if cond.write = 'replace'
        then
          update tiles set pressure = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set pressure = pressure + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'salinity'
      then
        if cond.write = 'replace'
        then
          update tiles set salinity = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set salinity = salinity + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'soil_humidity'
      then
        if cond.write = 'replace'
        then
          update tiles set soil_humidity = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set soil_humidity = soil_humidity + cond.value where id = created_id;
        end if;
      elsif cond.arg = 'quality'
      then
        if cond.write = 'replace'
        then
          update tiles set quality = cond.value where id = created_id;
        elsif cond.write = 'delta'
        then
          update tiles set quality = quality + cond.value where id = created_id;
        end if;
      end if;
    end loop;
  END;
$$ LANGUAGE plpgsql;

