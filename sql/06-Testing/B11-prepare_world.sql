CREATE OR REPLACE FUNCTION prepare_world() RETURNS VOID AS $$
  DECLARE
    t varchar;
    w varchar;
    e varchar;
    e_name varchar;
    r record;
    c record;
    o varchar;
    v numeric;
  BEGIN
    /* Set world object as game world to differentiate it from the template */
    select id from worlds
    where latitude_delta is not null
    into w;

    insert into links
      (id, type, name, target)
    values
      (w, 'attribute', 'world_class', 'world');

    /* Check if database is sane */
    if exists ( select id from objects where classes = array[]::varchar[] )
    then
      perform log_event ( 'warning',
                          '[INTERNAL]prepare_world()',
                          'Found tiles with no assigned classes, please check biome inits');
      for t in select id from objects where classes = array[]::varchar[]
      loop
        update objects
        set classes = array[]::varchar[] || 'void'::varchar
        where id = t;
        select make() into e;
        update everything set type = 'object', in_table = 'objects' where id = e;
        insert into objects (id, classes, active)
        values (e, array[]::varchar[] || 'wild_terrain'::varchar, false);
        insert into links (id, target, type, name)
        values (t, w, 'parent', 'world');
        insert into links (id, target, type, name)
        values (t, e, 'ecoregion', 'border');
      end loop;
    end if;

    /* Define ecoregions */
    for e in (select distinct target from links where type = 'ecoregion')
    loop
      select unnest(o.classes) from objects o
      where o.id = e
      into e_name;
      for r in (select l.id, l.name from links l where l.type = 'ecoregion' and l.target = e)
      loop
        insert into ecoregions
          (tile_id, ecoregion_id, ecoregion_name, tile_location, is_proto_ecoregion)
        values
          (r.id, e, e_name, r.name, ( select exists ( select p.id from proto_ecoregions p where p.id = e_name ) ) );
      end loop;
    end loop;

    /* Initialize objects */
    for o in (select id from objects)
    loop
      perform update_inherited(o);
      perform initialize_attribute(o, 'initial_tile_index');
    end loop;

    /* Perform all initial updates for weird tiles and such */
    for r in (
      select
        t.id as id,
        cond.id as condition_id
      from
        tiles t,
        inherited i,
        conditions cond,
        triggers trigg
      where
            i.id = t.id
        and i.inherits = trigg.target
        and trigg.task = 'hook_prepare_world'
        and cond.target = trigg.id
    )
    loop
      select * from conditions where id = r.condition_id into c;
      if 'adjust_tile_attribute' = any(c.tags)
      then
        execute format(
          'update tiles
           set %s = $1
           where id = %L;',
          c.write,
          r.id
        )
        using c.value;
      elsif 'copy_attribute' = any(c.tags)
      then
        execute format(
          'select %s from tiles
           where id = %L;',
          c.arg,
          r.id
        )
        into v;
        execute format(
          'update tiles
           set %s = $1
           where id = %L;',
          c.write,
          r.id
        )
        using v;
      end if;
    end loop;
  END;
$$ LANGUAGE plpgsql;

