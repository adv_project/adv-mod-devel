CREATE OR REPLACE FUNCTION make_coast(
  r record,
  coast_type varchar
) RETURNS VOID AS $$
  DECLARE
    swap_tiles boolean;
    trigger_task varchar;
    orig_bio varchar;
    neigh_bio varchar;
    orig_eco varchar;
    neigh_eco varchar;
    trigger_id varchar;
    selected_coastline varchar;
    created_id varchar;
    world_id varchar;
    coastline_altitude numeric;
    host_altitude numeric;
    dest_altitude numeric;
    bio_matches_host boolean;
    bio_matches_dest boolean;
    opposite_direction varchar;
    coastline_ecoregion varchar;
    pathway_to_host varchar;
    fake record;
  BEGIN
    /* Resolve which tile is to be considered as origin */
    if r.origin_is_flooded is true and r.neighbour_is_flooded is not true
    then
      select true into swap_tiles;
    elsif r.origin_is_flooded is not true and r.neighbour_is_flooded is true
    then
      select false into swap_tiles;
    elsif r.origin_proto_ecoregion = 'oceanic_watermass'
      and r.neighbour_proto_ecoregion = 'inland_landlocked_watermass'
    then
      select true into swap_tiles;
    elsif r.origin_proto_ecoregion = 'inland_landlocked_watermass'
      and r.neighbour_proto_ecoregion = 'oceanic_watermass'
    then
      select false into swap_tiles;
    elsif r.altitude_gradient < 0
    then
      select false into swap_tiles;
    else
      select true into swap_tiles;
    end if;

    /* Check for object classes that match the given definitions */
    select 'define_'::varchar || coast_type || '_coastline'::varchar into trigger_task;

    if swap_tiles is true
    then
      select r.neighbour_proto_ecoregion into orig_eco;
      select r.origin_proto_ecoregion into neigh_eco;
      select unnest(r.neighbour_classes) into orig_bio;
      select unnest(r.origin_classes) into neigh_bio;
    else
      select r.origin_proto_ecoregion into orig_eco;
      select r.neighbour_proto_ecoregion into neigh_eco;
      select unnest(r.origin_classes) into orig_bio;
      select unnest(r.neighbour_classes) into neigh_bio;
    end if;

    select t.id from triggers t
    where t.task = trigger_task
    and orig_bio || '_meets_'::varchar || neigh_bio = any(t.tags)
    order by random()
    limit 1
    into trigger_id;

    if trigger_id is null
    then
      select t.id from triggers t
      where t.task = trigger_task
      and orig_bio || '_meets_'::varchar || neigh_eco = any(t.tags)
      order by random()
      limit 1
      into trigger_id;
    end if;

    if trigger_id is null
    then
      select t.id from triggers t
      where t.task = trigger_task
      and orig_eco || '_meets_'::varchar || neigh_bio = any(t.tags)
      order by random()
      limit 1
      into trigger_id;
    end if;

    if trigger_id is null
    then
      select t.id from triggers t
      where t.task = trigger_task
      and orig_eco || '_meets_'::varchar || neigh_eco = any(t.tags)
      order by random()
      limit 1
      into trigger_id;
    end if;

    if trigger_id is null
    then
      return;
    end if;
    
    /* Create the coast tile and make the pathways */
    select target from triggers where id = trigger_id into selected_coastline;
    select create_object(array[]::varchar[] || selected_coastline, false) into created_id;

    if created_id is null
    then
      return;
    end if;

    /* Create and locate tile object */
    select id from string_attributes where attribute_name = 'world_class' and value = 'world' into world_id;
    perform make_location(created_id, world_id, 'world');
    
    /* If we swap tiles (and it's a big if...) */
    select opposite_direction(r.direction) into opposite_direction;
    select altitude from tiles where id = created_id into coastline_altitude;

    select write from conditions
    where target = trigger_id
    and arg = 'proto_ecoregion'
    into coastline_ecoregion;

    if coastline_ecoregion is null
    then select 'steep_terrain' into coastline_ecoregion;
    end if;

    select write from conditions
    where target = trigger_id
    and arg = 'pathway_to_host'
    into pathway_to_host;

    if pathway_to_host is null
    then select 'steep' into pathway_to_host;
    end if;

    if swap_tiles is true
    then
      /* Make the coastline inherit origin's attributes except for nwaterfalls */
      perform inherit_tile_attributes(created_id, r.neighbour, trigger_id);

      /* Collect relevant data */
      select altitude from tiles where id = r.neighbour into host_altitude;
      select altitude from tiles where id = r.origin into dest_altitude;

      select r.neighbour_classes = array[]::varchar[] || selected_coastline
      into bio_matches_host;

      select r.origin_classes = array[]::varchar[] || selected_coastline
      into bio_matches_dest;

      select write from conditions
      where target = trigger_id
      and arg = 'proto_ecoregion'
      into coastline_ecoregion;

      if coastline_ecoregion is null
      then select 'steep_terrain' into coastline_ecoregion;
      end if;

      /* Prepare the fake neighborhood */
      -- coastline to fake origin
      delete from fake_neighborhood;
      insert into fake_neighborhood (
        origin,
        origin_classes,
        neighbour,
        neighbour_classes,
        direction,
        biomes_match,
        origin_is_flooded,
        neighbour_is_flooded,
        altitude_gradient,
        origin_proto_ecoregion,
        neighbour_proto_ecoregion
      ) values (
        created_id,
        array[]::varchar[] || selected_coastline,
        r.neighbour,
        r.neighbour_classes,
        r.direction,
        bio_matches_host,
        false,
        r.neighbour_is_flooded,
        host_altitude - coastline_altitude,
        coastline_ecoregion,
        r.neighbour_proto_ecoregion
      );
      for fake in select * from fake_neighborhood
      loop perform make_pathways(fake, pathway_to_host);
      end loop;
      -- coastline to fake destination
      delete from fake_neighborhood;
      insert into fake_neighborhood (
        origin,
        origin_classes,
        neighbour,
        neighbour_classes,
        direction,
        biomes_match,
        origin_is_flooded,
        neighbour_is_flooded,
        altitude_gradient,
        origin_proto_ecoregion,
        neighbour_proto_ecoregion
      ) values (
        created_id,
        array[]::varchar[] || selected_coastline,
        r.origin,
        r.origin_classes,
        opposite_direction,
        bio_matches_dest,
        false,
        r.origin_is_flooded,
        dest_altitude - coastline_altitude,
        coastline_ecoregion,
        r.origin_proto_ecoregion
      );
      for fake in select * from fake_neighborhood
      loop perform make_pathways(fake, coast_type);
      end loop;

      /* Register this coastline */
      insert into coastline_buffer ( tile_id, coast_id, direction )
      values ( r.neighbour, created_id, opposite_direction);
    else
      /* Make the coastline inherit origin's attributes except for nwaterfalls */
      perform inherit_tile_attributes(created_id, r.origin, trigger_id);

      /* Collect relevant data */
      select altitude from tiles where id = r.origin into host_altitude;
      select altitude from tiles where id = r.neighbour into dest_altitude;

      select r.origin_classes = array[]::varchar[] || selected_coastline
      into bio_matches_host;

      select r.neighbour_classes = array[]::varchar[] || selected_coastline
      into bio_matches_dest;

      select write from conditions
      where target = trigger_id
      and arg = 'proto_ecoregion'
      into coastline_ecoregion;

      if coastline_ecoregion is null
      then select 'steep_terrain' into coastline_ecoregion;
      end if;

      /* Prepare the fake neighborhood */
      -- coastline to fake origin
      delete from fake_neighborhood;
      insert into fake_neighborhood (
        origin,
        origin_classes,
        neighbour,
        neighbour_classes,
        direction,
        biomes_match,
        origin_is_flooded,
        neighbour_is_flooded,
        altitude_gradient,
        origin_proto_ecoregion,
        neighbour_proto_ecoregion
      ) values (
        created_id,
        array[]::varchar[] || selected_coastline,
        r.neighbour,
        r.neighbour_classes,
        r.direction,
        bio_matches_host,
        false,
        r.neighbour_is_flooded,
        host_altitude - coastline_altitude,
        coastline_ecoregion,
        r.neighbour_proto_ecoregion
      );
      for fake in select * from fake_neighborhood
      loop perform make_pathways(fake, pathway_to_host);
      end loop;
      -- coastline to fake destination
      delete from fake_neighborhood;
      insert into fake_neighborhood (
        origin,
        origin_classes,
        neighbour,
        neighbour_classes,
        direction,
        biomes_match,
        origin_is_flooded,
        neighbour_is_flooded,
        altitude_gradient,
        origin_proto_ecoregion,
        neighbour_proto_ecoregion
      ) values (
        created_id,
        array[]::varchar[] || selected_coastline,
        r.origin,
        r.origin_classes,
        opposite_direction,
        bio_matches_dest,
        false,
        r.origin_is_flooded,
        dest_altitude - coastline_altitude,
        coastline_ecoregion,
        r.origin_proto_ecoregion
      );
      for fake in select * from fake_neighborhood
      loop perform make_pathways(fake, pathway_to_host);
      end loop;

      /* Register this coastline */
      insert into coastline_buffer ( tile_id, coast_id, direction )
      values ( r.origin, created_id, r.direction);
    end if;
  END;
$$ LANGUAGE plpgsql;

