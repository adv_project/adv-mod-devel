CREATE OR REPLACE FUNCTION resolve_wording(
  token TEXT,
  player TEXT
) RETURNS TEXT AS $$
  BEGIN
    RETURN token;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_player_language(
  player TEXT
) RETURNS TEXT AS $$
  DECLARE
    player_id   varchar;
    language    varchar;
  BEGIN
    select id from links where type = 'attribute' and name = 'username' and target = player into player_id;
    select target from links where type = 'attribute' and name = 'language' and id = player_id into language;
    RETURN language;
  END;
$$ LANGUAGE plpgsql;

