CREATE OR REPLACE FUNCTION parse_action(
  action        TEXT[],
  player        TEXT,
  text_entry    TEXT[]
) RETURNS JSONB AS $$
  DECLARE
    result  JSONB;
  BEGIN
    result := jsonb_build_object(
       'command'::text, action::text[],
       'player'::text,  player::text,
       'text'::text,    array[]::text[] || text_entry::text[]
    );
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION execute_action(
  action JSONB
) RETURNS void AS $$
  DECLARE
    trigger_id  VARCHAR;
    a varchar;
    cnt integer;
    job varchar;
    args varchar[];
    trigger_found boolean;
  BEGIN
    cnt := 0;
    for a in select * from jsonb_array_elements_text(action->'command')
    loop
      if cnt = 0 then
        action := action || jsonb_build_object(
          'job'::text, a::text
        );
        job := a;
      else
        action := action || jsonb_build_object(
          concat('arg', cnt)::text, a::text
        );
      end if;
      cnt := cnt + 1;
    end loop;

    /* Look for some command trigger that matches the job */
    /* TODO: unfinished query, must filter inheritance */
    trigger_found := false;
    for trigger_id in
      select t.id from triggers t where
        t.task = job and
        t.type = 'user_command'
    loop
      perform push_trigger(trigger_id, action);
      trigger_found := true;
    end loop;
    if trigger_found is not true
    then
      action := action || jsonb_build_object(
        'response_class'::text, 'trigger_not_found'::text,
        'response_type'::text, 'message'::text
      );
      perform push_response(action);
    end if;

    return;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION execute_admin_action(
  action JSONB
) RETURNS void AS $$
  DECLARE
    trigger_id  VARCHAR;
    a varchar;
    cnt integer;
    job varchar;
    args varchar[];
    trigger_found boolean;
  BEGIN
    cnt := 0;
    for a in select * from jsonb_array_elements_text(action->'action')
    loop
      if cnt = 0 then
        job := a;
        action := action || jsonb_build_object(
          'job'::text, a::text
        );
      else
        action := action || jsonb_build_object(
          concat('arg', cnt)::text, a::text
        );
      end if;
      cnt := cnt + 1;
    end loop;

    /* Look for some admin trigger that matches the job */
    trigger_found := false;
    for trigger_id in
      select t.id from triggers t, everything e where
        e.type = 'admin' and
        t.target = e.id and
        t.task = job and
        t.type = 'admin_command'
    loop
      perform push_trigger(trigger_id, action);
      trigger_found := true;
    end loop;
    if trigger_found is not true
    then
      action := action || jsonb_build_object(
        'response_class'::text, 'trigger_not_found'::text,
        'response_type'::text, 'message'::text
      );
      perform push_response(action);
    end if;

    return;
  END;
$$ LANGUAGE plpgsql;


