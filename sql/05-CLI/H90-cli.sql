CREATE OR REPLACE FUNCTION get_active_world_id() RETURNS VARCHAR AS $$
  DECLARE
    world_id VARCHAR;
  BEGIN
    select id from links where type = 'attribute' and name = 'world_class' and target = 'world' limit 1
    into world_id;

    if world_id is not null
    then return world_id;
    else return ''::varchar;
    end if;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION advance_time(world_id varchar) RETURNS jsonb AS $$
  DECLARE
    tpd                   NUMERIC;
    dpy                   NUMERIC;
    current_tick_of_day   NUMERIC;
    current_day_of_year   NUMERIC;
    current_year          NUMERIC;
    day_change            BOOLEAN;
    year_change           BOOLEAN;
    hours_per_tick    NUMERIC;
    result                jsonb;
  BEGIN
    select ticks_per_day from worlds where id = world_id into tpd;
    select 24 / tpd into hours_per_tick;
    select days_per_year from worlds where id = world_id into dpy;
    select estimate_attribute(world_id, 'tick_of_day') into current_tick_of_day;
    select estimate_attribute(world_id, 'day_of_year') into current_day_of_year;
    select estimate_attribute(world_id, 'year') into current_year;
    select false into day_change;
    select false into year_change;

    select current_tick_of_day + 1 into current_tick_of_day;
    if current_tick_of_day >= tpd
    then
      select 0 into current_tick_of_day;
      select true into day_change;
      select current_day_of_year + 1 into current_day_of_year;
      if current_day_of_year >= dpy
      then
        select 0 into current_day_of_year;
        select true into year_change;
        select current_year + 1 into current_year;
      end if;
    end if;
    
    perform modify_attribute(world_id, 'tick_of_day',  current_tick_of_day, 'set');

    if day_change is true
    then
      perform modify_attribute(world_id, 'day_of_year',  current_day_of_year, 'set');
      if year_change is true
      then
        perform modify_attribute(world_id, 'year',  current_year, 'set');
      end if;
    end if;

    select jsonb_build_object(
      'world_id', world_id,
      'current_tick_of_day', current_tick_of_day::numeric,
      'current_day_of_year', current_day_of_year::numeric,
      'current_year', current_year::numeric,
      'hours_per_tick', hours_per_tick::numeric
    ) into result;
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION trigger_update(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    world_id varchar;
    bottom_parent_id varchar;
    object_id varchar;
    trigg varchar;
    world_buffer jsonb;
    this_buf jsonb;
    responses JSONB;
    i integer;
    j integer;
  BEGIN
    if not exists ( select id from players where connected is true )
    then
      perform broadcast_log_event('info', 'trigger_update',
               concat(' * skipping update (no connected players) *'));
      return '[]'::jsonb;
    end if;

    --perform reset_log_commits();
    perform broadcast_log_event('info', 'trigger_update',
               concat(' * TRIGGERING UPDATE *'));
    select * from get_active_world_id() into world_id;
    select advance_time(world_id) into world_buffer;
    
    perform push_response(world_buffer::jsonb);
    i := 0;
    j := 0;
    for bottom_parent_id in
      select l.id from links l, objects o
      where o.active is true
      and o.id = l.id
      and l.type = 'parent'
      and l.target = world_id
    loop
      i := i + 1;
      select world_buffer into this_buf;
      foreach object_id in array ( select * from all_in_self_recursive(bottom_parent_id) )
      loop
        j := i + 1;
        perform trigger_hook(object_id, 'hook_update', this_buf);
      end loop;
    end loop;

    perform log_event('info', 'trigger_update',
      concat('[', world_id, ']: updating ', i, ' active tiles (total: ', j, ' objects)'));
    perform flush_triggers_stack();
    responses := flush_responses_stack();
    perform purge_all_logs();
    perform broadcast_log_event('info', 'trigger_update', concat(' * UPDATE FINISHED *'));
    return responses::jsonb;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION process_user_entry(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    token             TEXT;
    action_resolved   TEXT[];
    action_parsed     JSONB;
    responses         JSONB;
    text_entry        TEXT[];
  BEGIN
    perform reset_log_commits();
    perform broadcast_log_event('info',
                      'process_user_entry',
                      '* processing user entry *');
    text_entry := array[]::TEXT[] || jsonb_array_elements_text(buffer->'text');
    -- Aliases, ignored words, integers, ...
    FOR token IN
      SELECT *
      FROM jsonb_array_elements_text(buffer->'action')
    LOOP
      -- Populate this array with the resolved words
      action_resolved := action_resolved || resolve_wording(token, quote_ident(buffer->>'player'));
    END LOOP;

    -- A jsonb that organizes the command for execution.
    action_parsed := parse_action(action_resolved::TEXT[], quote_ident(buffer->>'player')::TEXT, text_entry::TEXT[]);
    -- Actually picks the trigger and runs it, and flushes the stack until there is nothing left
    PERFORM execute_action(action_parsed);
    PERFORM flush_triggers_stack();
    responses := flush_responses_stack();
    perform broadcast_log_event(  'info',
                        'process_user_entry',
                        '* USER ENTRY PROCESSED *'
    );
    RETURN responses::jsonb;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION process_admin_entry(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    responses         JSONB;
  BEGIN
    perform reset_log_commits();
    perform broadcast_log_event('info',
                      'process_admin_entry',
                      '* processing admin entry *');
    PERFORM execute_admin_action(buffer);
    PERFORM flush_triggers_stack();
    responses := flush_responses_stack();
    perform broadcast_log_event(  'info',
                        'process_admin_entry',
                        '* ADMIN ENTRY PROCESSED *');
    RETURN responses::jsonb;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION process_mod_message(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    responses         JSONB;
  BEGIN
    perform reset_log_commits();
    perform broadcast_log_event(  'info',
                        'process_mod_message',
                        '* processing mod message *');
    PERFORM push_response(buffer);
    responses := flush_responses_stack();
    perform broadcast_log_event(  'info',
                        'process_mod_message',
                        '* MOD MESSAGE PROCESSED *');
    RETURN responses::jsonb;
  END;
$$ LANGUAGE plpgsql;

