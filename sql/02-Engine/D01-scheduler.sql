CREATE OR REPLACE FUNCTION flush_triggers_stack()
RETURNS VOID AS $$
  DECLARE
    call record;
  BEGIN
    perform log_trigger_stack_event('info',
                      'flush_triggers_stack'::text,
                      '* flushing triggers stack *');
    while exists (select ord from triggers_stack limit 1)
    loop
      for call in select t.ord, t.trigger_id, t.buffer from triggers_stack t order by ord
      loop
        perform log_trigger_stack_event('info',
                          'flush_triggers_stack'::text,
                 concat(  '* pulling trigger :', call.trigger_id, ' *'));
        perform pull_trigger(call);
        delete from triggers_stack s where s.ord = call.ord;
      end loop;
    end loop;
    perform log_trigger_stack_event('info',
                      'flush_triggers_stack',
                      '* TRIGGERS STACK FLUSHED *'::text);
    RETURN;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION flush_epiphanies_stack() RETURNS VOID AS $$
  DECLARE
    r jsonb;
  BEGIN
    FOR r IN SELECT response FROM epiphanies_stack
    LOOP
      INSERT INTO responses_stack VALUES (r);
    END LOOP;

    DELETE FROM epiphanies_stack;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION flush_responses_stack() RETURNS JSONB AS $$
  DECLARE
    responses JSONB[];
    result    JSONB;
  BEGIN
    -- Pick all mod API calls left by the triggers, save them in this array, and flush the stack
    responses := array(
      SELECT response
      FROM responses_stack
      order by ord asc
    );
    DELETE FROM responses_stack;
    result := array_to_json(responses);
    RETURN result::jsonb;
  END;
$$ LANGUAGE plpgsql;

