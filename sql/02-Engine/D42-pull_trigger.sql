/*
The core function, everything else is cellophan.
Process the conditions in sequence.
 */
CREATE OR REPLACE FUNCTION pull_trigger(
  call  RECORD
)
RETURNS JSONB AS $$
  DECLARE
    trigger_definition  RECORD;
    buffer              JSONB;
    this_condition      RECORD;

    player_id           VARCHAR;  -- The player id is appended to the buffer
    retval              BOOLEAN;  -- The return value for the last condition
    inside_if           BOOLEAN;  -- True if inside an 'if' statement
    inside_goto         BOOLEAN;  -- True if searching for a 'goto' label
    eval_state          BOOLEAN;  -- Result of the evaluation of an 'if' clause
    eval_done           BOOLEAN;  -- Is this condition already evaluated?
    exec_done           BOOLEAN;  -- True if a successful 'if' clause has been executed
    eval_pending        BOOLEAN;  -- True if an 'if' clause needs to be evaluated
    goto_label          VARCHAR;  -- The label we are searching for
    i                   numeric;  -- Dummy number
    s                   varchar;  -- Dummy string
    ss                  varchar;  -- Dummy string
    have_test           BOOLEAN;  -- does this condition have test?
    have_query          BOOLEAN;  -- does this condition have query?
    have_test_routine   BOOLEAN;  -- is this condition test a routine?
    have_query_routine  BOOLEAN;  -- is this condition query a routine?
    do_abort            BOOLEAN;  -- should we abort at this stage?
    this_query          JSONB;    -- Our temporary buffers for each condition
    this_test           JSONB;
    this_tmp_buffer     JSONB;
    this_result         JSONB;
    write_field varchar;
    log_class varchar;
    log_trace varchar;
    response_class varchar;
    response_type  varchar;
    
  BEGIN
    select t.id, t.target, t.label, t.task, t.tags
    from triggers t where t.id = call.trigger_id
    into trigger_definition;

    buffer := call.buffer;
    log_class := 'pull_trigger';
    log_trace := concat(':: ', trigger_definition.label);

    perform log_trigger_event('info', log_class, log_trace, '* pulling trigger *', buffer);

    /* Reset all flags */
    retval          := FALSE;
    inside_if       := FALSE;
    inside_goto     := FALSE;
    eval_state      := FALSE;
    exec_done       := FALSE;
    eval_pending    := FALSE;
    do_abort        := false;

    /* Append player's id to the buffer */
    if buffer ? 'player' and not buffer ? 'player_id' then
      select id from links
      where type = 'attribute' and name = 'username' and target = buffer->>'player'
      into player_id;
      buffer := buffer || jsonb_build_object(
        'player_id'::text,  player_id::text
      );
    end if;

    /* EXECUTION CYCLE */
    for this_condition in
      select * from conditions c
      where c.target = call.trigger_id
      order by c.ord asc
    loop

      /***** CONDITION EXECUTION *****/

      log_trace := concat(' => ', this_condition.label);
      perform log_trigger_event('info', log_class, log_trace, ' * pulling condition *');

      eval_pending  := FALSE;
      eval_done     := FALSE;
      
      /* Checks */
      if inside_goto is true and goto_label != this_condition.label then
        perform log_trigger_event('info', log_class, log_trace, ' * goto label does not match, skipping *');
        continue;
      elsif inside_goto is true and goto_label = this_condition.label then
        perform log_trigger_event('info', log_class, log_trace, ' * goto label matches, back to execution *');
        inside_goto = FALSE;
      end if;

      if this_condition.type = 'goto' and inside_if is not true
      or this_condition.type = 'goto' and eval_state is true
      then
        inside_goto := TRUE;
        goto_label := this_condition.arg;
        perform log_trigger_event('info', log_class, log_trace, concat(' GOTO LABEL IS NOW:'));
        perform log_trigger_event('info', log_class, log_trace, concat('   "', goto_label, '"'));
        inside_if := false;       eval_state := false;
        eval_pending := false;    exec_done := false;
        continue;
      elsif this_condition.type = 'if' then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating if statement *'));
        eval_pending := TRUE;     inside_if := TRUE;
      elsif this_condition.type = 'elsif' and eval_state is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping if statement *'));
        inside_if := TRUE;        eval_pending := false;
        exec_done := TRUE;
        continue;
      elsif this_condition.type = 'elsif' and eval_state is not TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating elsif statement *'));
        eval_pending := TRUE;     inside_if := TRUE;
      elsif this_condition.type = 'else' and eval_state is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping elsif statement *'));
        inside_if := TRUE;        eval_pending := false;
        exec_done := TRUE;
        continue;
      elsif this_condition.type = 'else' and eval_state is not TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating else statement *'));
        inside_if := TRUE;        eval_state := TRUE;
      elsif this_condition.type = 'endif' then
        perform log_trigger_event('info', log_class, log_trace, concat(' * closing if statement *'));
        if 'force_success' = any(this_condition.tags) then
          retval := true;
        else
          retval := eval_state;
        end if;

        inside_if := false;       eval_state := false;
        eval_pending := false;    exec_done := false;
        continue;
      elsif inside_if is TRUE and eval_state is TRUE and exec_done is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping *'));
        continue;
      end if;

      if ( inside_if is true and eval_state is true and exec_done is not true )
      or ( inside_if is true and eval_pending is true )
      or ( inside_if is not true )
      then
        /* Run condition */
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating condition *'));
        have_test := is_valid(this_condition.test_name);
        have_query := is_valid(this_condition.query_name);

        if have_test is true then
          have_test_routine := is_routine_name(this_condition.test_name);
        else
          have_test_routine := false;
        end if;

        if have_query is true then
          have_query_routine := is_routine_name(this_condition.query_name);
        else
          have_query_routine := false;
        end if;

        /*** QUERY EXECUTION ***/

        log_trace := concat('  -> query [', this_condition.query_name, ']');

        if have_query is true then
          perform log_trigger_event('info', log_class, log_trace,
            concat('  * evaluating query *'));
          if have_query_routine is true then
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * executing query *'));
            select try_query from try_query(buffer, this_condition.id)
            INTO this_query;
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * finished executing query *'), this_query);
          elsif have_query is true and this_condition.query_name like '@@%' then
            s := regexp_replace(this_condition.query_name, '@@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling as array: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_query := jsonb_build_object(
                'this_query'::text,  array[buffer->>s]::varchar[],
                'retval'::text, true::boolean
              );
            else
              this_query := '{"this_query":[],"retval":false}'::jsonb;
            end if;
          elsif have_query is true and this_condition.query_name like '@%' then
            s := regexp_replace(this_condition.query_name, '@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_query := jsonb_build_object(
                'this_query'::text,  buffer->s,
                'retval'::text, true::boolean
              );
            else
              this_query := '{"this_query":null,"retval":false}'::jsonb;
            end if;
          else
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pasting: ', this_condition.query_name), buffer);
            this_query := jsonb_build_object(
              'this_query'::text, this_condition.query_name::text,
              'retval'::text,     true::boolean
            );
          end if;
        else
          perform log_trigger_event('info', log_class, log_trace, concat('  (empty)'));
          this_query := '{"this_query":[],"retval":true}'::jsonb;
        end if;

        /*** QUERY DONE ***/

        perform log_trigger_event('info', log_class, log_trace, concat('  * QUERY DONE *'), this_query);

        /*** TEST EXECUTION ***/

        write_field := resolve_condition_write(this_condition.write, 'this_test');
        log_trace := concat('  -> test [', this_condition.test_name, ']');

        if have_test is true then
          perform log_trigger_event('info', log_class, log_trace, concat('  * evaluating test *'));
          if have_test_routine is true then
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * executing test * '));
            select try_test from try_test(buffer, this_query, this_condition.id)
            INTO this_test;
            this_test := this_query || this_test;
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * finished executing test *'), this_test);
          elsif have_test is true and this_condition.test_name like '@@%' then
            s := regexp_replace(this_condition.test_name, '@@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer as array: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[buffer->>s]::varchar[],
                'retval'::text,     true::boolean
              );
            else
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[]::varchar[],
                'retval'::text,     false::boolean
              );
            end if;
          elsif have_test is true and this_condition.test_name like '@%' then
            s := regexp_replace(this_condition.test_name, '@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_test := this_query || jsonb_build_object(
                write_field::text,  buffer->s,
                'retval'::text,     true::boolean
              );
            else
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[]::varchar,
                'retval'::text,     false::boolean
              );
            end if;
          else
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pasting: ', s));
            this_test := this_query || jsonb_build_object(
              write_field::text,  this_condition.test_name::text,
              'retval'::text,     true::boolean
            );
          end if;
        else
          perform log_trigger_event('info', log_class, log_trace,
            concat('  (empty)'));
          this_test := this_query || jsonb_build_object(
            write_field::text,  this_condition.test_name::text,
            'retval'::text,     true::boolean
          );
        end if;

        /*** TEST DONE ***/

        perform log_trigger_event('info', log_class, log_trace, concat('  * TEST DONE *'), this_test);
        log_trace := concat(' => ', this_condition.label);

        /*  AGGREGATE RESULTS TO BUFFER
         *  write safely* the results in the buffer
         *  TODO: unsafe, simplistic, brute -- should preserve arrays (concatenate)
         */

        /** RESOLVE RETURN VALUE FOR THIS TEST **/

        retval := this_test->>'retval' like '%true%';
        
        if  this_condition.type = 'abort'
            or 'do_abort' = any(this_condition.tags)
            or 'do_abort_message' = any(this_condition.tags)
        then
          retval := false;
          do_abort := true;
        elsif this_condition.type = 'exit'
              or 'do_exit' = any(this_condition.tags)
              or 'do_exit_message' = any(this_condition.tags)
        then
          do_abort := true;
        end if;

        if retval is not true and (
              'do_abort_on_failure' = any(this_condition.tags)
           or 'do_abort_message_on_failure' = any(this_condition.tags)
        )
        then
          do_abort := true;
        end if;

        if 'negate' = any(this_condition.tags) then
          retval := this_test->>'retval' not like '%true%'; 
        else
          retval := this_test->>'retval' like '%true%'; 
        end if;

        /** TEST RETURN VALUE RESOLVED **/

        /** AGGREGATE TEST RESULT TO BUFFER **/

        if 'discard' = any(this_condition.tags)
        or this_condition.write = 'discard'
        then
          perform log_trigger_event('info', log_class, log_trace, ' * discarding test result *');
        else
          perform log_trigger_event('info', log_class, log_trace, ' * aggregating test result *', buffer);
          buffer := buffer || this_test;
        end if;
        
        /* FINAL CONDITION COMMANDS */

        perform log_trigger_event('info', log_class, log_trace, concat(' * POST CONDITION EVALUATION *'), this_test || this_query);

        if (     'do_abort_message_on_failure' = any(this_condition.tags)
              or 'do_message_on_failure' = any(this_condition.tags)
        ) and retval is not true
        then
          perform log_trigger_event('info', log_class, log_trace, concat(' Triggering response on failure:'));
          perform jtrigger_response(buffer::jsonb, '{"this_query":[]}'::jsonb, this_condition.id);
        end if;

      else
        log_trace := concat(' => ', this_condition.label);
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping *'));
      end if;

      /** RESOLVING RETURN VALUE FOR THIS CONDITION **/
      
      log_trace := concat(' => ', this_condition.label);

      if 'force_success' = any(this_condition.tags) then
        retval := true;
        perform log_trigger_event('info', log_class, log_trace, concat(' * forcing success *'));
      end if;

      /** RESOLVE CONDITION EVALUATION STATE IN IF CONTEXT **/

      if inside_if is true and eval_pending is true then
        eval_state := retval;
      end if;
      
      perform log_trigger_event('info', log_class, log_trace,
        concat(' Retval: ', retval::text), this_test);
      perform log_trigger_event('info', log_class, log_trace,
        concat(' Eval state: ', eval_state::text), buffer);

      /** ABORT IF NEEDED **/

      if do_abort is true
      then
        if retval is true
        then s := 'Exit';
        else s := 'Abort';
        end if;

        perform log_trigger_event('info', log_class, log_trace,
          concat(' ', s));

        exit;
      elsif 'abort_on_failure' = any(trigger_definition.tags) and retval is not true
      then
        perform log_trigger_event('info', log_class, log_trace,
          concat(' * ABORTING BECAUSE SOME CONDITION FAILED *'), buffer);
        exit;
      end if;

      /***** END OF THIS CONDITION *****/
      
      perform log_trigger_event('info', log_class, log_trace,
        concat(' * END OF THIS CONDITION *'), buffer);

    end loop;

    log_trace := concat(':: ', trigger_definition.label);
    perform log_trigger_event('info', log_class, log_trace,
      concat('  * END OF THIS TRIGGER *'), buffer);
    buffer := buffer || jsonb_build_object('retval', eval_state::boolean);
    RETURN buffer;
  END;
$$ LANGUAGE plpgsql;

