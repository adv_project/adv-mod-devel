CREATE OR REPLACE FUNCTION resolve_response(
  buffer          JSONB
) RETURNS JSONB AS $$
  DECLARE
    tmp_array text[];
    swap_array text[];
    words_array text[];
    classes_array text[];
    sentence text;
    parsed_sentence text;
    custom_sentence text[];
    word text;
    parsed_word text;
    result text[];
    recipient varchar;
    recipient_username varchar;
    lang varchar;
    log_class varchar;
  BEGIN
    log_class := 'resolve_response';
    tmp_array := array[]::text[];
    swap_array := array[]::text[];
    classes_array := array[]::text[];
    result := array[]::text[];

    perform log_responses_event('info', log_class, '=> Resolving response'::text, jsonb_pretty(buffer));
    if buffer ? 'recipient'
    then
      recipient := buffer->>'recipient';
      perform log_responses_event('info', log_class, concat('-> To recipient: ', recipient));
    elsif buffer ? 'player_id'
    then
      recipient := buffer->>'player_id';
      perform log_responses_event('info', log_class, concat('-> To player_id: ', recipient));
    elsif buffer ? 'target'
    then
      recipient := buffer->>'target';
      perform log_responses_event('info', log_class, concat('-> To target: ', recipient));
    elsif buffer ? 'recipient_username'
    then
      select p.id from players p, links l
      where l.id = p.id
      and l.type = 'attribute'
      and l.name = 'username'
      and l.target = buffer->>'recipient_username'
      into recipient;
      perform log_responses_event('info', log_class, concat('-> To recipient username: ', recipient));
    elsif buffer ? 'player'
    then
      select p.id from players p, links l
      where l.id = p.id
      and l.type = 'attribute'
      and l.name = 'username'
      and l.target = buffer->>'player'
      into recipient;
      perform log_responses_event('info', log_class, concat('-> To player: ', recipient));
    end if;
    
    if recipient is null
    then
      select id from players
      where is_owner is true
      into recipient;
      perform log_responses_event('info', log_class, concat('-> Fallback to owner: ', recipient));
    end if;

    if not buffer ? 'recipient'
    then
      buffer := buffer || jsonb_build_object(
        'recipient', recipient
      );
    end if;

    if not buffer ? 'recipient_username' and exists (select id from players where id = recipient)
    then
      select l.target from links l
      where name = 'username'
      and type = 'attribute'
      and id = recipient
      into recipient_username;

      buffer := buffer || jsonb_build_object(
        'recipient_username', recipient_username
      );
    end if;

    perform log_responses_event('info', log_class, concat(' * recipient resolved *'), jsonb_pretty(buffer));

    select target from links
    where id = recipient
    and name = 'language'
    and type = 'attribute'
    into lang;
    
    perform log_responses_event('info', log_class, concat('-> Setting language: ', lang));

    /* Parse response class first */
    if buffer->>'response_type' = 'message'
    then
      classes_array := classes_array || array(
        select string from dict_classes
        where class_id = buffer->>'response_class'
        and language = lang
        order by random()
        limit 1
      )::text[];
      perform log_responses_event('info', log_class, concat('-> Picking message: '));
      perform log_responses_event('info', log_class, concat('<', buffer->>'response_class', '>'), array_to_string(classes_array, ', '));
    elsif buffer->>'response_type' = 'dialog'
    then
      classes_array := classes_array || array(
        select string from dict_classes
        where class_id = buffer->>'response_class'
        and language = lang
        order by ord asc
      )::text[];
      perform log_responses_event('info', log_class, concat('-> Picking dialog: '));
      perform log_responses_event('info', log_class,
        concat('<', buffer->'response_class', '>'), array_to_string(classes_array, ', ') );
    end if;

    if classes_array = array[]::text[] then
      classes_array := classes_array || array[buffer->>'response_class']::text[];
      perform log_responses_event('info', log_class, concat('-> Picking response class: ') );
      perform log_responses_event('info', log_class, concat('<', buffer->>'response_class', '>'), array_to_string(classes_array, ', '));
    end if;

    -- Ok, we have result[] as an array of resolved classes.
    foreach sentence in array classes_array
    loop
      perform log_responses_event('info', log_class, concat(' -> Parsing sentence: ') );
      perform log_responses_event('info', log_class, concat(' "', sentence, '"' ) );
      
      parsed_sentence := null;
      for word in (
        select * from regexp_split_to_table(sentence, ' ')
      )
      loop
        select * from resolve_word(word, buffer) into parsed_word;

        perform log_responses_event('info', log_class, concat('  -> Passing word to parser:'));
        perform log_responses_event('info', log_class, concat('  "', word, '"'));
        perform log_responses_event('info', log_class, concat('    >> "', parsed_word, '"') );

        if parsed_sentence is null
        then parsed_sentence := parsed_word;
        else parsed_sentence := parsed_sentence || ' ' || parsed_word;
        end if;
        perform log_responses_event('info', log_class,
          concat(' [', parsed_sentence, ']') );
      end loop;

      if parsed_sentence like '%DISCARD%'
      then
        perform log_responses_event('info', log_class, concat(' * DISCARDING SENTENCE *'), parsed_sentence);
      else
        select d.custom_text from dict_custom d
        where d.sentence = parsed_sentence
        and d.language = lang
        order by random()
        limit 1
        into custom_sentence;

        if custom_sentence <> array[]::text[]
        then
          result := result || custom_sentence;
          perform log_responses_event('info', log_class,
            ' * REPLACING WITH CUSTOM SENTENCE *', array_to_string(custom_sentence, ' '));
        else
          result := result || array[parsed_sentence]::text[];
        end if;
      end if;
      perform log_responses_event('info', log_class,
        concat('* SENTENCE RESOLVED *'), array_to_string(result, ' '));
    end loop;

    perform log_responses_event('info', log_class,
      concat('* RESPONSE RESOLVED *'), array_to_string(result, ' '));
    RETURN buffer || jsonb_build_object('messages', result::text[]);
  END;
$$ language plpgsql;

