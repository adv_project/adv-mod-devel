CREATE OR REPLACE FUNCTION resolve_word(
  word    text,
  buffer  JSONB
) RETURNS TEXT AS $$
  DECLARE
    this_expression text;
    this_parser varchar;
    this_context varchar;
    these_ops varchar;
    lang varchar;
    op varchar;
    op_label varchar;
    op_value varchar;
    f varchar;
    c varchar;
    i varchar;
    a varchar[];
    w text;
    t varchar[];
    this_string text;
    this_description text;
    comma_string text;
    and_string text;
    this_row record;
    row_count integer;
    current_row integer;
    make_array boolean;
    skip_meme boolean;
    identify_token boolean;
    recall boolean;
    capitalize boolean;
    number_format varchar;
    bar_points integer;
    bar_state numeric;
    result text;
    log_class varchar;
    bar_symbol varchar;
  BEGIN
    log_class := 'resolve_word';
    perform log_word_event(log_class, concat('Processing word: "', word, '"'), jsonb_pretty(buffer));
    
    select target from links where type = 'attribute' and name = 'language' and id = buffer->>'recipient'
    into lang;

    make_array := false;
    skip_meme := false;
    identify_token := false;
    recall := false;

    if word like '@%@'
    then
      this_expression := regexp_replace(word, '@', '', 'g');
      this_parser := 'default';
      this_context := 'default';
      -- Extract operators from the expression
      if this_expression like '<%>%'
      then
        these_ops := substring(this_expression from '<#"%#">%' for '#');
        this_expression := regexp_replace(this_expression, '<.*>', '');
        for op in (
          select * from regexp_split_to_table(these_ops, ',')
        )
        loop
          op_label := substring(op from '#"%#":%' for '#');
          op_value := substring(op from '%:#"%#"' for '#');
          if op_label = 'context'
          then this_context := op_value;
          elsif op_label = 'parser'
          then this_parser := op_value;
          elsif op_label = 'makearray'
          then make_array := true;
          elsif op_label = 'skipmeme'
          then skip_meme := true;
          elsif op_label = 'identify'
          then identify_token := true;
          elsif op_label = 'recall'
          then recall := true;
          elsif op_label = 'capitalize'
          then capitalize := true;
          elsif op_label = 'fmt'
          then number_format := op_value;
          end if;
        end loop;
      end if;

      result := ''::text;

      if this_parser = 'number'
      then
        perform log_extractor_event('parser:number', 'Passing string as number');
        result := (to_number(buffer->>this_expression, number_format))::text;
      elsif this_parser = 'percentage'
      then
        perform log_extractor_event('parser:percentage', 'Passing string as percentage');
        result := concat( (to_number(buffer->>this_expression, number_format))::text, '%'::text);
      elsif this_parser = 'statsbar'
      then
        perform log_extractor_event('parser:statsbar', 'Converting to statsbar (EXPERIMENTAL)');

        bar_state := (to_number(buffer->>this_expression, '9999.'))::numeric;

        bar_points := 0;
        loop
          bar_points := bar_points + 100;
          
          if bar_state < bar_points
          or bar_points > 1100
          then exit;
          end if;
        end loop;
        
        bar_symbol := 'statsbar_'::text || bar_points::text;

        select string from dict_classes
        where language = lang
        and class_id = bar_symbol
        into result;

        if result is null
        then result := '#(bold)#(magenta)NULL';
        end if;

      elsif this_parser = 'statsbar_inv'
      then
        perform log_extractor_event('parser:statsbar_inv', 'Converting to inverted statsbar (EXPERIMENTAL)');

        bar_state := (to_number(buffer->>this_expression, '9999.'))::numeric;

        bar_points := 0;
        loop
          bar_points := bar_points + 100;
          
          if bar_state < bar_points
          or bar_points > 1100
          then exit;
          end if;
        end loop;
        
        bar_symbol := 'statsbar_inv_'::text || bar_points::text;

        select string from dict_classes
        where language = lang
        and class_id = bar_symbol
        into result;

        if result is null
        then result := '#(bold)#(magenta)NULL';
        end if;

      elsif this_parser = 'cat'
      then
        perform log_extractor_event('parser:cat', 'Concatenating from buffer');

        result := buffer->>this_expression;

        if result is null
        then result := concat('[', this_expression, ']');
        end if;

      elsif this_parser = 'extract'
      then
        perform log_extractor_event('parser:extract', 'Concatenating array from buffer');

        result := array_to_string(
          array[]::text[] || array(select * from jsonb_array_elements_text(buffer->this_expression)),
          ' '
        );

        if result is null
        then result := concat('[', this_expression, ']');
        end if;

      elsif this_parser = 'translate'
      then
        perform log_extractor_event('parser:translate', 'Translating from dictionary');

        if make_array is true
        then
          this_string := buffer->>this_expression;
        else
          this_string := array_to_string(
            array[]::text[] || array(select * from jsonb_array_elements_text(buffer->this_expression)),
            ' '
          );
        end if;

        if identify_token is true
        then
          select unnest(o.classes) from objects o
          where o.id = this_string limit 1
          into this_string;
        end if;

        if skip_meme is not true
        then
          perform check_memories(buffer->>'recipient', this_string, 'NULL', true, 'describe');
        end if;

        select string from dict_tokens
        where language = lang
        and token = this_string
        and context = this_context
        limit 1
        into result;

        if result is null
        then result := concat('[', this_string, ']');
        end if;

      elsif this_parser = 'grouped_list'
      then
        perform log_extractor_event('parser:grouped_list', 'Grouping objects');

        create temp table known on commit drop as 
          select * from identify_objects(buffer, this_expression, make_array, skip_meme, recall);

        create temp table item_count (
          token varchar,
          count integer
        ) on commit drop;

        select string from dict_punctuation
        where token = 'comma'
        and language = lang
        limit 1
        into comma_string;

        select string from dict_punctuation
        where token = 'and'
        and language = lang
        limit 1
        into and_string;

        for i in select distinct item from known
        loop
          for c in select token from known where item = i limit 1
          loop
            if exists (select token from item_count where token = c)
            then
              update item_count set count = count+1 where token = c;
            else
              insert into item_count (token, count) values (c::varchar, 1::integer);
            end if;
          end loop;
        end loop;

        current_row := 0;
        select count(token) from item_count into row_count;

        for this_row in select * from item_count
        loop
          current_row := current_row + 1;

          if this_row.count > 1
          then this_context := 'plural';
          else this_context := 'singular';
          end if;

          select string from dict_tokens
          where token = this_row.token
          and context = this_context
          and language = lang
          limit 1
          into this_string;

          if this_string is null then
            this_string := concat('[', this_context, ':', this_row.token, '] (COUNT)');
          end if;

          this_string := regexp_replace(this_string, 'COUNT', this_row.count::varchar); 
          
          if row_count = current_row + 1
          then
            this_string := this_string || and_string || ' ';
          elsif row_count > current_row
          then
            this_string := this_string || comma_string || ' ';
          end if;

          if result is null
          then result := this_string;
          else result := result || this_string;
          end if;

        end loop;

        drop table item_count;
        drop table known;

        if result is null
        or result like ''::text
        then result := 'NULL'::text;
        end if;
      elsif this_parser = 'collapsed_list'
      then
        perform log_extractor_event('parser:collapsed_list', 'Collapsing objects');

        create temp table known on commit drop as 
          select * from identify_objects(buffer, this_expression, make_array, skip_meme, recall);

        create temp table items_stack (
          token varchar,
          count integer
        ) on commit drop;

        select string from dict_punctuation
        where token = 'comma'
        and language = lang
        limit 1
        into comma_string;

        select string from dict_punctuation
        where token = 'and'
        and language = lang
        limit 1
        into and_string;

        for i in select distinct item from known
        loop
          for c in select token from known where item = i limit 1
          loop
            if not exists (select token from items_stack where token = c)
            then
              insert into items_stack (token) values (c::varchar);
            end if;
          end loop;
        end loop;

        current_row := 0;
        select count(token) from items_stack into row_count;

        for this_row in select * from items_stack
        loop
          current_row := current_row + 1;

          select string from dict_tokens
          where token = this_row.token
          and context = this_context
          and language = lang
          limit 1
          into this_string;

          if this_string is null then
            this_string := concat('[', this_context, ':', this_row.token, ']');
          end if;

          if row_count = current_row + 1
          then
            this_string := this_string || and_string || ' ';
          elsif row_count > current_row
          then
            this_string := this_string || comma_string || ' ';
          end if;

          if result is null
          then result := this_string;
          else result := result || this_string;
          end if;

        end loop;

        drop table items_stack;
        drop table known;

        if result is null
        or result like ''::text
        then result := 'NULL'::text;
        end if;
      elsif this_parser = 'translate_collapsed_list'
      then
        perform log_extractor_event('parser:translate_collapsed_list', 'Collapsing translations');

        create temp table items_stack (
          token varchar
        ) on commit drop;

        foreach i in array array(select distinct * from jsonb_array_elements_text(buffer->this_expression))
        loop
          perform log_extractor_event('parser:translate_collapsed_list',
            concat('Found item: "', i, '"'));
          insert into items_stack values (i);
        end loop;

        select string from dict_punctuation
        where token = 'comma'
        and language = lang
        limit 1
        into comma_string;

        select string from dict_punctuation
        where token = 'and'
        and language = lang
        limit 1
        into and_string;

        current_row := 0;
        select count(token) from items_stack into row_count;

        for this_row in select * from items_stack
        loop
          current_row := current_row + 1;

          select string from dict_tokens
          where token = this_row.token
          and context = this_context
          and language = lang
          limit 1
          into this_string;

          if this_string is null then
            this_string := concat('[', this_context, ':', this_row.token, ']');
          end if;

          perform log_extractor_event('parser:translate_collapsed_list',
            concat('"', this_row.token, '" -> "', this_string, '"'));

          if row_count = current_row + 1
          then
            this_string := this_string || and_string || ' ';
          elsif row_count > current_row
          then
            this_string := this_string || comma_string || ' ';
          end if;

          if result is null
          then result := this_string;
          else result := result || this_string;
          end if;

        end loop;

        drop table items_stack;

        if result is null
        or result like ''::text
        then result := 'NULL'::text;
        end if;
      else -- default
        perform log_extractor_event('parser:default', 'Fall back to default parser');

        create temp table known on commit drop as 
          select * from identify_objects(buffer, this_expression, make_array, skip_meme, recall);

        for i in select distinct item from known
        loop
          this_description := null;

          for c in select token from known where item = i
          loop
            select string from dict_tokens
            where token = c
            and context = this_context
            and language = lang
            limit 1
            into this_string;

            if this_string is null then this_string := concat('[', this_context, ':', c, ']'); end if;

            if this_description is null
            then this_description := this_string;
            elsif c is not null
            then this_description := this_string || ' ' || this_description;
            end if;
          end loop;

          if result is null
          then result := this_description;
          else result := result || this_description;
          end if;
        end loop;

        drop table known;
      end if;

      if capitalize is true
      then result := capitalize_word(result);
      end if;
    else
      result := word;
    end if;

    perform log_word_event(log_class, concat('Result: "', result, '"'));
    RETURN result;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION capitalize_word(
  word TEXT
)
RETURNS TEXT AS $$
  DECLARE
    result TEXT;
    first_char TEXT;
    first_capitalized TEXT;
    remainder_chars TEXT;
  BEGIN
    select substring(word from 1 for 1) into first_char;
    select substring(word from 2) into remainder_chars;
    select upper(first_char) into first_capitalized;
    select first_capitalized || remainder_chars into result;
    return result;
  END;
$$ language plpgsql;

