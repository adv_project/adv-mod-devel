CREATE OR REPLACE FUNCTION push_response(
  buffer          JSONB
) RETURNS VOID AS $$
  DECLARE
    position integer;
  BEGIN
    perform log_responses_stack_event('info', 'push_response',
      concat(':: Pushing response: "', buffer->>'response_class', '"'), buffer::jsonb);
    buffer := resolve_response(buffer);
    perform log_responses_stack_event('info', 'push_response', '-> Resolved response:');
    perform log_responses_stack_event('info', '',
      array_to_string(array(select * from jsonb_array_elements_text(buffer->'messages')), ', ') , buffer::jsonb);
    select ord from responses_stack order by ord desc limit 1 into position;
    if position is null
    then position := 1;
    else position = position + 1;
    end if;
    insert into responses_stack ( response, ord ) values ( buffer, position );
    perform flush_epiphanies_stack();
    RETURN;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION push_epiphany(
  buffer          JSONB
) RETURNS VOID AS $$
  BEGIN
    perform log_responses_stack_event('info', 'push_spiphany',
      concat(':: Pushing epiphany: "', buffer->>'response_class', '"'), buffer::jsonb);
    if exists ( select string from dict_classes where class_id = buffer->>'response_class' )
    then
      buffer := resolve_response(buffer);
      perform log_responses_stack_event('info', 'push_epiphany', '-> Resolved response:');
      perform log_responses_stack_event('info', '',
        array_to_string(array(select * from jsonb_array_elements_text(buffer->'messages')), ', ') , buffer::jsonb);
      insert into epiphanies_stack values (buffer);
    ELSE
      perform log_responses_stack_event('info', 'push_epiphany', '* skipping (undefined) *');
    END IF;
    RETURN;
  END;
$$ language plpgsql;

