CREATE OR REPLACE FUNCTION push_trigger(
  trigger_id  VARCHAR,
  buffer      JSONB
) RETURNS VOID AS $$
  DECLARE
    n BIGINT;
  
  BEGIN
    SELECT ord
    FROM   triggers_stack
    ORDER BY ord DESC
    LIMIT 1
    INTO n;

    IF n IS NULL THEN
      n := 0;
    END IF;
    perform log_trigger_stack_event('info',
                      'push_trigger',
           concat(    'Pushing trigger: ', trigger_id::text));
    INSERT INTO triggers_stack (
      ord,
      trigger_id,
      buffer
    )
    VALUES (
      n+1,
      trigger_id,
      buffer
    );

    RETURN;
  END;
$$ language plpgsql;


