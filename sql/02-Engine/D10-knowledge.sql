CREATE OR REPLACE FUNCTION resolve_datum(
  token   VARCHAR,
  action  VARCHAR,
  buffer  JSONB
) RETURNS BOOLEAN AS $$
  DECLARE
    self_id varchar;
    retval boolean;
    trigg varchar;
    trigg_task varchar;
    this_buf jsonb;
    call record;
    trigg_exists boolean;
    recognized boolean;
  BEGIN
    self_id := resolve_recipient(buffer);
    retval := true;
    trigg_exists := false;
    select concat('datum_', action) into trigg_task;
    perform log_datum_event(    'resolve_datum',
                    concat(     token, ' with task ', trigg_task), jsonb_pretty(buffer));

    for trigg in                                    /* TAG: LOOK FOR DATUM TRIGGERS FOR THIS CLASS */
      select t.id from triggers t
      where t.target = token
      and t.task = trigg_task
      and t.type = 'resolve_datum'
    loop
      trigg_exists := true;
      select trigg as trigger_id, buffer as buffer into call;
      this_buf := pull_trigger(call);                           --<-- the trigger is pulled into the buffer
      perform log_datum_event(    'resolve_datum',
                      concat(     'found trigger: ', trigg), this_buf::TEXT);
      select this_buf->>'recognized_datum' like '%true%'
      or not this_buf ? 'recognized_datum'
      into retval;
      
      if retval is false
      then exit;
      end if;
    end loop;                                       /* TAG: LOOK FOR DATUM TRIGGERS FOR THIS CLASS */
    
    select retval is true or not trigg_exists into recognized;
    perform log_datum_event('resolve_datum', 'Returning ' || recognized::text);
    return recognized;
  END;
$$ language plpgsql;

