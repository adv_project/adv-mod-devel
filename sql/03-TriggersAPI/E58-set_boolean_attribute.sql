CREATE OR REPLACE FUNCTION jset_boolean_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    attribute_name    VARCHAR;
    attribute_id      VARCHAR;
    attribute_target  VARCHAR;
    previous_value    BOOLEAN;
    all_inherited     VARCHAR[];
    b                 BOOLEAN;
    hook              VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;

    -- TODO: optionally use condition's arg, test and/or query result, etc
    IF buffer ? 'attribute_id'
    THEN
      attribute_id := (buffer->>'attribute_id');
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_id_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_name'
    THEN
      attribute_name := buffer->>'attribute_name';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_name_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_target'
    THEN
      attribute_target := quote_ident(buffer->>'attribute_target');
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_target_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    if exists (select e.id from everything e where e.id = attribute_id)
    then
      select get_boolean_attribute_value(attribute_id, attribute_name)
      into previous_value;
      if attribute_target like '%true%' then
        b := true;
      else
        b := false;
      end if;
      
      select modify_boolean_attribute(
        attribute_id,
        attribute_name,
        'set',
        b
      ) into b;
    else
      result := result || jsonb_build_object(
        'error'::text,    'attribute_id_does_not_exist: '::text || attribute_id::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    end if;

    result := result || jsonb_build_object(
      'previous_value'::text,  previous_value::text
    );

    perform trigger_hook(attribute_id, 'hook_listen_boolean_attribute_' || attribute_name, buffer::jsonb || result::jsonb);

    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

