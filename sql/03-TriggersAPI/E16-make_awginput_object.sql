CREATE OR REPLACE FUNCTION make_awginput_object(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    owner_id        VARCHAR;
    world_id        VARCHAR;
    attr            VARCHAR;
    gtype           varchar;
    w               RECORD;
		awg_attributes  JSONB;
    result          JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select o.id from objects o where 'world' = any(o.classes) limit 1 into world_id;
    select target from links where type = 'attribute' and name = 'geometry_type' and id = world_id into gtype;
    
    for attr in select attribute from attributes_index where id = 'world'
    loop
      perform initialize_attribute(world_id, attr);
    end loop;
    
    select * from worlds where id = world_id into w;

    awg_attributes := row_to_json(w)::jsonb || jsonb_build_object(
      'geometry_type'::text,  gtype::text,
      'response_type', 'awginput',
      'response_class', 'creating_world'
    );

    result := result || jsonb_build_object(
      'retval'::text,         true::boolean
    );
    
    perform push_response(awg_attributes);

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

