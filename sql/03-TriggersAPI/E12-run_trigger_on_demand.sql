CREATE OR REPLACE FUNCTION jtrigger_hook(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    this_buf jsonb;
    caller varchar;
    hook varchar;
    o varchar;
    result  JSONB;
	  log_class VARCHAR;
  BEGIN
    log_class := 'jtrigger_hook';
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    hook := resolve_condition_arg(this_condition.arg, buffer);
    perform log_test_event(log_class, concat('=> Trigger hook ', hook, 'on query'), query);

    for o in select * from jsonb_array_elements_text(query->'this_query')
    loop
      this_buf := buffer || jsonb_build_object('caller', o);
      perform log_test_event(log_class, concat('-> trigger hook ', hook, ' for ', o), buffer);
      perform trigger_hook(o, hook, buffer);
    end loop;

    result := result || jsonb_build_object('retval', true);
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jrun_trigger_on_demand(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    c varchar;
    t varchar;
    this_buf jsonb;
    trigger_task varchar;
    result  JSONB;
	  log_class VARCHAR;
  BEGIN
    log_class := 'jrun_trigger_on_demand';
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    trigger_task := resolve_condition_arg(this_condition.arg, buffer);
    
    perform log_test_event(log_class, concat('=> Run trigger ', trigger_task, 'on query'), query);

    for c in select * from jsonb_array_elements_text(query->'this_query')
    loop
      for t in
        select t.id from triggers t, inherited i
        where i.id = c
        and i.inherits = t.target
        and t.task = trigger_task
      loop
        --if not buffer ? 'anonymous'
        --then
        --  this_buf := buffer || jsonb_build_object('caller', o);
        --else
          this_buf := buffer;
        --end if;
        perform log_test_event(log_class, concat('-> Push trigger ', t), this_buf);
        perform push_trigger(t, this_buf);
      end loop;
    end loop;

    result := result || jsonb_build_object('retval', true);
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jtry_all_recipes(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    this_buf jsonb;
    trigger_task varchar;
    proposed_target varchar;
    t varchar;
    result  JSONB;
    call record;
    success boolean;
    recipe_error varchar;
	  log_class VARCHAR;
    this_result jsonb;
  BEGIN
    log_class := 'jtry_all_recipes';
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    trigger_task := buffer->>'proposed_trigger_task';
    proposed_target := buffer->>'target';
    
    perform log_test_event(log_class, concat('=> Pulling trigger ', trigger_task, ' on ', proposed_target));
    success := false;
    
    for t in
      select t.id from triggers t
      where t.target = proposed_target
      and t.task = trigger_task
      and t.type = 'recipe'
    loop
      --if not buffer ? 'anonymous'
      --then
      --  this_buf := buffer || jsonb_build_object('caller', o);
      --else
        this_buf := buffer;
      --end if;

      perform log_test_event(log_class, concat(' -> Found trigger: "', trigger_task, '"'));
      select t as trigger_id, this_buf as buffer into call;
      this_result := pull_trigger(call);                           --<-- the trigger is pulled hard
      if this_result->>'success' = 'true'
      then
        buffer := this_result;
        perform log_test_event(log_class, concat(' :: RECIPE OK'));
        success := true;
        exit;
      else
        if not buffer ? 'response_class'
        then
          buffer := buffer || jsonb_build_object(
            'response_class', this_result->>'response_class'
          );
        end if;
      end if;
    end loop;

    result := buffer || jsonb_build_object('retval', success);
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

