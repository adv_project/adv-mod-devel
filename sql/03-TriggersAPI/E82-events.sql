CREATE OR REPLACE FUNCTION jshow_displacement_event(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    caller_parent varchar;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);

    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'recipient', caller
    );

    /* Check out the respective location to guess what happened */
    caller_parent := pick_parent(caller);

    if    buffer->>'old_parent' = caller_parent
    and   buffer->>'old_parent' != caller
    and   buffer->>'parent_id' != caller
    and   buffer ? 'proposed_displacement'
    then
      -- Someone left caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_left_the_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'proposed_parent' = caller
    and   buffer->>'job' like 'pick'
    then
      -- Caller takes something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_take_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller
    and   buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Caller drops something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_drop_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'child_id' = caller
    then
      -- Caller enters the place
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_are_here'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Someone dropped it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_drops_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'job' like 'pick'
    then
      -- Someone picked it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_takes_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    then
      -- Someone enters caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    else
      -- We give in
      this_buf := buffer || jsonb_build_object(
        'response_class', 'something_happens'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jshow_movement_event(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    caller_parent varchar;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);

    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'recipient', caller
    );

    /* Check out the respective location to guess what happened */
    caller_parent := pick_parent(caller);

    if    buffer->>'old_parent' = caller_parent
    and   buffer->>'old_parent' != caller
    and   buffer->>'parent_id' != caller
    and   buffer ? 'proposed_displacement'
    then
      -- Someone left caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_left_the_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'proposed_parent' = caller
    and   buffer->>'job' like 'pick'
    then
      -- Caller takes something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_take_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller
    and   buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Caller drops something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_drop_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'child_id' = caller
    then
      -- Caller enters the place
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_are_here'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Someone dropped it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_drops_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'job' like 'pick'
    then
      -- Someone picked it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_takes_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    then
      -- Someone enters caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    else
      -- We give in
      this_buf := buffer || jsonb_build_object(
        'response_class', 'something_happens'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jshow_displacement_event(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    caller_parent varchar;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);

    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'recipient', caller
    );

    /* Check out the respective location to guess what happened */
    caller_parent := pick_parent(caller);

    if    buffer->>'old_parent' = caller_parent
    and   buffer->>'old_parent' != caller
    and   buffer->>'parent_id' != caller
    and   buffer ? 'proposed_displacement'
    then
      -- Someone left caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_left_the_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'proposed_parent' = caller
    and   buffer->>'job' like 'pick'
    then
      -- Caller takes something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_take_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller
    and   buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Caller drops something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_drop_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'child_id' = caller
    then
      -- Caller enters the place
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_are_here'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Someone dropped it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_drops_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'job' like 'pick'
    then
      -- Someone picked it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_takes_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    then
      -- Someone enters caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    else
      -- We give in
      this_buf := buffer || jsonb_build_object(
        'response_class', 'something_happens'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jshow_displacement_event(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    caller_parent varchar;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);

    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'recipient', caller
    );

    /* Check out the respective location to guess what happened */
    caller_parent := pick_parent(caller);

    if    buffer->>'old_parent' = caller_parent
    and   buffer->>'old_parent' != caller
    and   buffer->>'parent_id' != caller
    and   buffer ? 'proposed_displacement'
    then
      -- Someone left caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_left_the_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'proposed_parent' = caller
    and   buffer->>'job' like 'pick'
    then
      -- Caller takes something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_take_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller
    and   buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Caller drops something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_drop_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'child_id' = caller
    then
      -- Caller enters the place
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_are_here'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Someone dropped it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_drops_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'job' like 'pick'
    then
      -- Someone picked it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_takes_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    then
      -- Someone enters caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    else
      -- We give in
      this_buf := buffer || jsonb_build_object(
        'response_class', 'something_happens'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

