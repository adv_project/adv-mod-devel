CREATE OR REPLACE FUNCTION jshow_acquired_knowledge(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    this_condition record;
    all_known varchar[];
    k varchar;

  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient' then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;

    select array(select * from jsonb_array_elements_text(query->'this_query')) into all_known;
    
    if all_known <> array[]::varchar[]
    then
      foreach k in array array(select * from jsonb_array_elements_text(query->'this_query'))
      loop
        this_buf := buffer || jsonb_build_object(
          'target', k,
          'response_class', 'knowledge_description',
          'response_type', 'message'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
      end loop;
    else
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_know_nothing',
        'response_type', 'message'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

