CREATE OR REPLACE FUNCTION read_string_attribute(
  object_id  VARCHAR,
  attr_name VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    attribute_value VARCHAR;
  BEGIN
    select l.value from string_attributes l
    where l.id = object_id
    and l.attribute_name = attr_name
    into attribute_value;
    
    return attribute_value;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION qread_string_attribute(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    attribute_name VARCHAR;
    attribute_value VARCHAR;
    attribute_id VARCHAR;
    this_condition record;
    retval boolean;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;
    
    attribute_id := resolve_caller(buffer);
    attribute_name := resolve_condition_arg(this_condition.arg, buffer);
    attribute_value := read_string_attribute(attribute_id, attribute_name);
    select attribute_value is not null into retval;

    result := jsonb_build_object(
      'this_query', attribute_value,
      'retval', retval
    );

    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jread_string_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    attribute_name VARCHAR;
    attribute_value VARCHAR;
    attribute_id VARCHAR;
    write_field VARCHAR;
    this_condition record;
    retval boolean;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;
    
    attribute_name := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    attribute_id := query->>'this_query';
    attribute_value := read_string_attribute(attribute_id, attribute_name);
    select attribute_value is not null into retval;

    result := jsonb_build_object(
      write_field, attribute_value,
      'retval', retval
    );

    return result;
	END;
$$ LANGUAGE plpgsql;

