CREATE OR REPLACE FUNCTION jshow_debug_message(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    result JSONB;
    caller varchar;
    this_condition record;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    caller := resolve_caller(buffer);
    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'response_class', '<<<DEBUG>>> '::text || buffer::text,
      'recipient', caller
    );
    perform jtrigger_response(buffer, '{"this_query":[]}'::jsonb, condition_id);
    result := jsonb_build_object(
      'retval', true
    );
    return result;
  END;
$$ LANGUAGE plpgsql;

