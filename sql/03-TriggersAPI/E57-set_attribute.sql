CREATE OR REPLACE FUNCTION jset_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    attribute_name      VARCHAR;
    attribute_id        VARCHAR;
    attribute_target    VARCHAR;
    attribute_modifier  VARCHAR;
    previous_value      NUMERIC;
    i                   NUMERIC;
    new_value           NUMERIC;
    hook                VARCHAR;
    retval boolean;
    result              JSONB;
	
  BEGIN
    result := '{}'::jsonb;

    -- TODO: optionally use condition's arg, test and/or query result, etc
    attribute_id := (buffer->>'attribute_id');
    attribute_name := buffer->>'attribute_name';
    attribute_target := quote_ident(buffer->>'attribute_target');
    previous_value := get_attribute_value(attribute_id, attribute_name);

    IF buffer ? 'attribute_modifier'
    THEN
      attribute_modifier := quote_ident(buffer->>'attribute_modifier');
    ELSE
      attribute_modifier := 'set';
    END IF;
    
    i := attribute_target::NUMERIC;
      
    select modify_attribute(
      attribute_id,
      attribute_name,
      attribute_modifier,
      i
    ) into new_value;

    result := result || jsonb_build_object(
      'previous_value'::text,  previous_value,
      'new_value'::text,  new_value
    );

    perform trigger_hook(attribute_id, 'hook_listen_attribute_' || attribute_name, buffer::jsonb || result::jsonb);
    select new_value is not null into retval;

    result := result || jsonb_build_object(
      'retval'::text,   retval
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

