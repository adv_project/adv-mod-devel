CREATE OR REPLACE FUNCTION jshow_available_displacements(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    d record;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient' then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;

    for d in select * from displacements where id = caller
    loop
      this_buf := buffer || jsonb_build_object(
        'direction', array[d.direction]::varchar[],
        'displacement', array[d.displacement]::varchar[],
        'destination', array[d.destination]::varchar[],
        'response_class', 'you_can_displace',
        'response_type', 'message'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end loop;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

