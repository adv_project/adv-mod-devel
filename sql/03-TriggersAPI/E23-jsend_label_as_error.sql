CREATE OR REPLACE FUNCTION jsend_label_as_error(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		label   VARCHAR;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select c.label from conditions c where c.id = condition_id into label;

    result := result || jsonb_build_object(
      'error'::text,      'error_'::text || label::text,
      'retval'::text,     true::boolean
    );

    perform push_response(result);
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

