CREATE OR REPLACE FUNCTION create_object(
  inherits varchar[],
  is_active boolean default false
)
RETURNS VARCHAR AS $$
  DECLARE
    created_id varchar;
  BEGIN
		created_id := make();

		UPDATE everything
    SET
      type = 'instance',
      in_table = 'objects'
    WHERE
      id = created_id;

  	INSERT INTO objects
    VALUES (
      created_id,
      inherits,
      is_active
    );

    perform update_inherited(created_id);
    return created_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION do_create_object(
  inherits varchar[],
  is_active boolean default false,
  buffer jsonb default '{}'::jsonb
)
RETURNS VARCHAR AS $$
  DECLARE
    created_id varchar;
    c varchar;
    i integer;
    component_labels varchar[];
  BEGIN
		created_id := create_object(inherits, is_active);
    buffer := buffer || jsonb_build_object('created_id', created_id);

/*
    if buffer ? 'make_components'
    then
      for c in select * from jsonb_array_elements_text(buffer->'spawn_components')
      loop
      end loop;
    end if;

    if buffer ? 'components'
    then
      i := 0;
      
      if buffer ? 'component_labels'
      then
        component_labels := array[]::varchar[] || array(
          select * from jsonb_array_elements_text(buffer->'component_labels')
        );
      else
        component_labels := array[]::varchar[];
      end if;

      for c in select * from jsonb_array_elements_text(buffer->'make_components')
      loop
        i := i + 1;
        perform do_make_component(created_id, c, buffer || jsonb_build_object(
          'this_component', c,
          'this_component_label', component_labels[i]
          )
        );
      end loop;
    end if;
*/
    perform update_inherited(created_id);
    
    perform trigger_hook(created_id, 'hook_create', buffer::jsonb);
    return created_id;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jcreate_object(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		created_id      VARCHAR;
    inherits        VARCHAR[];
    is_active       BOOLEAN;
    result          JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    inherits := array(
      SELECT *
      FROM jsonb_array_elements_text(buffer->'classes')
    );

    IF buffer ? 'active'
    THEN
      is_active := buffer->>'active' like '%true%';
    ELSE
      is_active := TRUE;
    END IF;
    
    select do_create_object(inherits, is_active, buffer)
    into created_id;

    result := result || jsonb_build_object(
      'created_id'::TEXT, created_id::TEXT,
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jcreate_object_in_place(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		created_id      VARCHAR;
    inherits        VARCHAR[];
    is_active       BOOLEAN;
    jcreate_result  JSONB;
    jprepare_result  JSONB;
    result          JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    jcreate_result := jcreate_object(buffer, query, condition_id);
    created_id := jcreate_result->>'created_id';
    buffer := buffer || jsonb_build_object('proposed_child', created_id);
    jprepare_result := jprepare_location_fallback(buffer, query, condition_id);
    
    if jprepare_result->>'retval' like '%true%'
    then
      perform jmake_location(jprepare_result, query, condition_id);
    end if;

    result := result || jsonb_build_object(
      'created_id'::TEXT, created_id::TEXT,
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_spawned_class(
  proposed_class  varchar,
  proposed_source varchar
) RETURNS varchar AS $$
  DECLARE
    trigg_task      varchar;
    cond_id         varchar;
    cond            record;
    total           numeric;
    roll            numeric;
    pointer         numeric;
    r               record;
    selected_class  varchar;
  BEGIN
    select 'spawn_'::varchar || proposed_class into trigg_task;
    if exists (
      select i.id from triggers t, inherited i
      where t.type = 'spawn_resource'
      and t.task = trigg_task
      and i.id = proposed_source
      and t.target = i.inherits
    )
    then
      create temp table our_available_resources (
        resource_class  varchar,
        probability     numeric
      ) on commit drop;

      total := 0;

      for cond_id in
        select c.id from triggers t, inherited i, conditions c
        where t.type = 'spawn_resource'
        and t.task = trigg_task
        and i.id = proposed_source
        and t.target = i.inherits
        and c.target = t.id
      loop
        select * from conditions where id = cond_id into cond;
        if exists ( select resource_class from our_available_resources where resource_class = cond.arg )
        then
          update our_available_resources o
          set probability = o.probability + cond.value
          where o.resource_class = cond.arg;
        else
          insert into our_available_resources (
            resource_class, probability
          ) values (
            cond.arg, cond.value
          );
        end if;
        total := total + cond.value;
      end loop;

      roll := round(random()*total);
      pointer := 0;

      for r in select * from our_available_resources
      loop
        if roll <= pointer + r.probability
        then
          selected_class := r.resource_class;
          exit;
        else
          pointer := pointer + r.probability;
        end if;
      end loop;

      drop table our_available_resources;
    end if;

    if selected_class is null
    then return proposed_class;
    else return selected_class;
    end if;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_component(
  object_id varchar,
  component_class varchar,
  component_label varchar default 'body'::varchar
)
RETURNS VARCHAR AS $$
  DECLARE
    created_id varchar;
  BEGIN
    select create_object(array[component_class]::varchar[], false) into created_id;
    insert into links (id, target, type, name)
      values (object_id, created_id, 'component', component_label);
    return created_id;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_spawn_components(
  object_id varchar,
  proposed_source varchar
) RETURNS VOID AS $$
  DECLARE
    cond record;
    i numeric;
    cnt numeric;
    proposed_class varchar;
    resolved_class varchar;
    created_id varchar;
    component_label varchar;
  BEGIN
    if object_id is null then return; end if;
    for cond in
      select c.* from conditions c, triggers t, inherited i
      where i.id = object_id
      and t.type = 'recipe'
      and t.task = 'spawn' 
      and i.inherits = t.target
      and c.target = t.id
    loop
      if cond.test_name = 'make_component'
      then
        if cond.amount is null or cond.amount < 1
        then i := 1;
        else i := cond.amount;
        end if;

        if cond.write is null or cond.write = ''::varchar
        then component_label := 'body';
        else component_label := cond.write;
        end if;

        select cond.arg into proposed_class;

        cnt := 0;
        loop
          cnt := cnt + 1;
          if cnt > i then exit; end if;
          select resolve_spawned_class(proposed_class, proposed_source) into resolved_class;
          select make_component(object_id, resolved_class, component_label) into created_id;
          /* Here it goes recursive (the function calls itself) */
          perform make_spawn_components(created_id, proposed_source);
        end loop;
      end if;
    end loop;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jspawn_object_in_place(
  buffer jsonb,
  query jsonb,
  condition_id varchar
) RETURNS jsonb AS $$
  DECLARE
    proposed_class varchar;
    proposed_parent varchar;
    spawned_class varchar;
    spawned_classes varchar[];
    this_buffer jsonb;
    this_result jsonb;
  BEGIN
    select buffer->>'proposed_class' into proposed_class;
    select buffer->>'proposed_parent' into proposed_parent;
    select resolve_spawned_class(proposed_class, proposed_parent) into spawned_class;
    select array[spawned_class]::varchar[] into spawned_classes;
    select buffer || jsonb_build_object('classes', spawned_classes::varchar[]) into this_buffer;
    select jcreate_object_in_place(this_buffer, query, condition_id) into this_result;
    perform make_spawn_components(this_result->>'created_id', proposed_parent);
    return this_result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_into_component(
  object_id varchar,
  component_id varchar,
  component_label varchar default 'body'::varchar
)
RETURNS VOID AS $$
  BEGIN
        perform log_test_event('abracadabra', 'making components ' || object_id || ': ' || component_id);
    delete from links where id = component_id and type = 'parent';
    update objects set active = false where id = component_id;
    insert into links (id, target, type, name)
      values (object_id, component_id, 'component', component_label);
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmake_build_job(
  buffer jsonb,
  query jsonb,
  condition_id varchar
) RETURNS jsonb AS $$
  DECLARE
    proposed_class varchar;
    proposed_parent varchar;
    spawned_class varchar;
    spawned_classes varchar[];
    created_id varchar;
    this_buffer jsonb;
    this_result jsonb;
    s varchar;
  BEGIN
    select jcreate_object_in_place(buffer, query, condition_id) into this_result;
    select buffer || this_result into this_result;
    select this_result->>'created_id' into created_id;

    if this_result ? 'components'
    then
      foreach s in array array(select * from jsonb_array_elements_text(buffer->'components'))
      loop
        /* TODO: manage component labels (defaults to body for now) */
        perform make_into_component(created_id, s);
      end loop;
    end if;

    if this_result ? 'materials'
    then
      foreach s in array array(select * from jsonb_array_elements_text(buffer->'materials'))
      loop
        perform destroy_object(s);
      end loop;
    end if;

    return this_result;
	END;
$$ LANGUAGE plpgsql;

/*
CREATE OR REPLACE FUNCTION do_make_component(
  object_id varchar,
  component_id varchar,
  buffer jsonb default '{}'::jsonb
)
RETURNS void AS $$
  BEGIN
    if buffer ? 'this_component_label'
    then perform make_component(object_id, component_id, buffer->>'this_component_label');
    else perform make_component(object_id, component_id);
    end if;

    perform trigger_hook(component_id, 'hook_create_as_component', buffer::jsonb);
    return;
	END;
$$ LANGUAGE plpgsql;
*/

CREATE OR REPLACE FUNCTION destroy_object(
  object_id varchar
)
RETURNS BOOLEAN AS $$
  DECLARE
    components varchar[];
  BEGIN
    if not exists (
      select id from objects
      where id = object_id
    )
    then return false;
    end if;

    /* Destroy all components */
    /* TODO: not implemented */
--  select get_components_recursive(object_id) into components;
    
		DELETE FROM everything WHERE id = object_id;

    return true;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jdestroy_objects(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    o varchar;
    success boolean;
    destroyed varchar[];
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'destroyed_objects');
    select array[]::varchar[] into destroyed;

    for o in select * from jsonb_array_elements_text(query->'this_query')
    loop
      select destroy_object(o) into success;
      if success
      then destroyed := destroyed || o::varchar;
      end if;
    end loop;
    
    result := result || jsonb_build_object(
      write_field, destroyed,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmake_extraction_job(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    result jsonb;
    iterations numeric;
    chance numeric;
    i integer;
    c varchar;
    this_buf jsonb;
    this_result jsonb;
    proposed_parent varchar;
    collected varchar[];
    this_condition record;
    proposed_resource varchar;
    proposed_source varchar;
    spawned_class varchar;
    roll numeric;
	
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    result := '{}'::jsonb;
    collected := array[]::varchar[];
    i := 0;
    iterations := this_condition.amount;
    if iterations is null or iterations < 1 then iterations := 1; end if;
    chance := this_condition.value;
    if chance is null or chance > 100 then chance := 100; end if;
    proposed_resource := buffer->>'proposed_resource';
    proposed_source := buffer->>'proposed_source';

    if buffer ? 'proposed_parent'
    then proposed_parent := buffer->>'proposed_parent';
    else proposed_parent := proposed_source;
    end if;

    loop
      i := i + 1;
      if i > iterations or i > 50
      then exit;
      end if;
      
      roll := round(random()*100);
      if chance > roll
      then
        select resolve_spawned_class(proposed_resource, proposed_source) into spawned_class;
        this_buf := buffer || jsonb_build_object(
          'proposed_parent', proposed_parent,
          'classes', array[spawned_class]::varchar[]
        );

        this_result := jcreate_object_in_place(this_buf, query, condition_id);
        collected := collected || array[this_result->>'created_id']::varchar[];
      end if;
    end loop;
    
    result := result || jsonb_build_object(
      'collected_objects', collected,
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

