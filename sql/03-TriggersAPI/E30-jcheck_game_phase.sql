CREATE OR REPLACE FUNCTION jcheck_game_phase(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		player_username   VARCHAR;
		player_id         VARCHAR;
    game_phase        VARCHAR;
    target_game_phase VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    player_username := buffer->>'player';
    select id from links where type = 'attribute' and name = 'username' and target = player_username into player_id;
    select target from links where id = player_id and type = 'attribute' and name = 'game_phase' into game_phase;
    select c.arg from conditions c where c.id = condition_id into target_game_phase;

    if game_phase = target_game_phase then
      result := result || jsonb_build_object(
        'retval'::text,     true::boolean
      );
    else
      result := result || jsonb_build_object(
        'retval'::text,     false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jcheck_help_available(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		player_username   VARCHAR;
		player_id         VARCHAR;
    lang VARCHAR;
    retval boolean;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    player_username := buffer->>'player';
    select id from links where type = 'attribute' and name = 'username' and target = player_username into player_id;
    select target from links where type = 'attribute' and name = 'language' and id = player_id into lang;

    select exists (
      select string from dict_classes
      where language = lang
      and class_id = buffer->>'help_token'
    )
    into retval;

    result := result || jsonb_build_object(
      'retval'::text,     retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jtranslate_world_parameter(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    player_id varchar;
    lang VARCHAR;
    proposed_token VARCHAR;
    param_name VARCHAR;
    buffer_field VARCHAR;
    this_condition record;
    retval boolean;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    player_id = buffer->>'player_id';
    select target from links where type = 'attribute' and name = 'language' and id = player_id into lang;
    proposed_token := resolve_condition_arg(this_condition.arg, buffer);

    if is_valid(this_condition.write)
    then buffer_field := this_condition.write;
    else buffer_field := 'game_phase';
    end if;

    select string from dict_world_params
    where language = lang
    and token = proposed_token
    limit 1
    into param_name;
    
    if param_name is not null
    then
      retval = true;
    else
      param_name = 'unknown';
      retval = false;
    end if;

    result := jsonb_build_object(
      buffer_field, param_name,
      'retval', retval::boolean
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

