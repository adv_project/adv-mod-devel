CREATE OR REPLACE FUNCTION jset_string_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    attribute_id      VARCHAR;
    attribute_name    VARCHAR;
    attribute_target  VARCHAR;
    previous_value    VARCHAR;
    all_inherited   VARCHAR[];
    hook            VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;

    -- TODO: optionally use condition's arg, test and/or query result, etc
    IF buffer ? 'attribute_id'
    THEN
      attribute_id := buffer->>'attribute_id';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_id_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_name'
    THEN
      attribute_name := buffer->>'attribute_name';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_name_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_target'
    THEN
      attribute_target := buffer->>'attribute_target';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_target_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    if exists (select e.id from everything e where e.id = attribute_id)
    then
      select l.target from links l where
        l.id = attribute_id and
        l.type = 'attribute' and
        l.name = attribute_name
      into previous_value;
      delete from links l where
        l.id = attribute_id and
        l.type = 'attribute' and
        l.name = attribute_name;
      insert into links (
        id, type, name, target
      ) values (
        attribute_id, 'attribute', attribute_name, attribute_target
      );
    else
      result := result || jsonb_build_object(
        'error'::text,    'attribute_id_does_not_exist: '::text || attribute_id::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    end if;

    result := result || jsonb_build_object(
      'previous_value'::text,  previous_value::text
    );
    perform trigger_hook(attribute_id, 'hook_listen_string_attribute_' || attribute_name, buffer::jsonb || result::jsonb);

    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/* TODO: this function is an almost exact duplicate of jset_string_attribute() -- FIX */
CREATE OR REPLACE FUNCTION jset_init_string_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    attribute_id      VARCHAR;
    attribute_name    VARCHAR;
    attribute_target  VARCHAR;
    previous_value    VARCHAR;
    all_inherited   VARCHAR[];
    hook            VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;

    -- TODO: optionally use condition's arg, test and/or query result, etc
    IF buffer ? 'attribute_id'
    THEN
      attribute_id := buffer->>'attribute_id';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_id_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_name'
    THEN
      attribute_name := buffer->>'attribute_name';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_name_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    IF buffer ? 'attribute_target'
    THEN
      attribute_target := buffer->>'attribute_target';
    ELSE
      result := result || jsonb_build_object(
        'error'::text,    'no_attribute_target_provided'::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    END IF;
    
    if exists (select e.id from everything e where e.id = attribute_id)
    then
      delete from links l where
        l.id = attribute_id and
        l.type = 'init' and
        l.name = attribute_name;
      insert into links (
        id, type, name, target
      ) values (
        attribute_id, 'init', attribute_name, attribute_target
      );
    else
      result := result || jsonb_build_object(
        'error'::text,    'attribute_id_does_not_exist: '::text || attribute_id::text,
        'retval'::text,   false::boolean
      );
		  RETURN result;
    end if;

    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

