CREATE OR REPLACE FUNCTION modify_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  value     NUMERIC,
  modifier  VARCHAR default 'delta',
  buffer jsonb default '{}'::jsonb
)
RETURNS NUMERIC AS $$
  
  DECLARE
    attr_record         RECORD; 
    obj_is_instance     BOOLEAN;
    avatar_is_connected BOOLEAN;
    old_value           NUMERIC;
    new_value           NUMERIC;
    hook                VARCHAR;
    this_buf            JSONB;
    result JSONB;
  
  BEGIN
    SELECT  in_table, type FROM everything
    WHERE   id = attribute
    INTO    attr_record;

    SELECT  connected FROM players
    WHERE   id = object_id
    INTO    avatar_is_connected;

    SELECT  is_instance(object_id)
    INTO    obj_is_instance;

    IF attr_record.type != 'attribute'
    OR attr_record.in_table IS NULL
    THEN
      RETURN NULL;
    ELSIF NOT obj_is_instance THEN
      RETURN NULL;
    ELSIF avatar_is_connected IS NOT NULL
    AND   avatar_is_connected IS NOT TRUE
    THEN
      RETURN NULL;
    END IF;

     /* Try to read the attribute old_value from the table, if exists */
    old_value := get_attribute_value(
      object_id,
      attribute
    );

    /* Try to initialize it from inherited classes if not found */
    IF old_value is NULL THEN
      old_value := initialize_attribute(
        object_id,
        attribute
      );
    END IF;

    /* If we are here and old_value is null, we give up */
    IF old_value IS NULL THEN
      RETURN NULL;
    END IF;

    IF    modifier = 'factor'
    THEN
      new_value := old_value * value;
    ELSIF modifier = 'set'
    OR    modifier = 'fix'
    THEN
      new_value := value;
    ELSE
      /* Default modifier is delta */
      new_value := old_value + value;
    END IF;

    /* Write the new attribute value in the database */
    --TODO: perhaps not the most efficient way to check this...
    IF old_value <> new_value THEN
      EXECUTE format(
        'UPDATE %I
         SET    %I = $1
         WHERE  id = %L',
        attr_record.in_table,
        attribute,
        object_id
      )
      USING new_value;

      /* Clean the garbage */
      DELETE FROM estimated_values;

      /* Run the relevant hooks */
      this_buf := buffer || jsonb_build_object('old_value', old_value, 'new_value', new_value);
      perform trigger_hook(object_id, 'hook_listen_attribute_' || attribute, this_buf);
    END IF;

    RETURN new_value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION qmodify_attribute(
  buffer jsonb,
  condition_id varchar
)
RETURNS jsonb AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    value numeric;
    modifier varchar;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := resolve_caller(buffer);
    attribute := resolve_condition_arg(this_condition.arg, buffer);
    
    if buffer ? 'modifier'
    then modifier := buffer->>'modifier';
    else modifier := 'delta';
    end if;

    value := modify_attribute(object_id, attribute, this_condition.value, modifier, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      'this_query'::text,   value::numeric,
      'retval'::text,       retval::boolean
    );

		RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmodify_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    write_field varchar;
    value numeric;
    modifier varchar;
    modifier_value numeric;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := query->>'this_query';
    attribute := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    
    if buffer ? 'modifier'
    then modifier := buffer->>'modifier';
    else modifier := 'delta';
    end if;

    if buffer ? 'modifier_value'
    then modifier_value := buffer->>'modifier_value';
    else modifier_value := this_condition.value;
    end if;

    value := modify_attribute(object_id, attribute, modifier_value, modifier, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      write_field,   value::numeric,
      'retval'::text,       retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

