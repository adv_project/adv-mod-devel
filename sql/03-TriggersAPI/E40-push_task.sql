CREATE OR REPLACE FUNCTION jpush_task(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    pick_field VARCHAR;
    required_task VARCHAR;
    caller VARCHAR;
    trigg VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    if this_condition.arg like '@%' then
      pick_field := regexp_replace(this_condition.arg, '@', '');
      if buffer ? pick_field
      then required_task := buffer->>pick_field;
      else required_task := pick_field;
      end if;
    else required_task := this_condition.arg;
    end if;
    
    if buffer ? 'caller'
    then
      caller := buffer->>'caller';
    elsif buffer ? 'recipient'
    then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id'
    then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;
    
    buffer := buffer || jsonb_build_object(
      'recipient', caller
    );

    for trigg in (
      select t.id
      from  triggers t,
            inherited i
      where i.id = caller
      and   t.target = i.inherits
      and   t.task = required_task
    )
    loop
      perform push_trigger(trigg, buffer::jsonb);
    end loop;

    result := jsonb_build_object(
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

