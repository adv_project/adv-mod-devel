CREATE OR REPLACE FUNCTION modify_boolean_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  value     BOOLEAN,
  modifier  VARCHAR   DEFAULT 'set',
  buffer jsonb  DEFAULT '{}'::jsonb
)
RETURNS BOOLEAN AS $$
  
  DECLARE
    attr_record         RECORD; 
    obj_is_instance     BOOLEAN;
    avatar_is_connected BOOLEAN;
    old_value           BOOLEAN;
    new_value           BOOLEAN;
    hook                VARCHAR;
    this_buf            JSONB;
    result JSONB;
  
  BEGIN
    SELECT  in_table, type FROM everything
    WHERE   id = attribute
    INTO    attr_record;

    SELECT  connected FROM players
    WHERE   id = object_id
    INTO    avatar_is_connected;

    SELECT  is_instance(object_id)
    INTO    obj_is_instance;

    IF attr_record.type != 'attribute'
    OR attr_record.in_table IS NULL
    THEN
      RETURN NULL;
    ELSIF NOT obj_is_instance THEN
      RETURN NULL;
    END IF;

     /* Try to read the attribute old_value from the table, if exists */
    old_value := get_boolean_attribute_value(
      object_id,
      attribute
    );

    /* Try to initialize it from inherited classes if not found */
    IF old_value is NULL THEN
      old_value := initialize_boolean_attribute(
        object_id,
        attribute
      );
    END IF;

    /* If we are here and old_value is null, we give up */
    IF old_value IS NULL THEN
      RETURN NULL;
    END IF;

    IF modifier = 'toggle'
    THEN
      new_value := NOT old_value;
    ELSE
      new_value := value;
    END IF;

    /* Write the new attribute value in the database */
    --TODO: perhaps not the most efficient way to check this...
    IF old_value <> new_value THEN
      EXECUTE format(
        'UPDATE %I
         SET    %I = $1
         WHERE  id = %L',
        attr_record.in_table,
        attribute,
        object_id
      )
      USING new_value;

      /* Clean the garbage */
      DELETE FROM estimated_values;

      /* Run the relevant hooks */
      this_buf := buffer || jsonb_build_object('old_value', old_value, 'new_value', new_value);
      perform trigger_hook(object_id, 'hook_listen_boolean_attribute_' || attribute, this_buf);
    END IF;

    RETURN new_value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION qmodify_boolean_attribute(
  buffer jsonb,
  condition_id varchar
)
RETURNS jsonb AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    value boolean;
    modifier varchar;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := resolve_caller(buffer);
    attribute := resolve_condition_arg(this_condition.arg, buffer);
    
    if buffer ? 'modifier'
    then modifier := buffer->>'modifier';
    else modifier := 'toggle';
    end if;

    select this_condition.value > 0 into value;

    value := modify_boolean_attribute(object_id, attribute, value, modifier, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      'this_query'::text,   value::boolean,
      'retval'::text,       retval::boolean
    );

		RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmodify_boolean_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    write_field varchar;
    value boolean;
    modifier varchar;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := query->>'this_query';
    attribute := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    
    if buffer ? 'modifier'
    then modifier := buffer->>'modifier';
    else modifier := 'set';
    end if;

    select this_condition.value > 0 into value;

    value := modify_boolean_attribute(object_id, attribute, value, modifier, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      write_field,   value::text,
      'retval'::text,       retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

