CREATE OR REPLACE FUNCTION process_boolean_modifier(
  modifier varchar,
  value boolean
)
RETURNS boolean AS $$
  BEGIN
    return value;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION process_modifier(
  modifier varchar,
  value numeric
)
RETURNS numeric AS $$
  DECLARE
    factor numeric;
    delta numeric;
    fix numeric;
    setv numeric;
    floor numeric;
    ceiling numeric;
    c record;
  BEGIN
    factor := 1;
    delta := 0;

    for c in select cnd.* from conditions cnd
      where cnd.target = modifier
      order by ord
    loop
      if c.type = 'factor'
      then factor = factor * c.value;
      elsif c.type = 'delta'
      then delta = delta + c.value;
      elsif c.type = 'fix'
      then fix = c.value;
      elsif c.type = 'set'
      then setv = c.value;
      elsif c.type = 'floor'
      then floor = c.value;
      elsif c.type = 'ceiling'
      then ceiling = c.value;
      end if;
    END LOOP;

    -- Apply all modifiers
    IF setv IS NOT NULL THEN
      value := setv;
    end if;
      
    value := (value + delta)*factor;

    IF floor IS NOT NULL
    and value < floor
    THEN
      value := floor;
    end if;
      
    IF ceiling IS NOT NULL
    and value > ceiling
    THEN
      value := ceiling;
    end if;
      
    IF fix IS NOT NULL THEN
      value := fix;
    end if;
      
      perform log_debug_event('process_modifier', value::text);
    return value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION estimate_attribute(
  object_id       VARCHAR,
  attribute_name  VARCHAR,
  skip_init       boolean   DEFAULT FALSE
)
RETURNS NUMERIC AS $$
  
  DECLARE
    value         NUMERIC;
    trigg         varchar;
  
  BEGIN
/* First, try estimated values */
    SELECT  e.value
    FROM    estimated_values e
    WHERE   e.id = object_id
    AND     e.attribute = attribute_name
    INTO    value;

    IF value IS NOT NULL
    THEN
/* If it was estimated earlier and we saved the value, use it */
      RETURN value;
    END IF;

/* Try to read the attribute value FROM the table, IF exists */
    value := get_attribute_value(
      object_id,
      attribute_name
    );

/* Try to initialize it FROM inherited classes IF not found */
    IF value IS NULL
    AND NOT skip_init
    THEN
      value := initialize_attribute(
        object_id,
        attribute_name
      );
    END IF;

/* If it's not defined IN the classes, maybe it is a "volatile" attribute
 (not defined in the mods but intended to be calculated on the fly).
 See IF there's a function that calculates it
*/
    IF value IS NULL
    AND is_routine_name('calculate_' || attribute_name)
    THEN
      EXECUTE format(
        'SELECT %I(%L);',
        'calculate_' || attribute_name,
        object_id
      )
      INTO value;
    END IF;

    IF value IS NOT NULL THEN
      -- Process modifiers
      perform log_debug_event('estimate_attribute', value::text);
      FOR trigg IN
        SELECT  t.modifier
        FROM    modifiers t, inherited i
        WHERE   i.id = object_id
        AND     t.target = i.inherits
        AND     t.attribute = attribute_name
      LOOP
        value := process_modifier(trigg, value);
      END LOOP;
    END IF;

    -- Save the value for later use
    IF value IS NOT NULL
    THEN
      value := round(value, 5);
      INSERT INTO estimated_values (
        id,
        attribute,
        value
      )
      VALUES (
        object_id,
        attribute_name,
        value
      );
    END IF;

    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION estimate_boolean_attribute(
  object_id       VARCHAR,
  attribute_name  VARCHAR,
  skip_init       boolean   DEFAULT FALSE
)
RETURNS BOOLEAN AS $$
  
  DECLARE
    value         BOOLEAN;
    trigg         varchar;
  
  BEGIN
/* Try to read the attribute value FROM the table, IF exists */
    value := get_boolean_attribute_value(
      object_id,
      attribute_name
    );

/* Try to initialize it FROM inherited classes IF not found */
    IF value IS NULL
    AND NOT skip_init
    THEN
      value := initialize_boolean_attribute(
        object_id,
        attribute_name
      );
    END IF;

/* If it's not defined IN the classes, maybe it is a "volatile" attribute
 (not defined in the mods but intended to be calculated on the fly).
 See IF there's a function that calculates it
*/
    IF value IS NULL
    AND is_routine_name('calculate_boolean_' || attribute_name)
    THEN
      EXECUTE format(
        'SELECT %I(%L);',
        'calculate_boolean_' || attribute_name,
        object_id
      )
      INTO value;
    END IF;

    IF value IS NOT NULL THEN
      -- Process modifiers
      FOR trigg IN
        SELECT  t.modifier
        FROM    modifiers t, inherited i
        WHERE   i.id = object_id
        AND     t.target = i.inherits
        AND     t.attribute = attribute_name
      LOOP
        value := process_boolean_modifier(trigg, value);
      END LOOP;
    END IF;

    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION do_estimate_attribute(
  object_id       VARCHAR,
  attribute_name  VARCHAR,
  buffer jsonb   DEFAULT '{}'::jsonb
)
RETURNS NUMERIC AS $$
  DECLARE
    value numeric;
    skip_init boolean;

  BEGIN
    skip_init := false;

    if buffer ? 'skip_init'
    and buffer->>'skip_init' not like '%false%'
    then
      skip_init := true;
    end if;

    value := estimate_attribute(object_id, attribute_name, skip_init);
    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION do_estimate_boolean_attribute(
  object_id       VARCHAR,
  attribute_name  VARCHAR,
  buffer jsonb   DEFAULT '{}'::jsonb
)
RETURNS BOOLEAN AS $$
  DECLARE
    value BOOLEAN;
    skip_init boolean;

  BEGIN
    skip_init := false;

    if buffer ? 'skip_init'
    and buffer->>'skip_init' not like '%false%'
    then
      skip_init := true;
    end if;

    value := estimate_boolean_attribute(object_id, attribute_name, skip_init);
    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION qestimate_attribute(
  buffer jsonb,
  condition_id varchar
)
RETURNS jsonb AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    value numeric;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := resolve_caller(buffer);
    attribute := resolve_condition_arg(this_condition.arg, buffer);
    value := do_estimate_attribute(object_id, attribute, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      'this_query'::text,   value::numeric,
      'retval'::text,       retval::boolean
    );

		RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jestimate_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    write_field varchar;
    value numeric;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    if 'estimate_on_query' = any(this_condition.tags)
    then
      object_id := query->>'this_query';
    else
      object_id := resolve_caller(buffer);
    end if;

    attribute := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    value := do_estimate_attribute(object_id, attribute, buffer);
    select value is not null into retval;

    -- Multiply attribute value times condition->value
    if value is not null
    and this_condition.value <> 0.0
    then value := value * this_condition.value;
    end if;

    -- Randomize attribute value by +/-condition->amount
    if  value is not null
    and this_condition.amount <> 1.0
    then value := value + ( random() - 0.5 ) * 2*this_condition.amount;
    end if;

    result := result || jsonb_build_object(
      write_field,   value::numeric,
      'retval'::text,       retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jestimate_attributes(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attr         VARCHAR;
    write_field varchar;
    value numeric;
    bool boolean;
    string varchar;
    retval boolean;
    attr_type varchar;
    arg varchar;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    if 'estimate_on_query' = any(this_condition.tags)
    then
      object_id := query->>'this_query';
    else
      object_id := resolve_caller(buffer);
    end if;

    arg := resolve_condition_arg(this_condition.arg, buffer);

    for attr in
      select * from regexp_split_to_table(arg, ',')
    loop
      select attribute_type from attribute_types
      where attribute_name = attr
      into attr_type;

      if attr_type like '%numeric%'
      then
        value := do_estimate_attribute(object_id, attr, buffer);
        result := result || jsonb_build_object(attr, value::numeric);
      elsif attr_type like '%boolean%'
      then
        bool := do_estimate_boolean_attribute(object_id, attr, buffer);
        result := result || jsonb_build_object(attr, bool::text);
      end if;
    end loop;

    for attr in
      select s.attribute_name from string_attributes s
      where s.id = object_id
    loop
      select s.value from string_attributes s
      where s.id = object_id
      and s.attribute_name = attr
      into string;

      result := result || jsonb_build_object(attr, string);
    end loop;

    result := result || jsonb_build_object(
      'retval'::text,       true::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jestimate_all_attributes(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attr         VARCHAR;
    write_field varchar;
    value numeric;
    bool boolean;
    string varchar;
    retval boolean;
    attr_type varchar;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    if 'estimate_on_query' = any(this_condition.tags)
    then
      object_id := query->>'this_query';
    else
      object_id := resolve_caller(buffer);
    end if;

    for attr in
      select attribute from object_attributes
      where id = object_id
    loop
      select attribute_type from attribute_types
      where attribute_name = attr
      into attr_type;

      if attr_type like '%numeric%'
      then
        value := do_estimate_attribute(object_id, attr, buffer);
        result := result || jsonb_build_object(attr, value::numeric);
      elsif attr_type like '%boolean%'
      then
        bool := do_estimate_boolean_attribute(object_id, attr, buffer);
        result := result || jsonb_build_object(attr, bool::boolean);
      end if;
    end loop;

    for attr in
      select s.attribute_name from string_attributes s
      where s.id = object_id
    loop
      select s.value from string_attributes s
      where s.id = object_id
      and s.attribute_name = attr
      into string;

      result := result || jsonb_build_object(attr, string);
    end loop;

    result := result || jsonb_build_object(
      'retval'::text,       true::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

