CREATE OR REPLACE FUNCTION jprepare_location(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    result JSONB;
    slot record;
    plug record;
    proposed_child varchar;
    proposed_parent varchar;
    free_space numeric;
    errors varchar[];
    success boolean;
    log_class varchar;
  BEGIN
    log_class := 'jprepare_location';
    result := '{}'::jsonb;
    success := false;

    perform log_test_event(log_class,
      concat(':: Preparing location'));
    
    if buffer ? 'proposed_child' then
      proposed_child := buffer->>'proposed_child';
    elsif buffer ? 'player_id' then
      proposed_child := buffer->>'player_id';
    elsif buffer ? 'created_id' then
      proposed_child := buffer->>'created_id';
    else
      proposed_child := 'nobody';
    end if;

    perform log_test_event(log_class,
      concat('-> Proposed child: "', proposed_child, '"'));
    
    if buffer ? 'proposed_parent' and buffer->>'proposed_parent' not like 'KEEP_PARENT' then
      proposed_parent := buffer->>'proposed_parent';
    else
      select target from links where id = proposed_child and type = 'parent' into proposed_parent;
    end if;

    perform log_test_event(log_class,
      concat('-> Proposed parent: "', proposed_parent, '"'));
    
    create temp table proposed_slots on commit drop as
      select * from slots_by_object
      where id = proposed_parent
      order by ord;
    
    create temp table proposed_plugs on commit drop as
      select * from plugs_by_object
      where id = proposed_child
      order by ord;

    if buffer ? 'proposed_slot_type' then
      delete from proposed_slots where slot_type != buffer->>'proposed_slot_type';
      delete from proposed_plugs where slot_type != buffer->>'proposed_slot_type';
    end if;
    
    if buffer ? 'proposed_slot_name' then
      delete from proposed_slots where name != buffer->>'proposed_slot_name';
    end if;
    
    if buffer ? 'proposed_plug_name' then
      delete from proposed_plugs where name != buffer->>'proposed_plug_name';
    end if;
    
    perform log_test_event(log_class,
      concat('-> Proposed slots: [', array_to_string(array(select name from proposed_slots), ' '), ']'));
    perform log_test_event(log_class,
      concat('-> Proposed plugs: [', array_to_string(array(select name from proposed_plugs), ' '), ']'));

    for slot in (
      select * from proposed_slots s
      where exists (
        select p.id from proposed_plugs p
        where p.slot_type = s.slot_type
      )
      order by ord asc
    )
    loop
      perform log_test_event(log_class,
        concat(' trying slot "', slot.name, '"'));
      for plug in (
        select * from proposed_plugs p
        where p.slot_type = slot.slot_type
        order by ord asc
      )
      loop
        perform log_test_event(log_class,
          concat('  trying plug "', plug.name, '"'));

        free_space := get_free_space_in_object_slot(
          slot.id,
          slot.name
        );

        perform log_test_event(log_class,
          concat('   free space: ', free_space::text, ', plug value: ', plug.value::text));

        if free_space >= plug.value
        or free_space = -1.0
        then
          result := result || jsonb_build_object(
            'child_id', plug.id,
            'parent_id', slot.id,
            'slot_type', slot.slot_type,
            'slot_name', slot.name,
            'slot_ord', slot.ord,
            'plug_ord', plug.ord,
            'plug_name', plug.name
          );
          success := true;
          perform log_test_event(log_class,
            concat('   * SUCCESS *'));
          exit;
        else
          errors := errors || array[concat(slot.name, '_is_full')]::varchar[];
          perform log_test_event(log_class,
            concat('   * failed *'));
        end if;
      end loop;

      if success is true
      then exit;
      end if;

    end loop;
    
    if success is not true
    and errors is not null
    then
      result := result || jsonb_build_object(
        'errors', errors
      );
    elsif success is not true
    then
      result := result || jsonb_build_object(
        'errors', array['prepare_location_failed']::varchar[]
      );
    end if;

    if not buffer ? 'opposite_direction' then
      result := result || jsonb_build_object(
        'opposite_direction', 'out_of_nowhere'
      );
    end if;
    
    perform log_test_event(log_class,
      concat(':: Result: ', success::text));

    result := result || jsonb_build_object(
      'retval', success::boolean
    );
    
    drop table if exists proposed_slots;
    drop table if exists proposed_plugs;

    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jprepare_location_fallback(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    prepare_result jsonb;
    tmp_buf jsonb;
    this_parent varchar;
    result jsonb;
    i integer;
    do_abort boolean;

  BEGIN
    result := '{}'::jsonb;
    do_abort := false;

    -- First, attempt to locate with the buffer as-is
    tmp_buf := buffer;
    prepare_result := jprepare_location(tmp_buf, query, condition_id);
    
    -- Remove slot/plug restrictions
    -- TODO: split in steps, in some order
    if prepare_result->>'retval' not like '%true%'
    then
      tmp_buf := tmp_buf - 'proposed_slot_type' - 'proposed_slot_name' - 'proposed_plug_name';
      prepare_result := jprepare_location(tmp_buf, query, condition_id);
    end if;

    -- Loop over parents until we reach the biome
    if prepare_result->>'retval' not like '%true%'
    then
      i := 0;
      loop
        select target from links
        where type = 'parent'
        and id = buffer->>'proposed_parent'
        into this_parent;

        if exists (
          select target from links
          where id = this_parent
          and type = 'parent'
          and name = 'world'
        )
        then exit;
        end if;

        tmp_buf := tmp_buf || jsonb_build_object('proposed_parent', this_parent);
        prepare_result := jprepare_location(tmp_buf, query, condition_id);

        if prepare_result->>'retval' like '%true%'
        then exit;
        end if;

        i := i + 1;
        if i > 50
        then
          do_abort := true;
          exit;
        end if;
      end loop;
    end if;

    if do_abort is true
    then
      result := result || jsonb_build_object('retval', false);
      return result;
    end if;
    
    -- Fall over altitude gradient
    if prepare_result->>'retval' not like '%true%'
    then
      i := 0;
      loop
        select neighbour from neighborhood
        where origin = buffer->>'proposed_parent'
        order by altitude_gradient limit 1
        into this_parent;

        tmp_buf := tmp_buf || jsonb_build_object('proposed_parent', this_parent);
        prepare_result := jprepare_location(tmp_buf, query, condition_id);

        if prepare_result->>'retval' like '%true%'
        then exit;
        end if;

        i := i + 1;
        if i > 20
        then
          do_abort := true;
          exit;
        end if;
      end loop;
    end if;

    if do_abort is true
    then
      result := result || jsonb_build_object('retval', false);
      return result;
    end if;
    
    if prepare_result->>'retval' like '%true%'
    then
      result := prepare_result;
    else
      result := result || jsonb_build_object('retval', false);
    end if;
    
    return result;
	END;
$$ LANGUAGE plpgsql;

