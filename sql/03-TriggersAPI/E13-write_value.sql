CREATE OR REPLACE FUNCTION jwrite_value(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    write_field VARCHAR;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    write_field := resolve_condition_write(this_condition.write);

    result := jsonb_build_object(
      write_field, this_condition.value,
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

