CREATE OR REPLACE FUNCTION trigger_hook(
  caller_id varchar,
  hook_task varchar,
  buffer jsonb default '{}'::jsonb
)
RETURNS void AS $$
  DECLARE
    hook VARCHAR;
  BEGIN
    buffer := buffer || jsonb_build_object('caller', caller_id);
    for hook in (
      select t.id from triggers t, inherited i where
        i.id = caller_id and
        t.target = i.inherits and
        t.task = hook_task
    )
    loop
      perform push_trigger(hook, buffer::jsonb);
    end loop;
    return;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_condition_arg(
  arg varchar,
  buffer jsonb default '{}'::jsonb
)
RETURNS varchar AS $$
  DECLARE
    string VARCHAR;
    pick_field VARCHAR;
  BEGIN
    if arg like '@%' then
      pick_field := regexp_replace(arg, '@', '');
      if buffer ? pick_field
      then string := buffer->>pick_field;
      else string := concat('unknown_', pick_field);
      end if;
    else string := arg;
    end if;
    return string;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_condition_write(
  write varchar,
  default_value varchar default 'result'
)
RETURNS varchar AS $$
  DECLARE
    write_field VARCHAR;
  BEGIN
    if is_valid(write)
    then write_field := write;
    else write_field := default_value;
    end if;
    return write_field;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_caller(
  buffer jsonb,
  default_value varchar default ''::varchar
)
RETURNS varchar AS $$
  DECLARE
    caller VARCHAR;
  BEGIN
    if buffer ? 'caller' then
      caller := buffer->>'caller';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    elsif buffer ? 'recipient' then
      caller := buffer->>'recipient';
    else
      caller := default_value;
    end if;
    return caller;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_recipient(
  buffer jsonb,
  default_value varchar default ''::varchar
)
RETURNS varchar AS $$
  DECLARE
    recipient VARCHAR;
  BEGIN
    if buffer ? 'recipient' then
      recipient := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      recipient := buffer->>'player_id';
    elsif buffer ? 'caller' then
      recipient := buffer->>'caller';
    else
      recipient := default_value;
    end if;
    return recipient;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_free_space_in_object_slot(
  object_id  VARCHAR,
  proposed_slot_name  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    free_space  NUMERIC;
    child_size  NUMERIC;
  BEGIN
    select value from slots_by_object where id = object_id and name = proposed_slot_name into free_space;

    if free_space > 0.0::NUMERIC
    then
      for child_size in (
        select slug.plug_value from slugboard slug
        where slug.target = object_id
        and slug.slot_name = proposed_slot_name
      )
      loop
        free_space := free_space - child_size;
      end loop;
    end if;
        
    return free_space;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION try_query(
  buffer jsonb,
  condition_id varchar
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    result jsonb;
  BEGIN
      select * from conditions c
      where c.id = condition_id
      into this_condition;
    EXECUTE format(
      'SELECT %s FROM %s(
       %s::jsonb,
       %s
      );',
      this_condition.query_name,
      this_condition.query_name,
      quote_literal(buffer),
      quote_literal(this_condition.id)
    )
    into result;
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION try_test(
  buffer jsonb,
  this_query  jsonb,
  condition_id varchar
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    result jsonb;
  BEGIN
    select * from conditions c
    where c.id = condition_id
    into this_condition;
    EXECUTE format(
      'SELECT %s FROM %s(
       %s::jsonb,
       %s::jsonb,
       %s
      );',
      this_condition.test_name,
      this_condition.test_name,
      quote_literal(buffer),
      quote_literal(this_query),
      quote_literal(this_condition.id)
    )
    into result;
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

