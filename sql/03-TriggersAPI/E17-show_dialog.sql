CREATE OR REPLACE FUNCTION jshow_dialog(
  buffer JSONB,
  query         JSONB,
  condition_id  VARCHAR
) RETURNS JSONB AS $$
  DECLARE
    this_condition  RECORD;
    response_class  VARCHAR;
    result          JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    response_class := resolve_condition_arg(this_condition.arg, buffer);
    result := jsonb_build_object(
      'response_type'::text, 'dialog',
      'response_class'::text, response_class::text
    );
    perform push_response(buffer || result);
    return result;
  END;
$$ LANGUAGE plpgsql;

