CREATE OR REPLACE FUNCTION jmath_sum(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    field varchar;
    i numeric;
    sum numeric;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'sum_result');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    sum := 0.0;

    
    for field in select * from regexp_split_to_table(arg, ',')
    loop
      if buffer ? field
      then
        PERFORM log_debug_event('jmath_sum', concat('add to sum: ', field, ' = ', buffer->>field));
        sum := sum + (buffer->field)::numeric;
      else
        PERFORM log_debug_event('jmath_sum'::TEXT, 'warning: field not found: ' || field);
      end if;
    end loop;

    if 'round_result' = any(this_condition.tags)
    then sum := round(sum);
    elsif 'truncate_result' = any(this_condition.tags)
    then sum := trunc(sum);
    end if;

    result := result || jsonb_build_object(
      write_field, sum::numeric,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmath_times(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    field varchar;
    num numeric;
    den numeric;
    value numeric;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'sum_result');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    value := 1.0;
    
    for field in select * from regexp_split_to_table(arg, ',')
    loop
      if buffer ? field
      then
        PERFORM log_debug_event('jmath_times', concat('multiplying ', field, ' = ', buffer->>field));
        value := value * (buffer->field)::numeric;
      else
        PERFORM log_debug_event('jmath_times'::TEXT, 'warning: field not found: ' || field);
        result := result || jsonb_build_object(
          write_field, null,
          'retval', false
        );
        return result;
      end if;
    end loop;

    -- Multiply value times condition->value
    if value is not null
    and this_condition.value <> 0.0
    and this_condition.value is not null
    then value := value * this_condition.value;
    end if;

    -- Randomize value by +/-condition->amount
    if  value is not null
    and this_condition.amount <> 1.0
    and this_condition.amount is not null
    then value := value + ( random() - 0.5 ) * 2*this_condition.amount;
    end if;

    value := round(value, 5);
    if 'round_result' = any(this_condition.tags)
    then value := round(value);
    elsif 'truncate_result' = any(this_condition.tags)
    then value := trunc(value);
    end if;

    result := result || jsonb_build_object(
      write_field, value,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmath_division(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    field varchar;
    num numeric;
    den numeric;
    div numeric;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'sum_result');
    arg := resolve_condition_arg(this_condition.arg, buffer);

    
    for field in select * from regexp_split_to_table(arg, ',')
    loop
      if buffer ? field
      then
        if num is null then
          PERFORM log_debug_event('jmath_division', concat('dividing ', field, ' = ', buffer->>field));
          num := (buffer->field)::numeric;
        else
          PERFORM log_debug_event('jmath_division', concat('by ', field, ' = ', buffer->>field));
          den := (buffer->field)::numeric;
          exit;
        end if;
      else
        PERFORM log_debug_event('jmath_division'::TEXT, 'warning: field not found: ' || field);
        result := result || jsonb_build_object(
          write_field, null,
          'retval', false
        );
        return result;
      end if;
    end loop;

    if den <> 0
    then
      div := num / den;
    else
      PERFORM log_debug_event('jmath_division'::TEXT, 'warning: division by zero');
      result := result || jsonb_build_object(
        write_field, null,
        'retval', true
      );
    end if;

    -- Multiply value times condition->value
    if div is not null
    and this_condition.value <> 0.0
    and this_condition.value is not null
    then div := div * this_condition.value;
    end if;

    -- Randomize value by +/-condition->amount
    if  div is not null
    and this_condition.amount <> 1.0
    and this_condition.amount is not null
    then div := div + ( random() - 0.5 ) * 2*this_condition.amount;
    end if;

    div := round(div, 5);
    if 'round_result' = any(this_condition.tags)
    then div := round(div);
    elsif 'truncate_result' = any(this_condition.tags)
    then div := trunc(div);
    end if;

    result := result || jsonb_build_object(
      write_field, div,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION jmath_sqrt(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    square_root numeric;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'sum_result');
    arg := resolve_condition_arg(this_condition.arg, buffer);

    
    if buffer ? arg
    then
      PERFORM log_debug_event('jmath_sqrt', concat('calculating square root: ', arg, ' = ', buffer->>arg));
      square_root := sqrt( (buffer->arg)::numeric );
    else
      PERFORM log_debug_event('jmath_division'::TEXT, 'warning: field not found: ' || arg);
      result := result || jsonb_build_object(
        write_field, null,
        'retval', false
      );
      return result;
    end if;

    square_root := round(square_root, 5);
    if 'round_result' = any(this_condition.tags)
    then square_root := round(square_root);
    elsif 'truncate_result' = any(this_condition.tags)
    then square_root := trunc(square_root);
    end if;

    result := result || jsonb_build_object(
      write_field, square_root,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

