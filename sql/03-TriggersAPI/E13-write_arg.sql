CREATE OR REPLACE FUNCTION jresolve_boundaries(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    attribute_name VARCHAR;
    old_value NUMERIC;
    new_value NUMERIC;
    string VARCHAR;
    write_field VARCHAR;
    this_condition record;
    boundaries_resolved record;
    boundaries_array varchar[];
    last_element integer;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    attribute_name := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    old_value := buffer->'old_value';
    new_value := buffer->'new_value';

    select * from resolve_boundaries(attribute_name, old_value, new_value) limit 1 into boundaries_resolved;

    if boundaries_resolved.direction is not null
    then
      select boundaries_resolved.boundaries into boundaries_array;
      select array_length(boundaries_array, 1) into last_element;
      select concat(
        attribute_name
        || '_'::varchar
        || boundaries_resolved.direction
        || '_'::varchar
        || boundaries_array[last_element]
      ) into string;
    
      result := jsonb_build_object(
        write_field, string,
        'retval', true
      );
    else
      result := jsonb_build_object(
        write_field, null,
        'retval', false
      );
    end if;

    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jwrite_arg(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    string VARCHAR;
    pick_field VARCHAR;
    write_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    string := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    
    result := jsonb_build_object(
      write_field, string,
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jwrite_arg_as_number(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    value numeric;
    pick_field VARCHAR;
    write_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    value := (resolve_condition_arg(this_condition.arg, buffer))::numeric;
    write_field := resolve_condition_write(this_condition.write);
    
    result := jsonb_build_object(
      write_field, value::numeric,
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

