CREATE OR REPLACE FUNCTION jshow_inspection(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    d varchar;
    this_condition record;

    this_plug_name varchar;
    this_slot_name varchar;
    this_parent varchar;
    these_elements varchar[];
    these_colors varchar[];
    this_shape varchar;
    this_diaphaneity varchar;
    these_contained varchar[];
    this_token varchar;
    description_label varchar;
    description_class varchar;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient' then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;

    foreach d in array array(select * from jsonb_array_elements_text(buffer->'candidates'))
    loop
      if not buffer ? 'describe_as_parent'
      then
        /* Inspect an object */
        select resolve_plug_name(d) into this_plug_name;      --<-- string
        select resolve_slot_name(d) into this_slot_name;      --<-- string
        select resolve_parent(d) into this_parent;            --<-- string
        select resolve_components(d) into these_elements;       --<-- array
        select resolve_colors(d) into these_colors;           --<-- array
        select resolve_shape(d) into this_shape;              --<-- string
        select resolve_diaphaneity(d) into this_diaphaneity;  --<-- string
        select resolve_contained(d) into these_contained;     --<-- array

        if these_elements = array[]::varchar[]
        then description_label = 'detailed_description_noelements';
        else description_label = 'detailed_description';
        end if;

        if 'color_colorless' = all(these_colors)
        and this_shape = 'shape_undefined'
        then description_class := description_label || '_nocolor_noshape';
        elsif 'color_colorless' = all(these_colors)
        then description_class := description_label || '_nocolor';
        elsif this_shape = 'shape_undefined'
        then description_class := description_label || '_noshape';
        else description_class := description_label;
        end if;

        this_buf := buffer || jsonb_build_object(
          'target', d,
          'slot_name', this_slot_name,
          'plug_name', this_plug_name,
          'parent', this_parent,
          'response_class', 'title',
          'response_type', 'message'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);

        this_buf := buffer || jsonb_build_object(
          'target', d,
          'elements', these_elements::varchar[],
          'colors', these_colors::varchar[],
          'shape', this_shape,
          'diaphaneity', this_diaphaneity,
          'response_class', description_class,
          'response_type', 'message'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);

        if these_contained <> array[]::varchar[]
        then
          this_buf := buffer || jsonb_build_object(
            'target', d,
            'contained', these_contained::varchar[],
            'response_class', 'it_contains',
            'response_type', 'message'
          );
          perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
        end if;
      else
        /* Inspect a place */
        select array[]::varchar[] into these_contained;

        select token from identify_objects(buffer, 'candidates', false, true) limit 1 into this_token;

        this_buf := buffer || jsonb_build_object(
          'target', d,
          'response_class', 'place_title',
          'response_type', 'message'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);

        this_buf := buffer || jsonb_build_object(
          'target', d,
          'response_class', this_token || '_detailed_description',
          'response_type', 'dialog'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
      end if;
    end loop;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_plug_name(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    result VARCHAR;
  BEGIN
    select plug_name from slugboard where id = object_id into result;
    if result is null then result := 'no_plug'::varchar; end if;
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_slot_name(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    result VARCHAR;
  BEGIN
    select slot_name from slugboard where id = object_id into result;
    if result is null then result := 'no_slot'::varchar; end if;
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_parent(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    result VARCHAR;
  BEGIN
    select target from slugboard where id = object_id into result;
    if result is null then result := 'no_parent'::varchar; end if;
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_components(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    result VARCHAR[];
    elem varchar;
    elems varchar[];
  BEGIN
    result := array[]::varchar[];
    elems := array[]::varchar[];

    if exists (select id from  tagged where id = object_id and tag = 'skip_components_on_description')
    then return result;
    end if;

    for elem in select * from get_components_recursive(object_id)
    loop
      if exists (
        select e.id from everything e, inherited i
        where i.id = elem
        and e.id = i.inherits
        and e.type = 'resource_class'
      )
      then
        elems := elems || elem;
      end if;
    end loop;

    if elems = array[]::varchar[]
    then elems := array['unknown_resource_class']::varchar[];
    end if;

    for elem in select distinct unnest(elems)
    loop
      result := result || elem;
    end loop;

    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_colors(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    elements VARCHAR[];
    result VARCHAR[];
    col_code numeric;
    col_codes numeric[];
    col_names varchar[];
    elems varchar[];
    elem varchar;
  BEGIN
    select estimate_attribute(object_id, 'color') into col_code;
    col_codes := array[]::numeric[];
    col_names := array[]::varchar[];

    if col_code is not null
    then
      col_codes := array[col_code]::numeric[];
    else
      elems := resolve_components(object_id);
      
      foreach elem in array elems
      loop
        select estimate_attribute(elem, 'color') into col_code;
        
        if col_code is not null
        then col_codes := col_codes || col_code;
        end if;
      end loop;
    end if;

    if col_codes = array[]::numeric[]
    then col_codes := array[-1.0]::numeric[];
    end if;

    foreach col_code in array col_codes
    loop
      col_names := col_names || resolve_color_code(col_code);
    end loop;

    return col_names;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_shape(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    shape_code numeric;
    result VARCHAR;
  BEGIN
    select estimate_attribute(object_id, 'shape') into shape_code;
    if shape_code is null then select -1.0 into shape_code; end if;
    return resolve_shape_code(shape_code);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_diaphaneity(
  object_id VARCHAR
)
RETURNS VARCHAR AS $$
  DECLARE
    diaphaneity_code numeric;
    result VARCHAR;
    elems varchar[];
    elem varchar;
    this_code numeric;
  BEGIN
    select estimate_attribute(object_id, 'diaphaneity') into diaphaneity_code;
    if diaphaneity_code is null
    then
      select resolve_components(object_id) into elems;
      foreach elem in array elems
      loop
        select estimate_attribute(elem, 'diaphaneity') into this_code;
      end loop;
      if diaphaneity_code is null or this_code > diaphaneity_code
      then select this_code into diaphaneity_code;
      end if;
    end if;
    if diaphaneity_code is null then select -1.0 into diaphaneity_code; end if;
    return resolve_diaphaneity_code(diaphaneity_code);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_contained(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    result VARCHAR[];
    c varchar;
  BEGIN
    result := array[]::varchar[];
    
    for c in select id from slugboard where target = object_id
    loop
      result := result || c;
    end loop;

    return result;
  END;
$$ LANGUAGE plpgsql;

