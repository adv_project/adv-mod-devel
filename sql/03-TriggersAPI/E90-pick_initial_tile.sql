CREATE OR REPLACE FUNCTION jpick_initial_tile(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    initial_tile VARCHAR;
    buffer_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    select id from links
    where type = 'init'
    and name = 'initial_tile'
    order by random() limit 1
    into initial_tile;
    
    if is_valid(this_condition.write)
    then buffer_field := this_condition.write;
    else buffer_field := 'initial_tile';
    end if;

    result := jsonb_build_object(
      buffer_field, initial_tile
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jpick_world_template(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    world_template VARCHAR;
    buffer_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    select id from links
    where type = 'attribute'
    and name = 'world_class'
    and target = 'template'
    limit 1
    into world_template;
    
    if is_valid(this_condition.write)
    then buffer_field := this_condition.write;
    else buffer_field := 'world_template';
    end if;

    result := jsonb_build_object(
      buffer_field, world_template
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jpick_game_phase(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    player_id varchar;
    game_phase VARCHAR;
    buffer_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    player_id = buffer->>'player_id';

    select target from links
    where type = 'attribute'
    and name = 'game_phase'
    and id = player_id
    limit 1
    into game_phase;
    
    if is_valid(this_condition.write)
    then buffer_field := this_condition.write;
    else buffer_field := 'game_phase';
    end if;

    result := jsonb_build_object(
      buffer_field, game_phase
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

