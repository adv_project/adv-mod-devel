CREATE OR REPLACE FUNCTION jmath_gt(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'eval');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    
    if buffer ? arg
    then
      PERFORM log_debug_event('jmath_gt', concat('is ', arg, ' (', (buffer->arg)::text, ') greater than ', (this_condition.value)::text));
      select (buffer->arg)::numeric > this_condition.value into retval;
    else
      PERFORM log_debug_event('jmath_gt'::TEXT, 'warning: field not found: ' || arg);
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, retval::text,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmath_ge(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'eval');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    
    if buffer ? arg
    then
      PERFORM log_debug_event('jmath_ge', concat('is ', arg, ' (', (buffer->arg)::text, ') greater than or equal to ', (this_condition.value)::text));
      select (buffer->arg)::numeric >= this_condition.value into retval;
    else
      PERFORM log_debug_event('jmath_ge'::TEXT, 'warning: field not found: ' || arg);
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, retval::text,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmath_lt(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'eval');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    
    if buffer ? arg
    then
      PERFORM log_debug_event('jmath_lt', concat('is ', arg, ' (', (buffer->arg)::text, ') less than ', (this_condition.value)::text));
      select (buffer->arg)::numeric < this_condition.value into retval;
    else
      PERFORM log_debug_event('jmath_lt'::TEXT, 'warning: field not found: ' || arg);
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, retval::text,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmath_le(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    arg varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'eval');
    arg := resolve_condition_arg(this_condition.arg, buffer);
    
    PERFORM log_debug_event('jmath_le', concat('is ', arg, ' (', (buffer->arg)::text, ') less than or equal to ', (this_condition.value)::text));

    if buffer ? arg
    then
      select (buffer->arg)::numeric <= this_condition.value into retval;
    else
      PERFORM log_debug_event('jmath_le'::TEXT, 'warning: field not found: ' || arg);
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, retval::text,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

