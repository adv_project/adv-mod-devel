CREATE OR REPLACE FUNCTION get_attribute_value(
  id        VARCHAR,
  attribute VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name VARCHAR;
    value      NUMERIC;
  
  BEGIN
     SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;

    IF table_name IS NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'SELECT %I
         FROM %I
         WHERE id = %L;',
        attribute,
        table_name,
        id
      )
      INTO value;
       
      RETURN value::NUMERIC;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_boolean_attribute_value(
  id        VARCHAR,
  attribute VARCHAR
)
RETURNS BOOLEAN AS $$
  
  DECLARE
    table_name VARCHAR;
    value      BOOLEAN;
  
  BEGIN
    SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;

    IF table_name IS NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'SELECT %I
         FROM %I
         WHERE id = %L;',
        attribute,
        table_name,
        id
      )
      INTO value;
       
      RETURN value::BOOLEAN;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_attribute_value(
  id        VARCHAR,
  attribute VARCHAR,
  value     NUMERIC
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name VARCHAR;
  
  BEGIN
     SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;
    
    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'update %I
         set    %I = $1
         WHERE  id = %L;',
        table_name,
        attribute,
        id
      )
      USING value;
       
      RETURN value;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insert_boolean_attribute_value(
  id        VARCHAR,
  attribute VARCHAR,
  value     BOOLEAN
)
RETURNS BOOLEAN AS $$
  
  DECLARE
    table_name VARCHAR;
  
  BEGIN
     SELECT x.in_table
    FROM   everything x
    WHERE  x.id = attribute
    INTO   table_name;
    
    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      EXECUTE format(
        'update %I
         set    %I = $1
         WHERE  id = %L;',
        table_name,
        attribute,
        id
      )
      USING value;
       
      RETURN value;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION initialize_attribute(
  object_id VARCHAR,
  attribute VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    table_name        VARCHAR;
    inherited         VARCHAR;
    value             NUMERIC;
    already_in_table  VARCHAR;
  
  BEGIN
    SELECT in_table
    FROM   everything
    WHERE  id = attribute
    INTO   table_name;

    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      FOREACH inherited IN ARRAY array(
        SELECT i.id
        FROM   get_all_inherited(object_id) i
      )
      LOOP
        EXECUTE format(
          'SELECT %I
           FROM   %I
           WHERE  id = %L;',
          attribute,
          table_name,
          inherited
        )
        INTO value;

        IF value IS NOT NULL THEN
           EXECUTE format(
            'SELECT id
             FROM   %I
             WHERE  id = %L;',
            table_name,
            object_id
          )
          INTO already_in_table;

          IF already_in_table IS NOT NULL THEN
             EXECUTE format(
              'UPDATE %I
               SET    %I = $1
               WHERE  id = %L;',
              table_name,
              attribute,
              object_id
            )
            USING value;
          ELSE
             EXECUTE format(
              'INSERT INTO %I (
                 id,
                 %I
               )
               VALUES (
                 %L,
                 $1
               );',
              table_name,
              attribute,
              object_id
            )
            USING value;
          END IF;
          EXIT;
        END IF;
      END LOOP;
    END IF;

     RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION initialize_boolean_attribute(
  object_id VARCHAR,
  attribute VARCHAR
)
RETURNS BOOLEAN AS $$
  
  DECLARE
    table_name        VARCHAR;
    inherited         VARCHAR;
    value             BOOLEAN;
    already_in_table  VARCHAR;
  
  BEGIN
    SELECT in_table
    FROM   everything
    WHERE  id = attribute
    INTO   table_name;

    IF table_name is NULL THEN
      RETURN NULL;
    ELSE
      FOREACH inherited IN ARRAY array(
        SELECT i.id
        FROM   get_all_inherited(object_id) i
      )
      LOOP
        EXECUTE format(
          'SELECT %I
           FROM   %I
           WHERE  id = %L;',
          attribute,
          table_name,
          inherited
        )
        INTO value;

        IF value IS NOT NULL THEN
           EXECUTE format(
            'SELECT id
             FROM   %I
             WHERE  id = %L;',
            table_name,
            object_id
          )
          INTO already_in_table;

          IF already_in_table IS NOT NULL THEN
             EXECUTE format(
              'UPDATE %I
               SET    %I = $1
               WHERE  id = %L;',
              table_name,
              attribute,
              object_id
            )
            USING value;
          ELSE
             EXECUTE format(
              'INSERT INTO %I (
                 id,
                 %I
               )
               VALUES (
                 %L,
                 $1
               );',
              table_name,
              attribute,
              object_id
            )
            USING value;
          END IF;
          EXIT;
        END IF;
      END LOOP;
    END IF;

     RETURN value;
  END;
$$ LANGUAGE plpgsql;

