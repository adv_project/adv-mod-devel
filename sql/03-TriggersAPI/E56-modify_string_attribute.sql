CREATE OR REPLACE FUNCTION modify_string_attribute(
  object_id VARCHAR,
  attribute VARCHAR,
  value     VARCHAR,
  buffer jsonb  DEFAULT '{}'::jsonb
)
RETURNS VARCHAR AS $$
  
  DECLARE
    old_value           VARCHAR;
    hook                VARCHAR;
    attribute_type      VARCHAR;
    this_buf            JSONB;
    result JSONB;
  
  BEGIN
    result := '{}'::jsonb;
    
    if buffer ? 'attribute_type'
    then attribute_type := buffer->>'attribute_type';
    else attribute_type := 'attribute';
    end if;

    select target from links
    where type = attribute_type
    and name = attribute
    and id = object_id
    into old_value;

    /* Try to initialize it from inherited classes if not found */
    IF old_value = value
    then
      return old_value;
    ELSIF old_value IS NULL THEN
      insert into links (id, type, name, target)
        values (object_id, attribute_type, attribute, value);
    ELSE
      update links set target = value
      where id = object_id
      and type = attribute_type
      and name = attribute;
    END IF;

    /* Clean the garbage */
    DELETE FROM estimated_values;

    /* Run the relevant hooks */
    this_buf := buffer || jsonb_build_object('old_value', old_value, 'new_value', value);
    perform trigger_hook(object_id, 'hook_listen_string_attribute_' || attribute, this_buf);

    RETURN value;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION qmodify_string_attribute(
  buffer jsonb,
  condition_id varchar
)
RETURNS jsonb AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    value varchar;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := resolve_caller(buffer);
    attribute := resolve_condition_write(this_condition.write);
    value := resolve_condition_arg(this_condition.arg, buffer);
    
    value := modify_string_attribute(object_id, attribute, value, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      'this_query'::text,   value,
      'retval'::text,       retval::boolean
    );

		RETURN result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmodify_string_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		object_id   VARCHAR;
		attribute         VARCHAR;
    write_field varchar;
    value varchar;
    retval boolean;
    result            JSONB;
    this_condition record;
	
  BEGIN
    result := '{}'::jsonb;
    
    select * from conditions where id = condition_id
    into this_condition;

    object_id := query->>'this_query';
    attribute := resolve_condition_write(this_condition.write);
    value := resolve_condition_arg(this_condition.arg, buffer);
    
    value := modify_string_attribute(object_id, attribute, value, buffer);
    select value is not null into retval;

    result := result || jsonb_build_object(
      'retval'::text,       retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

