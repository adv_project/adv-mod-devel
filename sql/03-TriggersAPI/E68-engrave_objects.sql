CREATE OR REPLACE FUNCTION append_text_to_object(
  object_id varchar,
  text_line text
)
RETURNS VOID AS $$
  DECLARE
    current_line integer;
    next_line integer;
  BEGIN
    select line_number from engraved_text
    where id = object_id
    order by line_number desc
    limit 1
    into current_line;

    if current_line is null
    then next_line := 1;
    else next_line := current_line + 1;
    end if;

    insert into engraved_text
      (id, line_number, text_line)
    values
      (object_id, next_line, text_line);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION do_append_text_to_object(
  object_id varchar,
  text_line text,
  buffer jsonb default '{}'::jsonb
)
RETURNS VOID AS $$
  BEGIN
    perform append_text_to_object(object_id, text_line);
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jappend_text_to_object(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition  record;
		object_id       VARCHAR;
    text_line       text;
    result          JSONB;
	
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    text_line := resolve_condition_arg(this_condition.arg::text, buffer);
    object_id := query->>'this_query';
    perform do_append_text_to_object(object_id, text_line);
    
    result := jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jdisplay_text_in_object(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    object_id varchar;
    d record;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient' then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;

    for object_id in select * from jsonb_array_elements_text(buffer->'reading')
    loop
        for d in
          select line_number, text_line from engraved_text
          where id = object_id
          order by line_number asc
        loop
          this_buf := buffer || jsonb_build_object(
            'response_class', d.text_line,
            'response_type',  'dialog'
          );
          perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, 'dummy_condition');
        end loop;
        result := result || jsonb_build_object(
          'retval', true
        );
    end loop;

    return result;
  END;
$$ LANGUAGE plpgsql;

