CREATE OR REPLACE FUNCTION jshow_learning_knowledge(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    this_condition record;
    all_learning varchar[];
    k varchar;
    p numeric;

  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient' then
      caller := buffer->>'recipient';
    elsif buffer ? 'player_id' then
      caller := buffer->>'player_id';
    else
      caller := 'nobody';
    end if;

    select array(select * from jsonb_array_elements_text(query->'this_query')) into all_learning;
    
    if all_learning <> array[]::varchar[]
    then
      foreach k in array all_learning
      loop
        select estimate_attribute(k, 'learning_points') into p;
        this_buf := buffer || jsonb_build_object(
          'learning_points', p::numeric,
          'target', k,
          'response_class', 'learning_progress',
          'response_type', 'message'
        );
        perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
      end loop;
    else
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_are_not_learning_anything',
        'response_type', 'message'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

