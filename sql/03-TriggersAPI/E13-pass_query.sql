CREATE OR REPLACE FUNCTION jpass_query(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    query_as_array varchar[];
    query_as_string varchar;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_query');
    
    if 'pass_query_as_array' = any(this_condition.tags)
    then
      query_as_array := array( select * from jsonb_array_elements_text(query->'this_query') );
      result := result || jsonb_build_object(
        write_field, query_as_array,
        'retval', true
      );
    elsif 'pass_query_as_string' = any(this_condition.tags)
    then
      query_as_array := array( select * from jsonb_array_elements_text(query->'this_query') );
      query_as_string := array_to_string(query_as_array, '_');
      result := result || jsonb_build_object(
        write_field, query_as_string,
        'retval', true
      );
    else
      result := result || jsonb_build_object(
        write_field, jsonb_array_elements_text(query->'this_query'),
        'retval', true
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jcheck_query(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    query_as_array varchar[];
    query_as_string varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_query');
    
    if 'check_query_as_array' = any(this_condition.tags)
    then
      query_as_array := array( select * from jsonb_array_elements_text(query->'this_query') );
      select query_as_array <> array[]::varchar[] into retval;
      result := result || jsonb_build_object(
        'retval', retval
      );
    else
      result := result || jsonb_build_object(
        'retval', query->'retval'
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jconcat_string(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    string varchar;
    arg varchar;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_string');
    
    if buffer ? write_field
    then string := buffer->>write_field;
    else string := ''::varchar;
    end if;

    if is_valid(this_condition.query_name)
    then string := string || query->>'this_query';
    end if;

    arg := resolve_condition_arg(this_condition.arg, buffer);

    string := string || arg;

    result := result || jsonb_build_object(
      write_field, string,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jconcat_arg_as_bundled_array(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    string varchar;
    arg varchar;
    bundle varchar[];
    result  JSONB;
    log_class VARCHAR;
  BEGIN
    log_class := 'jconcat_arg_as_bundled_string';
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_bundle');
    
    if buffer ? write_field and 'overwrite' != all(this_condition.tags)
    then bundle := array(select * from jsonb_array_elements_text(buffer->write_field));
    else bundle := array[]::varchar[];
    end if;

    arg := resolve_condition_arg(this_condition.arg, buffer);
    perform log_test_event(log_class, concat('=> Concatenating bundled array:'));
    perform log_test_event(log_class, concat('    "', arg, '"'));
    perform log_test_event(log_class, concat('   into buffer->', write_field), buffer);

    bundle := bundle || array(
      select * from regexp_split_to_table(arg, ',')
    )::varchar[];
    
    result := result || jsonb_build_object(
      write_field, bundle,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jpick_first(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    string varchar;
    field varchar;
    bundle varchar[];
    result  JSONB;
    retval boolean;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'picked_object';
    end if;
    
    field := resolve_condition_arg(this_condition.arg, buffer);

    if buffer ? field
    then bundle := array( select * from jsonb_array_elements_text(buffer->field) );
    else bundle := array[]::varchar[];
    end if;

    select bundle <> array[]::varchar[] into retval;

    result := result || jsonb_build_object(
      write_field, bundle[1],
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

