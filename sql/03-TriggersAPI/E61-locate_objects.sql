CREATE OR REPLACE FUNCTION activate_location(
  object_id VARCHAR
)
RETURNS void AS $$
  DECLARE
    all_in_object varchar[];
    obj varchar;
  BEGIN
    if exists ( select id from objects where id = object_id and active is true )
    then return;
    end if;

    update objects set active = true where id = object_id;
    select * from all_in_self_recursive(object_id) into all_in_object;

    foreach obj in array all_in_object
    loop
      update objects set active = true where id = obj;
    end loop;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION make_location(
  child_id VARCHAR,
  parent_id VARCHAR,
  plug_name VARCHAR
)
RETURNS void AS $$
  BEGIN
    delete from links where type = 'parent' and id = child_id;
    insert into links(id, target, type, name) values (child_id, parent_id, 'parent', plug_name);
    perform activate_location(parent_id);
    return;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION do_make_location(
  child_id VARCHAR,
  parent_id VARCHAR,
  plug_name VARCHAR,
  buffer jsonb default '{}'::jsonb
)
RETURNS void AS $$
  DECLARE
    old_parent varchar;
  BEGIN
    old_parent := pick_parent(child_id);
    perform make_location(child_id, parent_id, plug_name);
    buffer := buffer || jsonb_build_object('old_parent', old_parent);
    perform trigger_hook(old_parent, 'hook_child_leaves', buffer);
    perform trigger_hook(parent_id, 'hook_child_enters', buffer);
    return;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION jmake_location(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    result JSONB;
  BEGIN
    perform do_make_location(buffer->>'child_id', buffer->>'parent_id', buffer->>'plug_name', buffer);
    result := jsonb_build_object('retval', true::boolean);
    return result;
	END;
$$ LANGUAGE plpgsql;

