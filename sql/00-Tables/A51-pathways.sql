DROP TABLE IF EXISTS pathways;
CREATE TABLE pathways (
	origin          VARCHAR REFERENCES everything ON DELETE CASCADE,
	destination     VARCHAR REFERENCES everything ON DELETE CASCADE,
	action          VARCHAR,
	direction       VARCHAR,
  difficulty      NUMERIC DEFAULT 1,
  can_be_closed   BOOLEAN DEFAULT FALSE,
  is_closed       BOOLEAN DEFAULT FALSE,
  can_be_locked   BOOLEAN DEFAULT FALSE,
  is_locked       BOOLEAN DEFAULT FALSE,
  lock_key        VARCHAR DEFAULT ''::VARCHAR
);

drop view if exists displacements;
create view displacements as
  select
    s.id,
    s.target as parent_id,
    p.action as displacement,
    p.direction,
    p.destination,
    p.difficulty,
    t.id as trigger_id
  from
    pathways p,
    slugboard s,
    inherited i,
    triggers t
  where
        s.id = i.id
    and t.target = i.inherits
    and p.origin = s.target
    and t.task = p.action
  ;

