drop view if exists string_attributes;
create view string_attributes as
  select  l.id,
          l.name as attribute_name,
          l.target as value
  from links l
  where l.type = 'attribute';

create view init_attributes as
  select  l.id,
          l.name as attribute_name,
          l.target as value
  from links l
  where l.type = 'init';

create view object_attributes as
  select
    i.id as id,
    a.attribute as attribute
  from
    inherited i,
    attributes_index a
  where
    i.inherits = a.id;

create view attribute_types as
  select
    e.id as attribute_name,
    i.data_type as attribute_type
  from
    everything e,
    information_schema.columns i
  where
    e.id = i.column_name
    and e.type = 'attribute'
    and e.in_table = i.table_name;

create view slots_by_object as
  select
    o.id,
    s.ord,
    s.slot_type,
    s.name,
    s.value,
    s.closure
  from
    objects o,
    slots s,
    inherited i
  where
        o.id = i.id
    and i.inherits = s.id
  order by ord asc;

create view plugs_by_object as
  select
    o.id,
    p.ord,
    p.slot_type,
    p.name,
    p.value
  from
    objects o,
    plugs p,
    inherited i
  where
        o.id = i.id
    and i.inherits = p.id
  order by ord asc;

create view slices_by_object as
  select
    o.id,
    s.name,
    s.mass
  from
    objects o,
    slices s,
    inherited i
  where
        o.id = i.id
    and i.inherits = s.id;

create view slugboard as
  select
    l.id,
    l.target,
    s.slot_type,
    s.name        as  slot_name,
    p.name        as  plug_name,
    s.value       as  slot_value,
    p.value       as  plug_value,
    s.closure     as  slot_closure
  from
    links l,
    slots_by_object s,
    plugs_by_object p
  where
             l.type = 'parent'
    and        l.id = p.id
    and    l.target = s.id
    and s.slot_type = p.slot_type
    and l.name = p.name;

drop view if exists make_neighborhood;
create view make_neighborhood as
  select
    l.id as origin,
    o.classes as origin_classes,
    l.target as neighbour,
    p.classes as neighbour_classes,
    l.name as direction,
    ( select exists (
        select oobj.id
        from objects oobj, objects nobj
        where oobj.id = l.id
        and nobj.id = l.target
        and oobj.classes = nobj.classes
      ) ) as biomes_match,
    ( select exists (select t.tag from tagged t
      where t.id = l.id
      and t.tag = 'is_waterfall') ) as origin_is_flooded,
    ( select exists (select t.tag from tagged t
      where t.id = l.target
      and t.tag = 'is_waterfall') ) as neighbour_is_flooded,
    nt.altitude - ot.altitude as altitude_gradient,
    nt.effective_temperature - ot.effective_temperature as temperature_gradient,
    ( select e.ecoregion_name
      from ecoregions e
      where e.tile_id = l.id
      and e.is_proto_ecoregion is true ) as origin_proto_ecoregion,
    ( select e.ecoregion_name
      from ecoregions e
      where e.tile_id = l.target
      and e.is_proto_ecoregion is true ) as neighbour_proto_ecoregion
  from
    links l,
    tiles ot,
    tiles nt,
    objects o,
    objects p
  where
        l.type = 'neighbour'
    and l.target = nt.id
    and l.id = ot.id
    and o.id = l.id
    and p.id = l.target;

create view modifiers as
  select
    t.id as modifier,
    t.target as target,
    t.task as attribute
  from
    triggers t
  where
    t.type = 'modifier';

