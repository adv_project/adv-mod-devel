DROP TABLE IF EXISTS dict_classes;
CREATE TABLE dict_classes (
  ord integer,
	class_id VARCHAR,
	language VARCHAR,
  string TEXT
);

DROP TABLE IF EXISTS dict_tokens;
CREATE TABLE dict_tokens (
  ord integer,
	token VARCHAR,
	language VARCHAR,
	context VARCHAR,
  string TEXT
);

DROP TABLE IF EXISTS dict_punctuation;
CREATE TABLE dict_punctuation (
	token VARCHAR,
	language VARCHAR,
  string TEXT
);

DROP TABLE IF EXISTS dict_world_params;
CREATE TABLE dict_world_params (
	token VARCHAR,
	language VARCHAR,
  string TEXT
);

DROP TABLE IF EXISTS dict_custom;
CREATE TABLE dict_custom (
  ord integer,
	sentence TEXT,
	language VARCHAR,
  custom_text TEXT[]
);

DROP TABLE IF EXISTS dict_info;
CREATE TABLE dict_info (
  ord integer,
	sentence TEXT,
	language VARCHAR,
  info_text TEXT
);

