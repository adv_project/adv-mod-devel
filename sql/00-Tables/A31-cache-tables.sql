/* Traces of estimated attributes */

CREATE TABLE estimated_values (
  id          VARCHAR  REFERENCES everything ON DELETE CASCADE,
	attribute   VARCHAR,
	value       NUMERIC
);

CREATE TABLE inherited (
	id          VARCHAR    REFERENCES everything ON DELETE CASCADE,
	inherits    VARCHAR    REFERENCES everything ON DELETE CASCADE
);

CREATE TABLE tagged (
	id          VARCHAR    REFERENCES everything ON DELETE CASCADE,
	tag         VARCHAR
);

