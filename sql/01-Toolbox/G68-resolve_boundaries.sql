CREATE OR REPLACE FUNCTION resolve_boundaries(
  attribute VARCHAR,
  old_value NUMERIC,
  new_value NUMERIC
)
RETURNS TABLE (
  direction  VARCHAR,
  boundaries VARCHAR[]
)
AS $$

  DECLARE
    boundary RECORD;
    reached  BOOLEAN := false;
    ret      VARCHAR[];
    
  BEGIN
    IF old_value < new_value THEN
      FOR boundary IN
        SELECT   *
        FROM     boundaries
        WHERE    id = attribute
        ORDER BY value
      LOOP
        IF old_value <= boundary.value THEN
          reached := true;
        END IF;
        
        IF  reached
        AND new_value > boundary.value
        THEN
          ret := ret || array(
            SELECT boundary.label
          );
        END IF;
      END LOOP;

      IF ret IS NOT NULL THEN
        RETURN QUERY
          SELECT
            'up'::VARCHAR   AS direction,
            ret::VARCHAR[]  AS boundaries;
      END IF;
    ELSIF old_value > new_value THEN
      FOR boundary IN
        SELECT   *
        FROM     boundaries
        WHERE    id = attribute
        ORDER BY value DESC
      LOOP
        IF old_value > boundary.value THEN
          reached := true;
        END IF;
        
        IF reached AND new_value <= boundary.value THEN
          ret := ret || array(
            SELECT boundary.label
          );
        END IF;
      END LOOP;
        
      IF ret IS NOT NULL THEN
        RETURN QUERY
          SELECT
            'down'::VARCHAR AS direction,
            ret::VARCHAR[]  AS boundaries;
      END IF;
    END IF;
  END;
$$ LANGUAGE plpgsql;

