CREATE OR REPLACE FUNCTION get_static_tags(object_id varchar) RETURNS varchar[] AS $$
  DECLARE
    our_table varchar;
    result varchar[];
  BEGIN
    select e.in_table from everything e where object_id = e.id into our_table;
    if our_table is null or our_table = 'objects' then return array[]::varchar[]; end if;
    execute format(
      'select t.tags from %s t
       where t.id = %L;',
       our_table,
       object_id
    ) into result;
    RETURN result;
  END;
$$ language plpgsql;

