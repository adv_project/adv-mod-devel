CREATE TABLE available_directions (
  direction VARCHAR
);

insert into available_directions (direction) values
  ('north'), ('south'), ('west'), ('east'),
  ('north_east'), ('south_west'), ('north_west'), ('south_east'),
  ('right'), ('left'), ('front');

CREATE OR REPLACE FUNCTION pick_unused_direction(
  origin_id VARCHAR
) RETURNS VARCHAR AS $$
  DECLARE
    d         varchar;
    log_class varchar;
  BEGIN
    log_class := 'pick_unused_direction';
    perform log_internal_event(log_class,
      concat('Searching for unused directions from <', origin_id, '>') );
    perform log_internal_event(log_class,
      array_to_string(array(select direction from pathways where origin = origin_id)::varchar[], ', ') );
    for d in select direction from available_directions order by random()
    loop
      perform log_internal_event(log_class,
        concat('checking if "', d, '" is in use') );
      if not exists (
        select direction from pathways where origin = origin_id and direction = d
      )
      then
        perform log_internal_event(log_class,
          concat('picked direction: "', d, '"') );
        return d;
      end if;
    end loop;
    perform log_internal_event(log_class,
      concat('falling back to "nowhere"') );
    return 'nowhere';
	END;
$$ LANGUAGE plpgsql;

