CREATE OR REPLACE FUNCTION resolve_color_code(
  this_code NUMERIC
)
RETURNS VARCHAR AS $$
  DECLARE
    color VARCHAR;
  BEGIN
    select color_name from color_codes where color_code = this_code limit 1 into color;
    if color is null
    then
      select color_name from color_codes where color_code = round(this_code) limit 1 into color;
      select color || '_ish' into color;
    end if;
    if color is null
    then
      select color_name from color_codes where color_code = round(trunc) limit 1 into color;
      select color || '_ish' into color;
    end if;
    if color is null
    then
      select 'color_ish' into color;
    end if;

    return color;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_diaphaneity_code(
  this_code NUMERIC
)
RETURNS VARCHAR AS $$
  DECLARE
    diaphaneity VARCHAR;
  BEGIN
    select diaphaneity_name from diaphaneity_codes where diaphaneity_code = this_code limit 1 into diaphaneity;
    if diaphaneity is null
    then
      select diaphaneity_name from diaphaneity_codes where diaphaneity_code = round(this_code) limit 1 into diaphaneity;
      select diaphaneity || '_ish' into diaphaneity;
    end if;
    if diaphaneity is null
    then
      select diaphaneity_name from diaphaneity_codes where diaphaneity_code = round(trunc) limit 1 into diaphaneity;
      select diaphaneity || '_ish' into diaphaneity;
    end if;
    if diaphaneity is null
    then
      select 'diaphaneity_ish' into diaphaneity;
    end if;

    return diaphaneity;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resolve_shape_code(
  this_code NUMERIC
)
RETURNS VARCHAR AS $$
  DECLARE
    shape VARCHAR;
  BEGIN
    select shape_name from shape_codes where shape_code = this_code limit 1 into shape;
    if shape is null
    then
      select shape_name from shape_codes where shape_code = round(this_code) limit 1 into shape;
      select shape || '_ish' into shape;
    end if;
    if shape is null
    then
      select shape_name from shape_codes where shape_code = round(trunc) limit 1 into shape;
      select shape || '_ish' into shape;
    end if;
    if shape is null
    then
      select 'shape_ish' into shape;
    end if;

    return shape;
  END;
$$ LANGUAGE plpgsql;

