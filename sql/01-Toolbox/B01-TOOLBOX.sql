/* OLD ADV MOD CODE */

CREATE OR REPLACE FUNCTION is_valid(
  value VARCHAR
)
RETURNS BOOLEAN AS $$
  BEGIN
    IF value IS NULL OR value = ''::VARCHAR OR value = 'unknown'
    THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
  END;
$$ LANGUAGE plpgsql;

/* @doc h2 Database queries*/

CREATE OR REPLACE FUNCTION is_instance(
  object_id VARCHAR
)
RETURNS BOOLEAN AS $$
  BEGIN
    RETURN EXISTS (
      SELECT TRUE
      FROM   objects o
      WHERE  o.id = object_id
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes inherited by object with id <arg> 
CREATE OR REPLACE FUNCTION get_classes(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    our_table VARCHAR;
  
  BEGIN
    SELECT e.in_table
    FROM   everything e
    WHERE  e.id = arg
    INTO   our_table;

    IF our_table <> '' THEN
      RETURN QUERY
        EXECUTE FORMAT(
          'SELECT unnest(o.classes)
           FROM   %s o
           WHERE  o.id = ''%s'' ',
          our_table,
          arg
        );
    ELSE
      RETURN QUERY
        SELECT ''::VARCHAR;
    END IF;
  END;
$$ LANGUAGE plpgsql;

-- Returns all components inherited by object with id <arg> 
CREATE OR REPLACE FUNCTION get_components(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT  l.target
      FROM    links l
      WHERE   l.id = arg
      AND     l.type = 'component';
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes recursively inherited by object with id <arg>
CREATE OR REPLACE FUNCTION get_classes_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      WITH RECURSIVE my_recursive_table(current_id) AS (
          VALUES (arg)
            UNION
          SELECT get_classes(current_id)
          FROM   my_recursive_table
          WHERE  current_id IS NOT NULL
      ) SELECT
          rt.current_id AS id
        FROM
          my_recursive_table rt,
          everything         e
        WHERE e.id = rt.current_id;
  END;
$$ LANGUAGE plpgsql;

-- Returns all components recursively inherited by object with id <arg>
CREATE OR REPLACE FUNCTION get_components_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      WITH RECURSIVE my_recursive_table(current_id) AS (
          VALUES (arg)
            UNION
          SELECT get_components(current_id)
          FROM   my_recursive_table
          WHERE  current_id IS NOT NULL
      ) SELECT
          rt.current_id AS id
        FROM
          my_recursive_table rt,
          everything         e
        WHERE e.id = rt.current_id;
  END;
$$ LANGUAGE plpgsql;

-- Returns the tags for all classes inherited by the object
CREATE OR REPLACE FUNCTION get_tags(
  arg VARCHAR
)
RETURNS TABLE (
  tag VARCHAR
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT c.id
      FROM get_classes_recursive(arg) c
    ) LOOP
      SELECT e.in_table
        FROM everything e
      WHERE  e.id = r.id
      INTO   our_table;

      IF our_table <> ''
      AND EXISTS (
        SELECT attname
        FROM   pg_attribute
        WHERE  attrelid = (
            SELECT oid
            FROM   pg_class
            WHERE  relname = our_table LIMIT 1
        )
        AND attname = 'tags' LIMIT 1
      )
      THEN
        RETURN QUERY
          EXECUTE FORMAT(
            'SELECT unnest(o.tags)
             FROM   %s o
             WHERE  o.id = ''%s'' ',
            our_table,
            r.id
          );
      ELSE
        RETURN QUERY
          SELECT ''::VARCHAR;
      END IF;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_elements;
CREATE OR REPLACE FUNCTION get_elements(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    r         RECORD;
    c         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT cl.id
      FROM   get_classes_recursive(arg) cl
    )
    LOOP
      RETURN QUERY
        SELECT slot.element
        FROM   slots slot
        WHERE  slot.id = r.id
        AND    slot.element IS NOT NULL;

      RETURN QUERY
        SELECT plug.element
        FROM   plugs plug
        WHERE  plug.id = r.id
        AND    plug.element != NULL;

      RETURN QUERY
        SELECT slice.element
        FROM   slices slice
        WHERE  slice.id = r.id;
    END LOOP;

    FOR r IN (
      SELECT cm.id
      FROM   get_components_recursive(arg) cm
    )
      LOOP
      FOR c IN (
        SELECT cl.id
        FROM   get_classes_recursive(r.id) cl
      )
      LOOP
        RETURN QUERY
          SELECT slice.element
          FROM   slices slice
          WHERE  slice.id = c.id;
      END LOOP;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_elements_recursive;
CREATE OR REPLACE FUNCTION get_elements_recursive(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  
  DECLARE
    r RECORD;
  
  BEGIN
    for r IN (
      SELECT e.id
      FROM   get_elements(arg) e
    )
    LOOP
      RETURN QUERY
        SELECT c.id
        FROM   get_classes_recursive(r.id) c;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_element_tags;
CREATE OR REPLACE FUNCTION get_element_tags(
  arg VARCHAR
)
RETURNS TABLE (
  tag VARCHAR
)
AS $$
  
  DECLARE
    r RECORD;
  
  BEGIN
    FOR r IN (
      SELECT e.id
      FROM   get_elements_recursive(arg) e
    )
    LOOP
      RETURN QUERY
        SELECT t.tag
        FROM   get_tags(r.id) t;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

--drop function IF exists get_all_inherited;
CREATE OR REPLACE FUNCTION get_all_inherited(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR
)
AS $$
  BEGIN
    RETURN QUERY
      SELECT  c.id
      FROM    get_classes_recursive(arg) c;
    
	 RETURN QUERY
      SELECT e.id
      FROM   get_elements_recursive(arg) e;
    
	 RETURN QUERY
      SELECT  l.target
      FROM    links l
      WHERE   l.id = arg
      AND     l.type = 'inherits';
  END;
$$ LANGUAGE plpgsql;

-- Returns the slots provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_slots(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  ord integer,
  name VARCHAR,
  slot_type VARCHAR,
  value numeric,
  element VARCHAR,
  mass numeric,
  closure numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    FOR r IN (
      SELECT c.id
      FROM   get_classes_recursive(arg) c
    )
    LOOP
      RETURN QUERY
        SELECT *
        FROM   slots s
        WHERE  s.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

-- Returns the plugs provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_plugs(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  ord integer,
  name VARCHAR,
  slot_type VARCHAR,
  value numeric,
  element VARCHAR,
  mass numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    for r IN (
      SELECT c.id
      FROM   get_classes_recursive(arg) c)
    LOOP
      RETURN QUERY
        SELECT *
        FROM   plugs p
        WHERE  p.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

-- Returns the slices provided by all classes inherited by the object
CREATE OR REPLACE FUNCTION get_slices(
  arg VARCHAR
)
RETURNS TABLE (
  id VARCHAR,
  name VARCHAR,
  element VARCHAR,
  mass numeric
)
AS $$
  
  DECLARE
    r         RECORD;
    our_table VARCHAR;
  
  BEGIN
    for r IN (
      SELECT c.id
      FROM get_classes_recursive(arg) c
    )
    LOOP
      RETURN QUERY
        SELECT *
        FROM   slices p
        WHERE  p.id = r.id;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

/* END OF OLD ADV MOD CODE */

/* OLD INIT HOOK CODE */

-- Returns a random alphanumeric ASCII character (0-9, A-Z or a-z)
CREATE OR REPLACE FUNCTION new_alphanum_int()
RETURNS INTEGER AS $$
  
  DECLARE
      i INTEGER;
  
  BEGIN
    /* We have 10 + 26 + 26 = 62 possible characters */
    i := round( 62*random() );

    IF    (i < 10) THEN RETURN (i + 48);  -- We picked a number
    ELSIF (i < 36) THEN RETURN (i + 55);  -- We picked an uppercase letter
    ELSIF (i < 62) THEN RETURN (i + 61);  -- We picked a lowercase letter
    ELSE                RETURN      122;  -- Border case, RETURN 'z'
    END IF;
  END;
$$ LANGUAGE plpgsql;

-- Returns a random ID of length id_length
CREATE OR REPLACE FUNCTION new_id(
  id_length INTEGER
)
RETURNS VARCHAR AS $$
  BEGIN
    RETURN array_to_string(
      array(
        SELECT chr( new_alphanum_int() )
        FROM   generate_series(1, id_length)
      ),
      ''
    );
  END;
$$ LANGUAGE plpgsql;

-- Place a new identifier inside the 'everything' table
CREATE OR REPLACE FUNCTION make()
RETURNS VARCHAR AS $$
  
  DECLARE
    created_id      VARCHAR;
    id_length       INTEGER := 4; -- Initial length for new IDs
    number_of_tries INTEGER;
  
  BEGIN
    LOOP
      number_of_tries := 0;

      LOOP
        created_id := new_id(id_length);
        EXIT WHEN NOT EXISTS (
          SELECT o.id
          FROM   objects o
          WHERE created_id = o.id
        );
        number_of_tries := number_of_tries + 1;
        EXIT WHEN number_of_tries = 10; -- Number of tries before abandoning this ID length
      END LOOP;

      EXIT WHEN NOT EXISTS (
        SELECT o.id
        FROM   objects o
        WHERE  created_id = o.id
      );

      id_length := id_length + 1;
    END LOOP;

    INSERT INTO everything
    VALUES (created_id);

    RETURN created_id;
  END;
$$ LANGUAGE plpgsql;

/* END OF OLD INIT HOOK CODE */

CREATE OR REPLACE FUNCTION is_routine_name(
  arg   VARCHAR
)
RETURNS BOOLEAN AS $$
  BEGIN
    IF EXISTS (
      SELECT routine_name
      FROM   information_schema.routines
      WHERE  specific_schema = ( SELECT current_schema() )
      AND    routine_name = arg
    )
    THEN  RETURN TRUE;
    ELSE  RETURN FALSE;
    END IF;
  END;
$$ LANGUAGE plpgsql;

