[dialog:world_help_help]
#(green)Usage: #(yellow)help
#(green)       #(yellow)help commands
#(green)       #(yellow)help #(magenta)<command>
[end dialog]

[dialog:world_help_commands]
#(green)Available commands for this game phase:
  #(magenta) show set setgtype makeworld
[end dialog]

[dialog:world_help_show]
#(green)Usage: #(yellow)show
#(green)Show current world parameters on screen.
[end dialog]

[dialog:world_help_set]
#(green)Usage: #(yellow)set #(magenta)<param> <value>
#(green)Change value for the given parameter. Valid parameter names are:
  #(magenta) chrons year ticksperday daysperyear landperc wtilt
  #(magenta) gsize gwidth gheight altmin altmax salmin salmax shummin shummax
  #(magenta) atmhumdef tempmin tempmax tempmaxgrad tempperc temprand
  #(magenta) pressmin pressmax pressmaxgrad pressperc pressrand pressdef
  #(magenta) accstr erstr erperc errand colstr colperc colrand wfperc wfrand
#(green)Use #(bold)#(yellow)help #(reset)#(magenta)<param> #(green) for more information about each parameter.
[end dialog]

[dialog:world_help_setgtype]
#(green)Usage: #(yellow)setgtype #(magenta)<value>
#(green)Change value for geomerty type. Valid types are #(magenta)planet#(green) and #(magenta)grid#(green).
[end dialog]

[dialog:world_help_makeworld]
#(green)Usage: #(yellow)makeworld
#(green)Create the world with the current parameters and proceed to
#(green)the character creation phase.
[end dialog]

[dialog:game_help_help]
#(bold)#(blue)Usage: #(green)help
#(bold)#(blue)       #(green)help commands
#(bold)#(blue)       #(green)help #(magenta)<command>
[end dialog]

[dialog:game_help_commands]
#(bold)#(blue)Available commands for this game phase:
#(bold)  #(magenta) look read inspect stats inventory
#(bold)  #(magenta) go (walk, climb, descend, swim...)
#(bold)  #(magenta) pick drop wield hold carry
#(bold)  #(magenta) eat drink gather try recall build
[end dialog]

[dialog:game_help_look]
#(bold)#(blue)Usage: #(green)look
#(bold)#(blue)Look around.
[end dialog]

[dialog:game_help_inspect]
#(bold)#(blue)Usage: #(green)inspect
#(bold)              #(green)inspect #(magenta)<object>
#(bold)#(blue)Look closely at some place or object. Use #(green)inspect#(blue) without arguments
#(bold)#(blue)to inspect the place you are in.
[end dialog]

[dialog:game_help_read]
#(bold)#(blue)Usage: #(green)read #(magenta)<object>
#(bold)#(blue)Use it to read books, signs, and anything that has text written in it.
[end dialog]

[dialog:game_help_stats]
#(bold)#(blue)Usage: #(green)stats
#(bold)#(blue)Show your game stats.
[end dialog]

[dialog:game_help_inventory]
#(bold)#(blue)Usage: #(green)inventory
#(bold)#(blue)Shows your inventory.
#(bold)#(blue)You have 23 slots to carry objects (the #(cyan)inventory#(blue)), plus the #(cyan)wield#(blue) slot (right
#(bold)#(blue)hand) and the #(cyan)hold#(blue) slot (left hand).
#(bold)#(blue)Use #(green)wield#(blue) to move objects from the inventory to the right hand, #(green)hold#(blue) to
#(bold)#(blue)move objects from the inventory to the left hand, and #(green)carry#(blue) to move objects from
#(bold)#(blue)both hands to the inventory.
[end dialog]

[dialog:game_help_go]
#(bold)#(blue)Usage: #(green)go #(magenta)<direction>
#(bold)#(blue)Move in a certain direction. Directions are those shown by #(green)look#(blue).
[end dialog]

[dialog:game_help_pick]
#(bold)#(blue)Usage: #(green)pick #(magenta)<object>
#(bold)#(blue)Pick a nearby object and put it in your inventory.
[end dialog]

[dialog:game_help_drop]
#(bold)#(blue)Usage: #(green)drop #(magenta)<object>
#(bold)#(blue)Drop a nearby object and put it in your inventory.
[end dialog]

[dialog:game_help_wield]
#(bold)#(blue)Usage: #(green)wield #(magenta)<object>
#(bold)#(blue)Move an object from your inventory to your right hand.
[end dialog]

[dialog:game_help_hold]
#(bold)#(blue)Usage: #(green)hold #(magenta)<object>
#(bold)#(blue)Move an object from your inventory to your left hand.
[end dialog]

[dialog:game_help_carry]
#(bold)#(blue)Usage: #(green)carry #(magenta)<object>
#(bold)#(blue)Move an object from your hands to your inventory.
[end dialog]

[dialog:game_help_eat]
#(bold)#(blue)Usage: #(green)eat #(magenta)<object>
#(bold)               #(green)eat #(magenta)food
#(bold)#(blue)Eat whatever is available, if possible. If you eat too much you will get sick.
[end dialog]

[dialog:game_help_drink]
#(bold)#(blue)Usage: #(green)drink #(magenta)<liquid>
#(bold)               #(green)drink
#(bold)#(blue)Drink whatever is available, if possible. If you drink too much you will get sick.
#(bold)#(blue)Also, avoid drinking nasty liquids.
[end dialog]

[dialog:game_help_gather]
#(bold)#(blue)Usage: #(green)gather #(magenta)<object|resource>
#(bold)#(blue)Search for natural resources in the environment; what you can gather depends on where
#(bold)#(blue)you are, what tools you have, etc. For example, to gather water you will need a bottle
#(bold)#(blue)to hold it.
[end dialog]

[dialog:game_help_try]
#(bold)#(blue)Usage: #(green)try #(magenta)<object>
#(bold)#(blue)You can learn new techniques experimenting with things; #(green)try#(blue) a rock many
#(bold)#(blue)times and you may achieve something.
#(bold)#(blue)If you fail, try it a different way: perhaps holding the rock with your hands? Which hand?
#(bold)#(blue)Once you have mastered the technique, you will be able to see the world with more detail and
#(bold)#(blue)build more interesting and useful artifacts.
[end dialog]

[dialog:game_help_recall]
#(bold)#(blue)Usage: #(green)recall
#(bold)#(blue)       #(green)recall #(magenta)<knowledge>
#(bold)#(blue)       #(green)recall #(magenta)<object>
#(bold)#(blue)Review your knowledge of materials and techniques. Use #(green)recall#(blue) without arguments
#(bold)#(blue)to review what techniques you are learning and/or have already mastered; pass a
#(bold)#(blue)technique or object to review in detail what that technique/material has to offer.
[end dialog]

[dialog:game_help_build]
#(bold)#(blue)Usage: #(green)build #(magenta)<object>
#(bold)#(blue)Use your abilities to build new objects; generally you need to learn some technique first,
#(bold)#(blue)and also habe some tools and/or materials available and in the right place.
[end dialog]

